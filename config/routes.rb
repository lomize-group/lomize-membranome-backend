Rails.application.routes.draw do
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

    # these routes are explicitly defined for ease of reading
    # this should really be nested if routes beyond simple gets are implemented
    # nesting should never be more than one level deep

    match "protein_types" => "protein_types#find_all", :via => :get
    match "protein_types/:id" => "protein_types#find_one", :via => :get
    match "protein_types/:id/protein_classes" => "protein_types#find_classtypes", :via => :get
    match "protein_types/:id/proteins" => "protein_types#proteins", :via => :get

    match "protein_classes" => "protein_classes#find_all", :via => :get
    match "protein_classes/:id" => "protein_classes#find_one", :via => :get
    match "protein_classes/:id/protein_superfamilies" => "protein_classes#find_superfamilies", :via => :get
    match "protein_classes/:id/proteins" => "protein_classes#proteins", :via => :get

    match "protein_superfamilies" => "protein_superfamilies#find_all", :via => :get
    match "protein_superfamilies/:id" => "protein_superfamilies#find_one", :via => :get
    match "protein_superfamilies/:id/protein_families" => "protein_superfamilies#find_families", :via => :get
    match "protein_superfamilies/:id/proteins" => "protein_superfamilies#proteins", :via => :get

    match "protein_families" => "protein_families#find_all", :via => :get
    match "protein_families/:id" => "protein_families#find_one", :via => :get
    match "protein_families/:id/proteins" => "protein_families#proteins", :via => :get

    match "proteins" => "proteins#find_all", :via => :get
    match "proteins/random" => "proteins#find_random", :via => :get
    match "proteins/:id" => "proteins#find_one", :via => :get
    match "proteins/:id/mutations" => "proteins#mutations", :via => :get

    match "protein_pathways" => "protein_pathways#find_all", :via => :get
    match "protein_pathways/:id" => "protein_pathways#find_one", :via => :get



    ## Pathways
    match "pathways" => "pathways#find_all", :via => :get
    match "pathways/:id" => "pathways#find_one", :via => :get

    match "pathway_subclasses" => "pathway_subclasses#find_all", :via => :get
    match "pathway_subclasses/:id" => "pathway_subclasses#find_one", :via => :get
    match "pathway_subclasses/:id/pathways" => "pathway_subclasses#find_pathways", :via => :get

    match "pathway_classes" => "pathway_classes#find_all", :via => :get
    match "pathway_classes/:id" => "pathway_classes#find_one", :via => :get
    match "pathway_classes/:id/pathway_subclasses" => "pathway_classes#find_pathway_subclasses", :via => :get
    match "pathway_classes/:id/pathways" => "pathway_classes#find_pathways", :via => :get



    ## Diseases and mutations
    match "mutation_types" => "mutation_types#find_all", :via => :get
    match "mutation_types/:id" => "mutation_types#find_one", :via => :get
    match "mutation_types/:id/mutations" => "mutation_types#mutations", :via => :get

    match "diseases" => "diseases#find_all", :via => :get
    match "diseases/:id" => "diseases#find_one", :via => :get
    match "diseases/:id/mutations" => "diseases#mutations", :via => :get

    match "mutations" => "mutations#find_all", :via => :get



    ## Complexes
    match "complex_types" => "complex_types#find_all", :via => :get
    match "complex_types/:id" => "complex_types#find_one", :via => :get
    match "complex_types/:id/complex_classes" => "complex_types#complex_classes", :via => :get
    match "complex_types/:id/complex_structures" => "complex_types#complex_structures", :via => :get

    match "complex_classes" => "complex_classes#find_all", :via => :get
    match "complex_classes/:id" => "complex_classes#find_one", :via => :get
    match "complex_classes/:id/complex_superfamilies" => "complex_classes#complex_superfamilies", :via => :get
    match "complex_classes/:id/complex_structures" => "complex_classes#complex_structures", :via => :get

    match "complex_superfamilies" => "complex_superfamilies#find_all", :via => :get
    match "complex_superfamilies/:id" => "complex_superfamilies#find_one", :via => :get
    match "complex_superfamilies/:id/complex_families" => "complex_superfamilies#complex_families", :via => :get
    match "complex_superfamilies/:id/complex_structures" => "complex_superfamilies#complex_structures", :via => :get

    match "complex_families" => "complex_families#find_all", :via => :get
    match "complex_families/:id" => "complex_families#find_one", :via => :get
    match "complex_families/:id/complex_structures" => "complex_families#complex_structures", :via => :get

    match "complex_structures" => "complex_structures#find_all", :via => :get
    match "complex_structures/random" => "complex_structures#find_random", :via => :get
    match "complex_structures/:id" => "complex_structures#find_one", :via => :get

    match "subcomplexes" => "subcomplexes#find_all", :via => :get
    match "subcomplexes/random" => "subcomplexes#find_random", :via => :get
    match "subcomplexes/:id" => "subcomplexes#find_one", :via => :get


    ## relational objects
    match "membranes" => "membranes#find_all", :via => :get
    match "membranes/:id" => "membranes#find_one", :via => :get
    match "membranes/:id/proteins" => "membranes#find_proteins", :via => :get
    match "membranes/:id/complex_structures" => "membranes#find_complex_structures", :via => :get

    match "species" => "species#find_all", :via => :get
    match "species/:id" => "species#find_one", :via => :get
    match "species/:id/proteins" => "species#find_proteins", :via => :get

    match "opm_links/:pdbid" => "opm_links#find_by_pdb", :via => :get


    ## Other one offs
    match "stats" => "stats#basic_stats", :via => :get
    match "1tmnet" => "tmnet#run_tmnet", :via => :get #broken

    #new advanced search
    match "advanced_search" => "proteins#advanced_search", :via => :get

end

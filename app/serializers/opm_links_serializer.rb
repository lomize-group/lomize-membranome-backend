include ActiveModel::Serialization
class OpmLinksSerializer < ActiveModel::Serializer
  attributes :id, :protein_id, :opm_link, :subunit_name, :protein_pdbid

  def protein_pdbid
  	object.protein.pdbid
  end

end

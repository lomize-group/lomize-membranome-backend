class DiseasesSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :mutations_count
end

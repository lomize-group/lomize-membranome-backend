include ActiveModel::Serialization
class PathwayClassesPageSerializer < ActiveModel::Serializer
  type :pathway_classes
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: PathwayClassesSerializer
    
end

include ActiveModel::Serialization
class DomainsSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :firstaa, :lastaa, :protein_pdbid,
		 :pdbsubunit, :seqidentity, :domain, :pfamlink
end

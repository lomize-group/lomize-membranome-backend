include ActiveModel::Serialization
class SubcomplexesPageSerializer < ActiveModel::Serializer
  type :subcomplexes
  attributes :direction, :sort, :total_objects,
    :page_size, :page_start, :page_end, :page_num,
    :objects

  has_many :objects, serializer: SubcomplexesSerializer
end

include ActiveModel::Serialization
class PdbLinksSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :pdb, :subunit_name
end

include ActiveModel::Serialization
class ComplexStructuresPageSerializer < ActiveModel::Serializer
  type :complex_structures
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: ComplexStructuresSerializer
    
end

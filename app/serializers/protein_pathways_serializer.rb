include ActiveModel::Serialization
class ProteinPathwaysSerializer < ActiveModel::Serializer
	attributes :id, :ordering, :protein_id, :pathway_id,
	:pathway_link_cache, :pathway_name_cache, :pathway_dbname_cache,
	:protein_name_cache, :protein_pdbid_cache
end

include ActiveModel::Serialization
class ProteinsSerializer < ActiveModel::Serializer
    attributes :id, :ordering, :ordering_cache, :name, :comments, :pdbid,
       :resolution, :topology_show_in, :thickness,
       :thicknesserror, :tilt, :tilterror, :gibbs, :tau, :verification,
       :uniprotcode, :genename, :genenumber, :altname, :segment,
       :aaseq, :interpro, :pharmgkb, :mimegene, :hmp,
       :protein_family_name_cache, :protein_family_id,
       :membrane_name_cache, :membrane_id,
       :species_name_cache, :species_id, :mutations, :intact, :biogrid


    has_one :protein_family, serializer: ProteinFamiliesSerializer
    has_one :species, serializer: SpeciesSerializer
    has_one :membrane, serializer: MembranesSerializer

    has_many :additional_membranes, serializer: ProteinMembranesSerializer
  	has_many :complex_structures
    has_many :related_proteins, serializer: ProteinInteractionsSerializer

    has_many :opm_links, serializer: OpmLinksSerializer
    has_many :protein_pathways, serializer: ProteinPathwaysSerializer
    has_many :domains, serializer: DomainsSerializer
    has_many :pdb_links, serializer: PdbLinksSerializer


    #kicks off the biological classification tree
    def protein_family
      object.protein_family
    end

    def species
      object.species
    end

    def membrane
      object.membrane
    end

    def additional_membranes
      object.protein_membranes.where(:main => false)
    end

    def complex_structures
      object.complex_proteins
    end

    def related_proteins
      protein_list = ProteinInteraction.where(:primary_protein_id => object.id)
      # replace pubmeds that are just spaces with empty strings
      protein_list.each{ |x|
        if x.pubmed == " "
          x.pubmed = ""
        end
        if x.experiment_type = " "
          x.experiment_type = ""
        end
      }
      return protein_list
    end

  	def opm_links
  		object.opm_links
  	end

    def protein_pathways
      object.protein_pathways.order(:ordering)
    end

    def domains
      object.domains
    end

    def pdb_links
      object.pdb_links
    end

end

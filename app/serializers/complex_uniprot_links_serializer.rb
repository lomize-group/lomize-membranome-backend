include ActiveModel::Serialization
class ComplexUniprotLinksSerializer < ActiveModel::Serializer
	attributes :id, :uniprotcode, :uniprotid, :uniprot_name, :type_of_protein
end
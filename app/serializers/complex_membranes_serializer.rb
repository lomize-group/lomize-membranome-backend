include ActiveModel::Serialization
class ComplexMembranesSerializer < ActiveModel::Serializer
	attributes :id, :membrane_name_cache, :membrane_id, :complex_structures_count_cache
end
include ActiveModel::Serialization
class ProteinTypesSerializer < ActiveModel::Serializer
  attributes :id, :ordering_cache, :name, :description, :protein_classes_count
  
end

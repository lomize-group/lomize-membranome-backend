include ActiveModel::Serialization
class ProteinInteractionsSerializer < ActiveModel::Serializer
   attributes :related_protein_name_cache, :complex, :pubmed,
      :related_protein_id, :experiment_type, :interaction_type, :dbname
end
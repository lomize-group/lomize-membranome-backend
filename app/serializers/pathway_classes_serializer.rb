include ActiveModel::Serialization
class PathwayClassesSerializer < ActiveModel::Serializer
    attributes :id, :ordering, :ordering_cache, :name, :pathway_subclasses_count

end

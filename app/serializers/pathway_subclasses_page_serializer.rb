include ActiveModel::Serialization
class PathwaySubclassesPageSerializer < ActiveModel::Serializer
   type :pathway_subclasses
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: PathwaySubclassesSerializer

end

include ActiveModel::Serialization
class SubcomplexesSerializer < ActiveModel::Serializer
  attributes :id, :name, :uniprotids, :pdbcodes, :source_db, :source_db_link, :pubmed
end

include ActiveModel::Serialization
class PathwaySubclassesSerializer < ActiveModel::Serializer
    attributes :id, :ordering, :ordering_cache, :name, :pathways_count, :pathway_class_id, :pathway_class

    has_one :pathway_class, serializer: PathwayClassesSerializer

    def pathway_class
    	object.pathway_class
    end
end

include ActiveModel::Serialization
class MutationsSerializer < ActiveModel::Serializer
	attributes :id, :ordering_cache, :mutation, :pdbid, :protein_family_id,
	:protein_family_name_cache, :diseases_name_cache, :protein_id, :residue,
	:mutation_type_name_cache, :comments, :source
end

class DiseasesPageSerializer < ActiveModel::Serializer
  type :diseases
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, each_serializer: DiseasesSerializer
end

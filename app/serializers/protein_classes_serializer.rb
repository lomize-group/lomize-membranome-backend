include ActiveModel::Serialization
class ProteinClassesSerializer < ActiveModel::Serializer
  attributes :id, :ordering_cache, :name, :description, 
  	:protein_superfamilies_count

  has_one :protein_type, serializer: ProteinTypesSerializer

  def protein_type
  	object.protein_type
  end


end

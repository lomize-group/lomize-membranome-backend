include ActiveModel::Serialization
class ProteinSuperfamiliesPageSerializer < ActiveModel::Serializer
  type :protein_superfamilies
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: ProteinSuperfamiliesSerializer

end

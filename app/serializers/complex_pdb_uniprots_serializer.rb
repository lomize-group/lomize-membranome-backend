include ActiveModel::Serialization
class ComplexPdbUniprotsSerializer < ActiveModel::Serializer
  attributes :id, :uniprotid
end

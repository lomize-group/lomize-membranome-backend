include ActiveModel::Serialization
class ComplexClassificationsSerializer < ActiveModel::Serializer
	attributes :id, :complex_structure_id, :protein_family_id, :protein_superfamily_id,
		:protein_class_id, :protein_type_id, :complex_family, :complex_superfamily

	has_one :complex_family, serializer: ProteinFamiliesSerializer
	has_one :complex_superfamily, serializer: ProteinSuperfamiliesSerializer

	def complex_family
		object.protein_family
	end

	def complex_superfamily
		object.protein_superfamily
	end
end
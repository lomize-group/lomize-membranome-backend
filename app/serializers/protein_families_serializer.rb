include ActiveModel::Serialization
class ProteinFamiliesSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :ordering_cache, :name,
  :interpro, :proteins_count, :complex_structures_count

  has_one :protein_superfamily, serializer: ProteinSuperfamiliesSerializer

  def protein_superfamily
    object.protein_superfamily
  end

  def proteins_count
    species = @instance_options[:species]
    if species && species.length > 0
      return object.proteins.where(:species_id => species).count
    end
    
    return object.proteins_count
  end

end

include ActiveModel::Serialization
class MembranesSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :name,:topology_in,:topology_out,
		   :abbreviation, :proteins_count, :complex_structures_count
		   
	def proteins_count
		species = @instance_options[:species]
		if species && species.length > 0
			return object.proteins.where(:species_id => species).count
		end
	
		return object.proteins_count
	end
end

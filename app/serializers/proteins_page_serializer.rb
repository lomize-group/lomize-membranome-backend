include ActiveModel::Serialization
class ProteinsPageSerializer < ActiveModel::Serializer
  type :proteins
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: ProteinsSerializer

end

include ActiveModel::Serialization
class PathwaysPageSerializer < ActiveModel::Serializer
   type :pathways
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: PathwaysSerializer

end

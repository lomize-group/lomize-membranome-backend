include ActiveModel::Serialization
class ProteinMembranesSerializer < ActiveModel::Serializer
  attributes :id, :membrane_name_cache, :pubmed
end

include ActiveModel::Serialization
class SpeciesSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :name,:name_common,
  	:description,:proteins_count, :complex_structures_count
end

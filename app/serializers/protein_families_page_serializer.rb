include ActiveModel::Serialization
class ProteinFamiliesPageSerializer < ActiveModel::Serializer
   type :protein_families
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: ProteinFamiliesSerializer

end

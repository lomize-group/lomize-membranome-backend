include ActiveModel::Serialization
class ComplexStructuresSerializer < ActiveModel::Serializer
	attributes :id, :ordering, :name,
	 :thickness, :thicknesserror, :tilt, :tilterror,
	 :gibbs, :comments, :refpdbcode, :numsubunits,
	 :complex_family_data, :complex_membrane_data,
	 :source_db, :source_db_link, :pubmed, :polytopic_proteins_count


	has_many :complex_classes, serializer: ProteinClassesSerializer
	has_many :complex_superfamilies, serializer: ProteinSuperfamiliesSerializer
	has_many :complex_families, serializer: ProteinFamiliesSerializer
	
	has_many :membranes, serializer: ComplexMembranesSerializer

	has_many :related_proteins
	has_many :pathways, serializer: ProteinPathwaysSerializer

	has_many :pdb_links, serializer: ComplexPdbLinksSerializer
	has_many :uniprot_links, serializer: ComplexUniprotLinksSerializer

	has_many :subcomplexes, serializer: SubcomplexesSerializer

	def source_db
		object.source_db
	end

	def source_db_link
		object.source_db_link
	end

	def pubmed
		object.pubmed
	end

	def complex_families
		object.complex_classifications
				.select(:protein_family_id)
				.map{|c| c.protein_family}
	end

	def complex_family_data
		object.complex_classifications.select(:protein_family_id, :protein_family_name_cache)
	end

	def complex_membrane_data
		object.complex_membranes.select(:membrane_id, :membrane_name_cache)
	end

	def complex_superfamilies
		# may be multiple families for complex, belonging to same superfamily
		ComplexClassification.select("DISTINCT protein_superfamily_id")
			.where(:complex_structure_id => object.id)
			.map{|c| c.protein_superfamily}
	end

	def complex_classes
		# may be multiple families for complex, belonging to same class
		ComplexClassification.select("DISTINCT protein_class_id")
			.where(:complex_structure_id => object.id)
			.map{|c| c.protein_class}
	end

	def subcomplexes
		object.subcomplexes
	end

	def membranes
		object.complex_membranes
	end

	def pathways
		ProteinPathway.select("DISTINCT ON (pathway_id) *")
				.where(:protein_id => object.complex_proteins.select(:protein_id))
	end

	def related_proteins
		object.complex_proteins.select(:protein_id).map{|p| p.protein}
	end

	def pdb_links
		object.complex_pdb_links
	end
	
	def uniprot_links
		object.complex_uniprot_links
	end

end

include ActiveModel::Serialization
class ProteinTypesPageSerializer < ActiveModel::Serializer
  type :protein_types
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: ProteinTypesSerializer
    
end

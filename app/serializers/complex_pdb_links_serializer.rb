include ActiveModel::Serialization
class ComplexPdbLinksSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :pdb, :resolution, :species_name, :uniprots

  has_many :uniprots, serializer: ComplexPdbUniprotsSerializer

  def uniprots
    object.complex_pdb_uniprots.select(:uniprotid)
  end

end
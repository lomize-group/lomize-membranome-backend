class MutationTypesPageSerializer < ActiveModel::Serializer
  type :mutation_types
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: MutationTypesSerializer
end

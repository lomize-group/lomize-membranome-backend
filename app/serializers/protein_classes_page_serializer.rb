include ActiveModel::Serialization
class ProteinClassesPageSerializer < ActiveModel::Serializer
  type :protein_classes
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: ProteinClassesSerializer

end

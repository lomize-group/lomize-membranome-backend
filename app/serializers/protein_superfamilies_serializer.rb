include ActiveModel::Serialization
class ProteinSuperfamiliesSerializer < ActiveModel::Serializer
  attributes :id, :ordering_cache, :name, :comment, :pfam, 
  	:protein_families_count

  has_one :protein_class, serializer: ProteinClassesSerializer
  
  def protein_class
  	object.protein_class
  end
  
end

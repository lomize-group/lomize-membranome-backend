include ActiveModel::Serialization
class PathwaysSerializer < ActiveModel::Serializer
	attributes :id, :ordering, :ordering_cache, :name, :dbname, :link,
	:pathway_subclass_name_cache, :pathway_subclass_id

	has_one :pathway_subclass, serializer: PathwaySubclassesSerializer

	has_many :protein_pathways, serializer: ProteinPathwaysSerializer

	has_many :complex_structures
	has_many :membranes

    def pathway_subclass
    	object.pathway_subclass
	end
	
	def complex_structures
		ComplexProtein.select("DISTINCT ON (complex_structure_id) *")
				.where(:protein_id => object.protein_pathways.select(:protein_id))
	end

	def membranes
		ProteinMembrane.select("DISTINCT ON (membrane_id) *")
				.where(:protein_id => object.protein_pathways.select(:protein_id), :main => true)
	end

    def protein_pathways
    	object.protein_pathways.order(:ordering)
    end

end

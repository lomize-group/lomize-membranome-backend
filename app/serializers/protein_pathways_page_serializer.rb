include ActiveModel::Serialization
class ProteinPathwaysPageSerializer < ActiveModel::Serializer
   type :protein_pathways
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: ProteinPathwaysSerializer
   
end

class MembranesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "membranes" => 'membranes#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Membrane.all.order(:id)
			file = file_response(Membrane.column_names, objects)
			return send_data file, :filename => "localizations-#{Date.today}.csv", :type => @fileFormat
		end
		objects = Membrane.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Membrane.search(@search).count)
		render :json =>result, :status => 200, serializer: MembranesPageSerializer, :species => @species
	end

	# match "membranes/:id" => 'membranes#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Membrane.find(@id)
		render :json => result, :status => 200, serializer: MembranesSerializer, include: ['']
	end


	# match "membranes/:id/proteins" => 'membranes#find_proteins', :via => :get
	def find_proteins
		response.headers['Access-Control-Allow-Origin'] = '*'
		protein_species_selection(Membrane.find(@id).proteins)
		result = paged_response(@objects, @count)
		render :json => result, :status => 200, serializer: ProteinsPageSerializer,
			include: { :objects => :related_proteins }
	end
  
  	# match "membranes/:id/complex_structures" => 'membranes#find_complex_structures', :via => :get
  	def find_complex_structures
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ComplexStructure.joins(:complex_membranes)
				.where('complex_membranes.membrane_id' => @id)
				.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
				.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ComplexStructuresPageSerializer
	end
  

end

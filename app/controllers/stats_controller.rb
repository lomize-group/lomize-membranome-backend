class StatsController < ApplicationController
	require 'ostruct'


	# match "stats" => 'stats#basic_stats', :via => :get
	def basic_stats
		response.headers['Access-Control-Allow-Origin'] = '*'
		
		stats = OpenStruct.new(
			:protein_types => ProteinType.all.count,
			:protein_classes => ProteinClass.all.count,
			:protein_superfamilies => ProteinSuperfamily.all.count,
			:protein_families => ProteinFamily.all.count,
			:proteins => Protein.all.count,
			:species => Species.all.count,
			:membranes => Membrane.all.count,
			:complex_types => ProteinType.where("complex_structures_count > 0").count,
			:complex_classes => ProteinClass.where("complex_structures_count > 0").count,
			:complex_superfamilies => ProteinSuperfamily.where("complex_structures_count > 0").count,
			:complex_families => ProteinFamily.where("complex_structures_count > 0").count,
			:complex_structures => ComplexStructure.all.count,
			:diseases => Disease.all.count,
			:mutation_types => MutationType.all.count,
			:mutations => Mutation.all.count,
			:pathway_subclasses => PathwaySubclass.all.count,
			:pathway_classes => PathwayClass.all.count,
			:pathways => Pathway.all.count
		)

		render :json => stats, :status => 200
	end

end
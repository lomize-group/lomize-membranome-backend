class ComplexFamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "complex_families" => "complex_families#find_all", :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinFamily.where("complex_structures_count > 0").order(:id)
			file = file_response(ProteinFamily.column_names, objects)
			return send_data file, :filename => "complex_families-#{Date.today}.csv", :type => @fileFormat
		end
		all_objects = ProteinFamily.where("complex_structures_count > 0").search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ProteinFamiliesPageSerializer
	end

	# match "complex_families/:id" => "complex_families#find_one", :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinFamily.find(@id)
		render :json => result, serializer: ProteinFamiliesSerializer, :status => 200,
		include: [
			:protein_superfamily,
			:protein_superfamily => :protein_class
		]
	end

	# match "complex_families/:id/complex_structures" => "complex_families#complex_structures", :via => :get
	def complex_structures
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ComplexStructure.joins(:complex_classifications)
			.where("complex_classifications.protein_family_id" => @id)
			.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ComplexStructuresPageSerializer
	end

end
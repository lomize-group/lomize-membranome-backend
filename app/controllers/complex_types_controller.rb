class ComplexTypesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "complex_types" => "complex_types#find_all", :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinType.where("complex_structures_count > 0").order(:id)
			file = file_response(ProteinType.column_names, objects)
			return send_data file, :filename => "complex_types-#{Date.today}.csv", :type => @fileFormat
		end
		all_objects = ProteinType.where("complex_structures_count > 0").search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ProteinTypesPageSerializer
	end

	# match "complex_types/:id" => "complex_types#find_one", :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinType.find(@id)
		render :json => result, serializer: ProteinTypesSerializer, :status => 200
	end
	
	# match "complex_types/:id/complex_classes" => "complex_types#complex_classes", :via => :get
	def complex_classes
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ProteinType.find(@id)
			.protein_classes
			.where("complex_structures_count > 0")
			.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ProteinClassesPageSerializer
	end
	
	# match "complex_types/:id/complex_structures" => "complex_types#complex_structures", :via => :get
	def complex_structures
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ComplexStructure
			.joins(:complex_classifications)
			.where(complex_classifications: {protein_type_id: @id})
			.distinct
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ComplexStructuresPageSerializer
	end
end

class SpeciesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "species" => 'species#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Species.all.order(:id)
			file = file_response(Species.column_names, objects)
			return send_data file, :filename => "species-#{Date.today}.csv", :type => @fileFormat
		end
		objects = Species.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Species.search(@search).count)
		render :json =>result, :status => 200, serializer: SpeciesPageSerializer
	end

	# match "species/:id" => 'species#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Species.find(@id)
		render :json =>result, :status => 200, serializer: SpeciesSerializer, include: ['']
	end

	# match "species/:id/proteins" => 'species#find_proteins', :via => :get
	def find_proteins
		response.headers['Access-Control-Allow-Origin'] = '*'

		protein_species_selection(Species.find(@id).proteins)
		objects = @objects
		count = @count
		
		result = paged_response(objects, count)
		render :json =>result, :status => 200, serializer: ProteinsPageSerializer, include: []
	end

end
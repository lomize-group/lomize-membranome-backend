class PathwayClassesController < ApplicationController
	before_action :parse_paging_params

	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'

		if @fileFormat != ""
			objects = PathwayClass.all.order(:id)
			file = file_response(PathwayClass.column_names, objects)
			return send_data file, :filename => "pathwayclasses-#{Date.today}.csv", :type => @fileFormat
		end

		objects = PathwayClass.search(@search)
			.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, PathwayClass.search(@search).count)

		render :json => result, :status => 200, serializer: PathwayClassesPageSerializer
	end

	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'

		result = PathwayClass.find(@id)

		render :json => result, serializer: PathwayClassesSerializer, :status => 200
	end

	# match "pathway_classes/:id/pathway_subclasses" => 'pathway_classes#find_pathway_subclasses', :via => :get
	def find_pathway_subclasses
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PathwayClass.find(@id)
			.pathway_subclasses
			.order(@sort_column => @sort_direction)
			.search(@search)
			.paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, PathwayClass.find(@id).pathway_subclasses.search(@search).count)
			
		render :json => result, :status => 200, serializer: PathwaySubclassesPageSerializer
	end

	# match "pathway_classes/:id/pathways" => 'pathway_classes#find_pathways', :via => :get
	def find_pathways
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PathwayClass.find(@id)
			.pathways
			.order(@sort_column => @sort_direction)
			.search(@search)
			.paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, PathwayClass.find(@id).pathways.search(@search).count)

		render :json => result, :status => 200, serializer: PathwaysPageSerializer
	end

end
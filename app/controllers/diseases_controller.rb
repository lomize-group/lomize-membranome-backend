class DiseasesController < ApplicationController
	before_action :parse_paging_params

	# match "diseases" => 'diseases#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Disease.all.order(:id)
			file = file_response(Disease.column_names, objects)
			return send_data file, :filename => "diseases-#{Date.today}.csv", :type => @fileFormat
		end
		objects = Disease.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, Disease.search(@search).count)
		render :json =>result, :status => 200, serializer: DiseasesPageSerializer
	end

	# match "diseases/:id" => 'diseases#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Disease.find(@id)
		render :json =>result, serializer: MutationTypesSerializer, :status => 200
	end

	# match "diseases/:id/mutations" => 'diseases#mutations', :via => :get
	def mutations
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = Disease.find(@id).mutations.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects,  objects.count)
		render :json =>result, :status => 200, serializer: MutationsPageSerializer
	end
end

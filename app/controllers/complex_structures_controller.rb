class ComplexStructuresController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "complex_structures" => "complex_structures#find_all", :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ComplexStructure.all.order(:id)
			file = file_response(ComplexStructure.column_names, objects)
			return send_data file, :filename => "complex_structures-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ComplexStructure.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ComplexStructure.search(@search).count)
		render :json => result, :status => 200, serializer: ComplexStructuresPageSerializer
	end

	# match "complex_structures/random" => "complex_structures#find_random", :via => :get
	def find_random
		response.headers['Access-Control-Allow-Origin'] = '*'
		offset = rand(ComplexStructure.count)
		result = ComplexStructure.offset(offset).first
		render :json => result, serializer: ComplexStructuresSerializer, :status => 200
	end

	# match "complex_structures/:id" => "complex_structures#find_one", :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ComplexStructure.find(@id)
		render :json => result, serializer: ComplexStructuresSerializer, :status => 200,
		include: [
			:complex_families,
			:complex_superfamilies,
			:complex_classes,
			:membranes,
			:related_proteins,
			:subcomplexes,
			:pdb_links,
			:uniprot_links,
			:pathways
		]
	end
  

end
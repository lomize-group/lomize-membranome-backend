class ProteinFamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "protein_families" => 'families#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinFamily.all.order(:id)
			file = file_response(ProteinFamily.column_names, objects)
			return send_data file, :filename => "proteinfamilies-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ProteinFamily.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinFamily.search(@search).count)
		render :json => result, :status => 200, serializer: ProteinFamiliesPageSerializer
	end

	# match "protein_families/:id" => 'families#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinFamily.find(@id)
		render :json =>result, serializer: ProteinFamiliesSerializer, :status => 200, 
		include: [
			'protein_superfamily',
			'protein_superfamily.protein_class',
			'protein_superfamily.protein_class.protein_type'
		]

	end

	# match "protein_families/:id/proteins" => 'protein_families#proteins', :via => :get
	def proteins
		response.headers['Access-Control-Allow-Origin'] = '*'
		
		protein_species_selection(ProteinFamily.find(@id).proteins)
		result = paged_response(@objects, @count)
		render :json => result, :status => 200, serializer: ProteinsPageSerializer,
			include: { :objects => :related_proteins }
	end
  

end

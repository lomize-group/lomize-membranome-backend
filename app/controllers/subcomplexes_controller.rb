class SubcomplexesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "subcomplexes" => "subcomplexes#find_all", :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Subcomplex.all.order(:id)
			file = file_response(Subcomplex.column_names, objects)
			return send_data file, :filename => "subcomplexes-#{Date.today}.csv", :type => @fileFormat
		end
        if @sort_column == "ordering"
            @sort_column = "id"
        end
		objects = Subcomplex.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Subcomplex.search(@search).count)
		render :json => result, :status => 200, serializer: SubcomplexesPageSerializer
	end

	# match "subcomplexes/random" => "subcomplexes#find_random", :via => :get
	def find_random
		response.headers['Access-Control-Allow-Origin'] = '*'
		offset = rand(Subcomplex.count)
		result = Subcomplex.offset(offset).first
		render :json => result, serializer: SubcomplexesSerializer, :status => 200
	end

	# match "subcomplexes/:id" => "subcomplexes#find_one", :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Subcomplex.find(@id)
		render :json => result, serializer: SubcomplexesSerializer, :status => 200
	end
  

end

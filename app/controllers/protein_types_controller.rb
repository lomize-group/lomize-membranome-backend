class ProteinTypesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "protein_types" => 'types#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinType.all.order(:id)
			file = file_response(ProteinType.column_names, objects)
			return send_data file, :filename => "proteintypes-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ProteinType.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinType.search(@search).count)
		render :json =>result, :status => 200, serializer: ProteinTypesPageSerializer
	end

	# match "protein_types/:id" => 'types#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinType.find(@id)
		render :json =>result, :status => 200, serializer: ProteinTypesSerializer
	end

	# match "protein_types/:id/protein_classes" => 'types#find_classtypes', :via => :get
	def find_classtypes
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = ProteinType.find(@id).protein_classes.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinType.find(@id).protein_classes.search(@search).count)
		render :json =>result, :status => 200, serializer: ProteinClassesPageSerializer
	end

	# match "protein_types/:id/proteins" => 'types#proteins', :via => :get
	def proteins
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = ProteinType.find(@id).proteins.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Protein.where(:protein_type_id => @id).search(@search).count)
		protein_species_selection(ProteinType.find(@id).proteins)
		result = paged_response(@objects, @count)
		
		render :json =>result, :status => 200, serializer: ProteinsPageSerializer
	end
	
end
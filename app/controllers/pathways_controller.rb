class PathwaysController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "pathways" => 'pathways#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Pathway.all.order(:id)
			file = file_response(Pathway.column_names, objects)
			return send_data file, :filename => "pathways-#{Date.today}.csv", :type => @fileFormat
		end
		objects = Pathway.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Pathway.search(@search).count)
		render :json => result, :status => 200, serializer: PathwaysPageSerializer
	end

	# match "pathways/:id" => 'pathways#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Pathway.find(@id)
		render :json => result, serializer: PathwaysSerializer, :status => 200
	end
end

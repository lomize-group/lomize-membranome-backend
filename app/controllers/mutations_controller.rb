class MutationsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "mutations" => 'mutations#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Mutation.all.order(:id)
			file = file_response(Mutation.column_names, objects)
			return send_data file, :filename => "mutations-#{Date.today}.csv", :type => @fileFormat
		end
		objects = Mutation.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Mutation.search(@search).count)
		render :json =>result, :status => 200, serializer: MutationsPageSerializer
	end
end
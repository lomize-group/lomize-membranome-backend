class ProteinsController < ApplicationController
	include ActiveRecord::Sanitization
	include ActiveRecord::Sanitization::ClassMethods
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "proteins" => 'proteins#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Protein.all.order(:id)
			file = file_response(Protein.column_names, objects)
			return send_data file, :filename => "proteins-#{Date.today}.csv", :type => @fileFormat
		end
		protein_species_selection(Protein.all)
		result = paged_response(@objects, @count)
		# why were related_proteins already included, since they are included in ProteinsSerializer?
		render :json =>result, :status => 200, serializer: ProteinsPageSerializer,
			include: { :objects => :related_proteins }
	end

	# match "proteins/random" => 'proteins#find_random', :via => :get
	def find_random
		response.headers['Access-Control-Allow-Origin'] = '*'
		offset = rand(Protein.count)
		result = Protein.offset(offset).first
		render :json =>result, serializer: ProteinsSerializer, :status => 200,
		 include: [
			'opm_links','protein_pathways','domains','pdb_links',
			'complex_structures', 'related_proteins',
			'species',
			'membrane',
			'protein_family',
			'protein_family.protein_superfamily',
			'protein_family.protein_superfamily.protein_class',
			'protein_family.protein_superfamily.protein_class.protein_type'
		]
	end

	# match "proteins/:id" => 'proteins#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Protein.find(@id)
		render :json => result, serializer: ProteinsSerializer, :status => 200,
		include: [
			:opm_links, :protein_pathways, :domains, :pdb_links,
			:complex_structures, :related_proteins, :additional_membranes,
			:species,
			:membrane,
			:protein_family,
			{:protein_family => :protein_superfamily},
			{:protein_family => {:protein_superfamily => :protein_class}},
			{:protein_family => {:protein_superfamily => {:protein_class => :protein_type}}}
		]
	end

	# match "proteins/:id/mutations" => 'proteins#mutations', :via => :get
	def mutations
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = Protein.find(@id).mutations.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Mutation.where(:protein_id => @id).search(@search).count)
		
		render :json => result, :status => 200, serializer: MutationsPageSerializer
	end

	def advanced_search

		response.headers['Access-Control-Allow-Origin'] = '*'

		#helper funnctions for comparisons
		def to_comparison cmp
			cmp = cmp.to_s
			operators = ["<", "<=", ">", ">=", "="]
			operators.include?(cmp) ? cmp : "<"
		end

		def int_or_nil num
			num.to_s == "" ? nil : num.to_i
		end

		def float_or_nil num
			num.to_s == "" ? nil : num.to_f
		end

		#retrieve search values and API parameters
		protein_name = sanitize_sql_like(params[:name].to_s.strip.downcase)
		uniprot_id = sanitize_sql_like(params[:uniprot_id].to_s.strip.downcase)
		pdbid = sanitize_sql_like(params[:pdbid].to_s.strip.downcase)
		genename = sanitize_sql_like(params[:genename].to_s.strip.downcase)
		uniprot_features= sanitize_sql_like(params[:uniprot_features].to_s.strip.downcase)
		uniprot_annotations = sanitize_sql_like(params[:uniprot_annotations].to_s.strip.downcase)
		species = sanitize_sql_like(params[:species].to_s.strip.downcase)
		superfamily = sanitize_sql_like(params[:superfamily].to_s.strip.downcase)
		family = sanitize_sql_like(params[:family].to_s.strip.downcase)
		localization = sanitize_sql_like(params[:localization].to_s.strip.downcase)
		pfam_domains = sanitize_sql_like(params[:pfam_domains].to_s.strip.downcase)

		#comparison parameters here
		thickness_compare = to_comparison(params[:thickness_compare])
		thickness = float_or_nil(params[:thickness])
		tilt_angle_compare = to_comparison(params[:tilt_angle_compare])
		tilt_angle = int_or_nil(params[:tilt_angle])
		delta_transfer_compare = to_comparison(params[:delta_transfer_compare])
		delta_transfer = float_or_nil(params[:delta_transfer])
		delta_fold_compare = to_comparison(params[:delta_fold_compare])
		delta_fold = float_or_nil(params[:delta_fold])

		objects = Protein.all

		#add filters for fields
		if protein_name != ""
			objects = objects.where("lower(proteins.name) LIKE ?", "%#{protein_name}%").or(objects.where("lower(proteins.altname) LIKE ?","%#{protein_name}%"))
		end
		if uniprot_id != ""
			objects = objects.where("lower(proteins.pdbid) LIKE ?", "%#{uniprot_id}%").or(objects.where("lower(proteins.uniprotcode) LIKE ?", "%#{uniprot_id}%"))
		end	
		if pdbid != ""
			objects = objects.joins(:pdb_links).where("lower(pdb_links.pdb) LIKE ?", "%#{pdbid}%")
		end
		if genename != ""
			objects = objects.where("lower(proteins.genename) LIKE ?", "#{genename}")
		end
		if uniprot_features != ""
			#named differently in database
			objects = objects.where("lower(proteins.comments) LIKE ?", "%#{uniprot_features}%") 
		end
		if uniprot_annotations != ""
            #named differently in database
			objects = objects.where("lower(proteins.verification) LIKE ?", "%#{uniprot_annotations}%") 
		end	
		if species != ""
			objects = objects.where("lower(proteins.species_name_cache) LIKE ?", "%#{species}%")
			#objects = objects.joins(:species).where("lower(species.name) LIKE ?", "%#{species}%")
		end
		if superfamily != "" #TODO: inspect later
			objects = objects.joins(:protein_superfamily).where("lower(protein_superfamilies.name) LIKE ?", "%#{superfamily}%")
		end
		if family != ""
			objects = objects.where("lower(proteins.protein_family_name_cache) LIKE ?", "%#{family}%")
		end
		if localization != "" #inconsistent database name
			objects = objects.joins(:protein_membranes).where("lower(protein_membranes.membrane_name_cache) LIKE ?", "%#{localization}%")

		end
		if pfam_domains != ""
			objects = objects.joins(:domains).where("lower(domains.domain) LIKE ? OR lower(domains.pfamlink) LIKE ?","%#{pfam_domains}%","%#{pfam_domains}%")
		end

		#quantitative filters
		if !thickness.nil? && thickness_compare != ""
			objects = objects.where("proteins.thickness #{thickness_compare} #{thickness}")
		end
		if !tilt_angle.nil? && tilt_angle_compare != ""
			objects = objects.where("proteins.tilt #{tilt_angle_compare} #{tilt_angle}")
		end
		if !delta_transfer.nil? && delta_transfer_compare != ""
			objects = objects.where("proteins.interpro #{delta_transfer_compare} #{delta_transfer}") #inconsistent databse
		end
		if !delta_fold.nil? && delta_fold_compare != ""
			objects = objects.where("proteins.pharmgkb #{delta_fold_compare} #{delta_fold}")
		end

		count = objects.count
		objects = objects.order(@sort_column => @sort_direction)

		result = paged_response(objects, count)
		render :json => result, :serializer => ProteinsPageSerializer, :status => 200
	end
  
end

class TmnetController < ApplicationController
	before_action :parse_paging_params

	def self.interactions protein_ids
		direct_interactions = []
		direct_id_set = Set.new
		protein_ids.combination(2).each do |pair|
			if ProteinInteraction.exists?(:primary_protein_id => pair[0], :related_protein_id => pair[1])
				proteins = Protein.select(:id, :name, :pdbid).where(:id => pair)
				direct_interactions.append(proteins)
				direct_id_set.add(pair)
			end
		end

		indirect_interactions = []
		ProteinInteraction.select(:related_protein_id, "ARRAY_AGG(primary_protein_id) AS protein_ids")
			.where(:primary_protein_id => protein_ids)
			.group(:related_protein_id)
			.having("COUNT(*) > 1")
			.each{ |pi|
				related_protein = Protein.select(:id, :name, :pdbid).find(pi.related_protein_id)
				pi.protein_ids.combination(2).each do |pair|
					next if direct_id_set.include?(pair) || direct_id_set.include?(pair.reverse)
					indirect_interactions.append({
						:related_protein => related_protein,
						:proteins => Protein.select(:id, :name, :pdbid).where(:id => pair)
					})
				end
			}

		return direct_interactions, indirect_interactions
	end

	def self.complexes protein_ids
		# -- roughly equivalent in Postgres:
		# SELECT complex_structure_id, ARRAY_AGG(protein_id) AS protein_ids
		# FROM complex_proteins
		# WHERE protein_id in protein_id_list
		# GROUP BY (complex_structure_id)
		# HAVING COUNT(complex_structure_id) > 1;
		ComplexProtein.select(:complex_structure_id, "ARRAY_AGG(protein_id) AS protein_ids")
			.where(:protein_id => protein_ids)
			.group(:complex_structure_id)
			.having("COUNT(complex_structure_id) > 1")
			.map{ |cp|
				{
					:complex_structure => ComplexStructure.select(:id, :name).find(cp.complex_structure_id),
					:selected_proteins => Protein.select(:id, :name, :pdbid).where(:id => cp.protein_ids),
					:additional_proteins =>  Protein.select(:id, :name, :pdbid).where("proteins.id NOT IN (?)", cp.protein_ids).joins(:complex_proteins).where("complex_proteins.complex_structure_id = #{cp.complex_structure_id}"),
					:membranes => ComplexMembrane.where(:complex_structure_id => cp.complex_structure_id)
				}
			}
	end

	def self.pathways protein_ids, species
		ProteinPathway.select(:pathway_id, "ARRAY_AGG(protein_id) AS protein_ids")
			.where(:protein_id => protein_ids)
			.group(:pathway_id)
			.having("COUNT(pathway_id) > 1")
			.map{ |pp|
				{
					:pathway => Pathway.select(:id, :name, :dbname, :link).find(pp.pathway_id),
					:selected_proteins => Protein.select(:id, :name, :pdbid).where(:id => pp.protein_ids),
					:additional_proteins => Protein.select(:id, :name, :pdbid)
						.where("proteins.id NOT IN (?)", pp.protein_ids)
						.where(:species_id => species)
						.joins(:protein_pathways)
						.where("protein_pathways.pathway_id = #{pp.pathway_id}")
				}
			}
	end

	# match "tmnet" => "tmnet#run_tmnet", :via => :get
	def run_tmnet
		response.headers['Access-Control-Allow-Origin'] = '*'
		
		protein_ids = params[:protein_ids].map{|i| i.to_i}.select{|i| !i.nil?}.uniq
		direct_interactions, indirect_interactions = TmnetController.interactions(protein_ids)
		result = {
			:selected => Protein.select(:id, :name, :pdbid).where(:id => protein_ids),
			:direct_interactions => direct_interactions,
			:indirect_interactions => indirect_interactions,
			:complexes => TmnetController.complexes(protein_ids),
			:pathways => TmnetController.pathways(protein_ids, @species)
		}

		render :json => result, :status => 200
	end
end
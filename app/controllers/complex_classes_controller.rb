class ComplexClassesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "complex_classes" => "complex_classes#find_all", :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinClass.where("complex_structures_count > 0").order(:id)
			file = file_response(ProteinClass.column_names, objects)
			return send_data file, :filename => "complex_classes-#{Date.today}.csv", :type => @fileFormat
		end
		all_objects = ProteinClass.where("complex_structures_count > 0").search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ProteinClassesPageSerializer
	end

	# match "complex_classes/:id" => "complex_classes#find_one", :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinClass.find(@id)
		render :json => result, serializer: ProteinClassesSerializer, :status => 200
	end

	# match "complex_classes/:id/complex_superfamilies" => "complex_classes#complex_superfamilies", :via => :get
	def complex_superfamilies
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ProteinClass.find(@id)
			.protein_superfamilies
			.where("complex_structures_count > 0")
			.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ProteinSuperfamiliesPageSerializer
	end

	# match "complex_classes/:id/complex_structures" => "complex_classes#complex_structures", :via => :get
	def complex_structures
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ComplexStructure.joins(:complex_classifications)
			.where("complex_classifications.protein_class_id" => @id)
			.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ComplexStructuresPageSerializer
	end

end
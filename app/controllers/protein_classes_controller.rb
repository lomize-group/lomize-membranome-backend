class ProteinClassesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "protein_classes" => 'protein_classes#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinClass.all.order(:id)
			file = file_response(ProteinClass.column_names, objects)
			return send_data file, :filename => "proteinclasses-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ProteinClass.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinClass.search(@search).count)
		render :json =>result, :status => 200, serializer: ProteinClassesPageSerializer
	end

	# match "protein_classes/:id" => 'protein_classes#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinClass.find(@id)
		render :json =>result, :status => 200, serializer: ProteinClassesSerializer,
		include: [
		  'protein_type'
		]
	end

	# match "protein_classes/:id/protein_superfamilies" => 'protein_classes#find_superfamilies', :via => :get
	def find_superfamilies
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = ProteinClass.find(@id).protein_superfamilies.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinClass.find(@id).protein_superfamilies.search(@search).count)
		render :json =>result, :status => 200, serializer: ProteinSuperfamiliesPageSerializer
	end

	# match "protein_classes/:id/proteins" => 'protein_classes#proteins', :via => :get
	def proteins
		response.headers['Access-Control-Allow-Origin'] = '*'
		
		protein_species_selection(ProteinClass.find(@id).proteins)
		result = paged_response(@objects, @count)
		
		render :json =>result, :status => 200, serializer: ProteinsPageSerializer
	end

end
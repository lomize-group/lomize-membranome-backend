class MutationTypesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "mutation_types" => 'mutation_types#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = MutationType.all.order(:id)
			file = file_response(MutationType.column_names, objects)
			return send_data file, :filename => "mutationtypes-#{Date.today}.csv", :type => @fileFormat
		end
		objects = MutationType.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, MutationType.search(@search).count)
		render :json =>result, :status => 200, serializer: MutationTypesPageSerializer
	end

	# match "mutation_types/:id" => 'mutation_types#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = MutationType.find(@id)
		render :json =>result, serializer: MutationTypesSerializer, :status => 200
	end

	# match "mutation_types/:id/mutations" => 'mutation_types#mutations', :via => :get
	def mutations
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = MutationType.find(@id).mutations.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, objects.count)
		render :json =>result, :status => 200, serializer: MutationsPageSerializer
	end
end

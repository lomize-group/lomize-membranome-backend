class OpmLinksController < ApplicationController

	# match "opm_links/:pdbid" => 'opm_links#find_by_pdb', :via => :get
	def find_by_pdb
		response.headers['Access-Control-Allow-Origin'] = '*'

		pdbid = params[:pdbid].upcase
		objects = OpmLink.where("UPPER(opm_link) = ?", pdbid)
		render :json => objects, each_serializer: OpmLinksSerializer, :status => 200,
			include: [
				'protein_pdbid'
			]
	end
end

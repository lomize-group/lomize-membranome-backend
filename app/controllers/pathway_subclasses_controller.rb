class PathwaySubclassesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "pathway_subclasses" => 'pathway_subclasses#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'

		if @fileFormat != ""
			objects = PathwaySubclass.all.order(:id)
			file = file_response(PathwaySubclass.column_names, objects)
			return send_data file, :filename => "pathwaysubclasses-#{Date.today}.csv", :type => @fileFormat
		end

		objects = PathwaySubclass.search(@search)
			.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, PathwaySubclass.search(@search).count)

		render :json => result, :status => 200, serializer: PathwaySubclassesPageSerializer
	end

	# match "pathway_subclasses/:id" => 'pathway_subclasses#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'

		result = PathwaySubclass.find(@id)
		
		render :json => result, serializer: PathwaySubclassesSerializer, :status => 200
	end

	# match "pathway_subclasses/:id/pathways" => 'pathway_subclasses#find_pathways', :via => :get
	def find_pathways
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PathwaySubclass.find(@id)
			.pathways
			.order(@sort_column => @sort_direction)
			.search(@search)
			.paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, PathwaySubclass.find(@id).pathways.search(@search).count)

		render :json => result, :status => 200, serializer: PathwaysPageSerializer
	end

end

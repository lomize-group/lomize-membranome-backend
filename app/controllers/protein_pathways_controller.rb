class ProteinPathwaysController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "protein_pathways" => 'protein_pathways#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinPathway.all.order(:id)
			file = file_response(ProteinPathway.column_names, objects)
			return send_data file, :filename => "proteinpathways-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ProteinPathway.search(@search)
			.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
        result = paged_response(objects, ProteinPathway.search(@search).count)
		render :json => result, :status => 200, serializer: ProteinPathwaysPageSerializer
	end

	# match "protein_pathways/:id" => 'protein_pathways#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinPathway.find(@id)
		render :json => result, serializer: ProteinPathwaysSerializer, :status => 200
	end
end

class ProteinSuperfamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "protein_superfamilies" => 'superfamilies#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinSuperfamily.all.order(:id)
			file = file_response(ProteinSuperfamily.column_names, objects)
			return send_data file, :filename => "proteinsuperfamilies-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ProteinSuperfamily.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinSuperfamily.search(@search).count)
		render :json => result, :status => 200, serializer: ProteinSuperfamiliesPageSerializer
	end

	# match "protein_superfamilies/:id" => 'superfamilies#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinSuperfamily.find(@id)
		render :json => result, serializer: ProteinSuperfamiliesSerializer, :status => 200, 
		include: [
			'protein_class',
			'protein_class.protein_type'
		]
	end

	# match "protein_superfamilies/:id/protein_families" => 'superfamilies#find_families', :via => :get
	def find_families
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = ProteinSuperfamily.find(@id).protein_families.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ProteinSuperfamily.find(@id).protein_families.search(@search).count)
		render :json => result, :status => 200, serializer: ProteinFamiliesPageSerializer, :species => @species
	end

	# match "protein_superfamilies/:id/proteins" => 'superfamilies#proteins', :via => :get
	def proteins
		response.headers['Access-Control-Allow-Origin'] = '*'
		
		protein_species_selection(ProteinSuperfamily.find(@id).proteins)
		result = paged_response(@objects, @count)

		render :json => result, :status => 200, serializer: ProteinsPageSerializer
	end
	
end
class ApplicationController < ActionController::API
	require 'csv'
	require 'ostruct'
	include ActiveRecord::Sanitization
	include ActiveRecord::Sanitization::ClassMethods

	def parse_paging_params_unsafe_strings
		@sort_direction = "ASC"
		possible_sort_directions = ["ASC", "DESC"]
		#direction
		if params[:direction] && possible_sort_directions.include?(params[:direction])
			@sort_direction = params[:direction]
		end
		@sort_direction = sanitize_sql_for_order(@sort_direction)

		@sort_column = "ordering"
		#sort
		if params[:sort] && params[:sort].to_s.length > 0
			@sort_column = params[:sort].to_s
			@sort_column = @sort_column.gsub('primary_structures', 'complex_structures')
		end
		@sort_column = sanitize_sql_for_order(@sort_column)

		# response format
		@fileFormat = ""
		possible_file_formats = ["csv"]
		if params[:fileFormat] and possible_file_formats.include?(params[:fileFormat].downcase)
			@fileFormat = params[:fileFormat].downcase
		end

	end

	def parse_paging_params
		parse_paging_params_unsafe_strings()
		
		@search = ""
		if params[:search]
			@search = params[:search]
		end

		@search_options = {
			:search_by => "full",
			:separator => nil
		}
		if params[:search_by]
			@search_options[:search_by] = params[:search_by]
		end
		if params[:search_separator]
			@search_options[:separator] = params[:search_separator]
		end

		#pageSize
		@per_page = 50
		if params[:pageSize] && params[:pageSize].to_i > 0
			@per_page = params[:pageSize].to_i
		end
		
		#pageNum
		@page = 1
		if params[:pageNum] && params[:pageNum].to_i > 0
			@page = params[:pageNum].to_i
		end

		@id = nil
		if params[:id]
			@id = params[:id].to_i
		end

		@species = []
		if params[:species]
			@species = params[:species].to_s.split(",").map(&:strip)
		end
	end

	def file_response(headers, objects)
		CSV.generate(:headers => true) do |csv|
			csv << headers
			objects.each do |obj|
				csv << headers.map{ |col|
					case when col == "pdbid"
						then "=\"#{obj[col]}\""
					else
						obj[col]
					end
				}
			end
		end
	end

	def paged_response(objects, total_objects)
		page_start = @per_page * (@page-1) + 1
		page_end = page_start + objects.length - 1
		if objects.length == 0
			page_start = 0
			page_end = 0
		end

		result = OpenStruct.new(
			:page_num => @page,
		 	:direction => "#{@sort_direction}",
		 	:sort => "#{@sort_column}",
		 	:total_objects => total_objects,
		 	:page_size => @per_page, 
		 	:page_start => page_start, 
		 	:page_end => page_end,
		 	:objects => objects)
		return result
	end

	def protein_species_selection(protein_selection)
		if @species.length > 0
			@objects = protein_selection
				.where(:species_id => @species)
				.order(@sort_column => @sort_direction)
				.search(@search, @search_options)
				.paginate(:page => @page, :per_page => @per_page)
				
			@count = protein_selection.where(:species_id => @species).search(@search, @search_options).count
		else
			@objects = protein_selection
				.order(@sort_column => @sort_direction)
				.search(@search, @search_options)
				.paginate(:page => @page, :per_page => @per_page)
				
			@count = protein_selection.search(@search, @search_options).count
		end
	end

end

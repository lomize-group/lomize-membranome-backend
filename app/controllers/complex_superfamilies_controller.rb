class ComplexSuperfamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "complex_superfamilies" => "complex_superfamilies#find_all", :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ProteinSuperfamily.where("complex_structures_count > 0").order(:id)
			file = file_response(ProteinSuperFamily.column_names, objects)
			return send_data file, :filename => "complex_superfamilies-#{Date.today}.csv", :type => @fileFormat
		end
		all_objects = ProteinSuperfamily.where("complex_structures_count > 0").search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json =>result, :status => 200, serializer: ProteinSuperfamiliesPageSerializer
	end

	# match "complex_superfamilies/:id" => "complex_superfamilies#find_one", :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ProteinSuperfamily.find(@id)
		render :json =>result, serializer: ProteinSuperfamiliesSerializer, :status => 200,
		include: [:protein_class]
	end

	# match "complex_superfamilies/:id/complex_families" => "complex_superfamilies#complex_families", :via => :get
	def complex_families
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ProteinSuperfamily.find(@id)
			.protein_families
			.where("complex_structures_count > 0")
			.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ProteinFamiliesPageSerializer
	end

	# match "complex_superfamilies/:id/complex_structures" => "complex_superfamilies#complex_structures", :via => :get
	def complex_structures
		response.headers['Access-Control-Allow-Origin'] = '*'
		all_objects = ComplexStructure.joins(:complex_classifications)
			.where('complex_classifications.protein_superfamily_id' => @id)
			.search(@search)
		objects = all_objects.paginate(:page => @page, :per_page => @per_page)
			.order(@sort_column => @sort_direction)
		result = paged_response(objects, all_objects.count)
		render :json => result, :status => 200, serializer: ComplexStructuresPageSerializer
	end

end
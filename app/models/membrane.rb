class Membrane < ApplicationRecord
	has_many :proteins
	has_many :protein_membranes

	has_many :complex_structures
	has_many :complex_membranes

	def update_count
		self.update_columns(
			:proteins_count => ProteinMembrane.where(:membrane_id => self.id).count,
			:complex_structures_count => ComplexMembrane.where(:membrane_id => self.id).count
		)
	end

	def self.renumber
		puts "Renumbering Membrane"
		new_num = 0.0
		objects = Membrane.order("ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end
	end
	
	include PgSearch
  	pg_search_scope :search_full_text,
  		against: [:name, :abbreviation, :topology_in, :topology_out],
	  	using: {
	      :trigram => {:threshold => 0.30},
	      :tsearch => {:prefix => true}
	    },
	    :ranked_by => ":trigram"

end

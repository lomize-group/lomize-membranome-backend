class ProteinFamily < ApplicationRecord
	belongs_to :protein_superfamily, dependent: :destroy, counter_cache: true
	has_many :proteins

	has_many :complex_classifications

	def update_count
		ProteinFamily.reset_counters(self.id, :proteins)
		self.update_columns(:complex_structures_count => ComplexClassification.where(:protein_family_id => self.id).count)
	end

	def self.renumber
		puts "Renumbering ProteinFamily"
		new_num = 0.0
		objects = ProteinFamily.joins(:protein_superfamily).order("protein_superfamilies.ordering_cache, ordering")
		object_count = ProteinSuperfamily.order("protein_families_count DESC").first.protein_families_count
		parent = objects.first.protein_superfamily
		global_order = 1
		for object in objects do
			global_order += 1
			if object.protein_superfamily.ordering_cache == parent.ordering_cache
				new_num += 1
			else
				parent = object.protein_superfamily
				new_num = 1
			end
			object.update_column("ordering", global_order)
			new_order_cache = parent.ordering_cache + "." + self.size_order_cache(new_num, object_count)
			object.update_column("ordering_cache", new_order_cache)		
		end
	end

	include PgSearch
	pg_search_scope :search_full_text, against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"

end

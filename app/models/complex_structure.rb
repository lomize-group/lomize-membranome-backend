class ComplexStructure < ApplicationRecord
	belongs_to :species, counter_cache: true

	has_many :subcomplexes
	has_many :complex_classifications
	has_many :complex_membranes, counter_cache: true

	has_many :complex_proteins

	has_many :complex_pdb_links
	has_many :complex_uniprot_links
	

	def update_hierarchy
		self.species_name_cache = self.species.name
		self.polytopic_proteins_count = self.complex_uniprot_links.where(:type_of_protein => "polytopic").count
		self.create_classification
	end

	def create_classification
		# delete and re-create all complex classifications based on component proteins
		self.complex_classifications.destroy_all
		self.complex_proteins.each do |cp|
			protein = cp.protein
			family_id = protein.protein_family_id
			if !self.complex_classifications.exists?(:protein_family_id => family_id)
				ComplexClassification.create!(
					:complex_structure_id => self.id,
					:protein_family_name_cache => protein.protein_family.name,
					:protein_family_id => family_id,
					:protein_superfamily_id => protein.protein_superfamily_id,
					:protein_class_id => protein.protein_class_id,
					:protein_type_id => protein.protein_type_id
				)
			end
		end
	end

	def self.renumber
		puts "Renumbering ComplexStructure"
		objects = ComplexStructure.all.order(:name)
		global_order = 1
		for object in objects do
			object.update_columns(:ordering => global_order)
			global_order += 1
		end
	end
	
	include PgSearch
	pg_search_scope :search_full_text,
		:against => [:name, :refpdbcode],
		:using => {
			:tsearch => {
				:prefix => true
			}
		},
		:ranked_by => ":trigram"
	
end
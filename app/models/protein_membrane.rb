class ProteinMembrane < ApplicationRecord
	belongs_to :protein, dependent: :destroy
	belongs_to :membrane, dependent: :destroy

	def update_hierarchy
		self.membrane_name_cache = self.membrane.name
		self.protein_pdbid_cache = self.protein.pdbid
	end
	
end

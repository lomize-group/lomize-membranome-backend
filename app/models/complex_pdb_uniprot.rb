class ComplexPdbUniprot < ApplicationRecord
	belongs_to :complex_pdb_link

	def self.renumber
		puts "Renumbering ComplexPdbUniprot"
		objects = ComplexPdbUniprot.all.order(:uniprotid).order(:uniprotcode)
		global_order = 1
		for object in objects do
			object.update_columns(:ordering => global_order)
			global_order += 1
		end
	end
end

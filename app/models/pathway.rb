class Pathway < ApplicationRecord
	belongs_to :pathway_subclass, counter_cache: true
	belongs_to :pathway_class

	has_many :protein_pathways

	def update_hierarchy
		# update classification
		self.pathway_class = self.pathway_subclass.pathway_class

		# update caches
		self.pathway_subclass_name_cache = self.pathway_subclass.name
	end

	def self.renumber
		puts "Renumbering Pathway"
		global_order = 1
		cur_order = 1
		objects = Pathway.joins(:pathway_subclass).order("pathway_subclasses.ordering_cache ASC, ordering")
		object_count = PathwaySubclass.order(:pathways_count => "DESC").first.pathways_count
		cur_parent = objects.first.pathway_subclass
		for object in objects do
			if object.pathway_subclass.id != cur_parent.id
				cur_parent = object.pathway_subclass
				cur_order = 1
			end

			new_order_cache = cur_parent.ordering_cache + "." + self.size_order_cache(cur_order, object_count)

			object.update_column(:ordering, global_order)
			object.update_column(:ordering_cache, new_order_cache)

			global_order += 1
			cur_order += 1
		end
	end

	include PgSearch
	pg_search_scope :search_full_text, 
	:against => [:name, :dbname],
	:using => {
		:tsearch => {:prefix => true}
    }

end

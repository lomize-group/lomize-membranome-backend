class ProteinInteraction < ApplicationRecord
	belongs_to :primary_protein, :class_name => "Protein", :foreign_key => "primary_protein_id"
	belongs_to :related_protein, :class_name => "Protein", :foreign_key => "related_protein_id"

	validates :primary_protein, :related_protein, presence: true

	before_destroy :remove_mirror

	def remove_mirror
		mirror = ProteinInteraction.find_by(primary_protein_id: self.related_protein_id, related_protein_id: self.primary_protein_id)
		if mirror != nil
			mirror.delete
		end
	end

	def remove_duplicates
		# find all duplicates
		dups = ProteinInteraction.where(
			:primary_protein_id => self.primary_protein_id,
			:complex => self.complex,
			:related_protein_id => self.related_protein_id,
			:pdbid => self.pdbid
		).to_a

		# we want to keep one record of the duplicates
		dups.pop
		
		# remove remaining records
		dups.each do |m|
			m.delete
		end
	end

	def update_hierarchy
		#update name cache
		self.related_protein_name_cache = self.related_protein.name
 	end


end

class ProteinSuperfamily < ApplicationRecord
	belongs_to :protein_class, dependent: :destroy, counter_cache: true
	has_many :protein_families
	has_many :proteins

	has_many :complex_classifications

	def update_count
	  ProteinSuperfamily.reset_counters(self.id, :protein_families)
	  self.update_columns(:complex_structures_count => ComplexClassification.where(:protein_superfamily_id => self.id).count)
	end

	def self.renumber
		puts "Renumbering ProteinSuperfamily"
		new_num = 0.0
		objects = ProteinSuperfamily.joins(:protein_class).order("protein_classes.ordering_cache, ordering")
		object_count = ProteinClass.order("protein_superfamilies_count DESC").first.protein_superfamilies_count
		parent = objects.first.protein_class
		global_order = 1
		for object in objects do
			global_order += 1
			if object.protein_class.ordering_cache == parent.ordering_cache
				new_num += 1
			else
				parent = object.protein_class
				new_num = 1
			end
			object.update_column("ordering", global_order)
			new_order_cache = parent.ordering_cache + "." + self.size_order_cache(new_num, object_count)
			object.update_column("ordering_cache", new_order_cache)
		end
	end

	
	include PgSearch
	pg_search_scope :search_full_text, against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"

end

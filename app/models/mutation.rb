class Mutation < ApplicationRecord
	belongs_to :protein_family
	belongs_to :protein_superfamily
  	belongs_to :protein_class
  	belongs_to :protein_type
  	belongs_to :protein

  	belongs_to :mutation_type, counter_cache: true

	has_and_belongs_to_many :diseases

  	def update_hierarchy
		#update pointers to upper level objects
		self.protein_family = self.protein.protein_family
		self.protein_superfamily = self.protein_family.protein_superfamily
		self.protein_class = self.protein_superfamily.protein_class
		self.protein_type = self.protein_class.protein_type

		# update caches
		self.pdbid = self.protein.pdbid
		self.protein_family_name_cache = self.protein_family.name
		self.mutation_type_name_cache = self.mutation_type.name
		self.diseases_name_cache = self.diseases.map{ |d| d.name.downcase }.sort.join(", ")
	end

	def self.renumber
		puts "Renumbering Mutation"
		objects = Mutation.joins(:protein_family).order("protein_families.ordering_cache, mutation")
		global_order = 0
		for object in objects do
			global_order += 1
			object.update_column("ordering", global_order)
			object.update_column("ordering_cache", object.protein_family.ordering_cache)
		end
	end

	include PgSearch
	pg_search_scope :search_full_text, 
	:against => [:mutation, :protein_family_name_cache, :pdbid, :comments,],
	:using => {
    	:tsearch => {:prefix => true}
    }
end

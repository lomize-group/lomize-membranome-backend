class ProteinType < ApplicationRecord
	has_many :protein_classes
	has_many :proteins

	has_many :complex_classifications

	def update_count
		ProteinType.reset_counters(self.id, :protein_classes)
		self.update_columns(:complex_structures_count => ComplexClassification.where(:protein_type_id => self.id).count)
	end

	def self.renumber
		puts "Renumbering ProteinType"
		new_num = 0.0
		objects = ProteinType.order("ordering")
		object_count = ProteinType.all.count
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
			new_order_cache = self.size_order_cache(new_num, object_count)
			object.update_column("ordering_cache", new_order_cache)
		end
	end

	include PgSearch
	pg_search_scope :search_full_text,
	against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"

end

class PathwaySubclass < ApplicationRecord
	belongs_to :pathway_class, counter_cache: true
	has_many :pathways

	def update_hierarchy
	    self.pathway_class_name_cache = self.pathway_class.name
	end

	def update_count
      PathwaySubclass.reset_counters(self.id, :pathways)
	end

	def self.renumber
		puts "Renumbering PathwaySubclass"
		global_order = 1
		cur_order = 1
		objects = PathwaySubclass.joins(:pathway_class).order("pathway_classes.ordering_cache ASC, ordering")
		object_count = PathwayClass.order(:pathway_subclasses_count => "DESC").first.pathway_subclasses_count
		cur_parent = objects.first.pathway_class
		for object in objects do
			if object.pathway_class.id != cur_parent.id
				cur_parent = object.pathway_class
				cur_order = 1
			end

			new_order_cache = cur_parent.ordering_cache + "." + self.size_order_cache(cur_order, object_count)

			object.update_column(:ordering, global_order)
			object.update_column(:ordering_cache, new_order_cache)

			global_order += 1
			cur_order += 1
		end
	end

	include PgSearch
	pg_search_scope :search_full_text,
	against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"

end

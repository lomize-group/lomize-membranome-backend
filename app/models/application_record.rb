class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  
  def self.search(query, search_options = {})
    if query.present?

      arr = false
      if search_options[:separator]
        query = query.split(search_options[:separator]).map(&:strip)
        if query.length > 1
          # use array search method
          arr = true
        else
          # just use single search term as string if there is only 1 search term
          query = query[0]
          
        end
      end

      if search_options[:search_by] == "limited"
        # limited search
        if arr
          return search_limited_array(query)
        end
        return search_limited(query)
      else
        # full text search
        if arr
          return search_full_text_array(query)
        end
        return search_full_text(query)
      end

    else
      # No query? Return all records, no particular order.
      return order("")
    end
  end

  def self.has_column?(column)
    return self.column_names.include?(column.to_s)
  end

  def self.size_order_cache(current_num, total_number)
    num_digits = total_number.to_i.to_s.length
    curr_digits = current_num.to_i.to_s.length
    result = "0" * (num_digits - curr_digits) + current_num.to_i.to_s
    return result
  end

  def self.update_cache(batch_size = 1000)
    # Do nothing if update_hierarchy is not overridden
    if self.instance_method(:update_hierarchy).owner == ApplicationRecord
      puts "No cache updates to be made for #{self.to_s}."
      return
    end
    count = self.all.count
    puts "Updating Cache for #{count} #{self.to_s}" 
    self.find_each(:batch_size => batch_size) do |object|
      object.update_hierarchy
      object.save
    end
    puts :json => self.first
    puts
  end

  def self.update_count(batch_size = 1000)
    # Do nothing if update_count is not overridden
    if self.instance_method(:update_count).owner == ApplicationRecord
      puts "No count updates to be made for #{self.to_s}.\n"
      return
    end
    puts "Updating count for #{self.to_s}"
    self.find_each(:batch_size => batch_size) do |object|
      object.update_count
      object.save
    end
  end

  #####
  # These methods are called during recache job.
  # Override them in the model if there is any cache to be updated.
  #####
  def self.renumber
    # Update ordering and ordering_cache for all objects.
    puts "Nothing to renumber for #{self.to_s}."
  end

  def update_hierarchy
    # Update hierarchy references and name caches for single object.
    # No hierarchy updates to be made by default
  end

  def update_count
    # Update counters for single object.
    # No count updates to be made by default
  end

end

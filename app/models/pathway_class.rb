class PathwayClass < ApplicationRecord
	has_many :pathway_subclasses
	has_many :pathways

	def update_count
		PathwayClass.reset_counters(self.id, :pathway_subclasses)
	end

	def self.renumber
		puts "Renumbering PathwayClass"
		global_order = 1
		objects = PathwayClass.order(:ordering)
		object_count = PathwayClass.count
		for object in objects do
			new_order_cache = self.size_order_cache(global_order, object_count)

			object.update_column(:ordering, global_order)
			object.update_column(:ordering_cache, new_order_cache)

			global_order += 1
		end
	end

	include PgSearch
	pg_search_scope :search_full_text,
	against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"

end

class PdbLink < ApplicationRecord
	belongs_to :protein, dependent: :destroy, counter_cache: true
end

class ComplexClassification < ApplicationRecord
	belongs_to :complex_structure
	belongs_to :protein_family
	belongs_to :protein_superfamily
	belongs_to :protein_class
	belongs_to :protein_type
end

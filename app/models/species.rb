class Species < ApplicationRecord
  has_many :proteins
  has_many :complex_structures

  def update_count
      Species.reset_counters(self.id, :proteins, :complex_structures)
  end

  def self.renumber
    puts "Renumbering Species"
		new_num = 0.0
		objects = Species.order("ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end
  end

  include PgSearch
  pg_search_scope :search_full_text,
    against: [:name, :name_common, :description],
    using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":trigram"

end
class MutationType < ApplicationRecord
	has_many :mutations

	def update_hierarchy
	    # probably dont need to do anything here
	    # highest point in the hierarchy for now
	end

	def update_count
      MutationType.reset_counters(self.id, :mutations)
	end

	include PgSearch
	pg_search_scope :search_full_text, against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"
end

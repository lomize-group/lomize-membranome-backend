class ComplexMembrane < ApplicationRecord
	belongs_to :membrane
	belongs_to :complex_structure

	def update_hierarchy
		self.membrane_name_cache = self.membrane.abbreviation
	end

	def update_count
		self.update_columns(:complex_structures_count_cache => self.membrane.complex_structures_count)
	end
	
end
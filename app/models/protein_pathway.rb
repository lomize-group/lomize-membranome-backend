class ProteinPathway < ApplicationRecord
	belongs_to :protein, counter_cache: true
	belongs_to :pathway

	def update_hierarchy
	    # update caches
	    self.pathway_name_cache = self.pathway.name
	    self.pathway_link_cache = self.pathway.link
	    self.pathway_dbname_cache = self.pathway.dbname

	    self.protein_name_cache = self.protein.name
	    self.protein_pdbid_cache = self.protein.pdbid
	end

	def self.renumber
		puts "Renumbering ProteinPathway"
		objects = ProteinPathway.order(:pathway_name_cache)
		global_order = 0
		for object in objects do
			global_order += 1
			object.update_column("ordering", global_order)
		end
	end

end

class ComplexPdbLink < ApplicationRecord
	belongs_to :complex_structure
	
	has_many :complex_pdb_uniprots

	def remove_duplicates
		# find all duplicates
		dups = ComplexPdbLink.where(
			:complex_id => self.complex_id,
			:pdb => self.pdb,
			:species_name => self.species_name
		).to_a

		# we want to keep one record of the duplicates
		dups.pop

		# remove remaining records
		dups.each do |m|
			m.delete
		end
	end

	def self.renumber
		puts "Renumbering ComplexPdbLink"
		objects = ComplexPdbLink.all.order(:pdb)
		global_order = 1
		for object in objects do
			object.update_columns(:ordering => global_order)
			global_order += 1
		end
	end

end
class Disease < ApplicationRecord
	has_and_belongs_to_many :mutations

	def update_hierarchy
	    # probably dont need to do anything here
	    # highest point in the hierarchy for now
	end

	def update_count
		# have to update count manually because it's many-to-many
		Disease.where(id: self.id).update_all('mutations_count = ' + mutations.count.to_s)
	end

	include PgSearch
	pg_search_scope :search_full_text, against: [:name],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"
end

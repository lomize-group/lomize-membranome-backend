class ProteinClass < ApplicationRecord
	belongs_to :protein_type, dependent: :destroy, counter_cache: true
	has_many :protein_superfamilies
	has_many :proteins

	has_many :complex_classifications

	def update_count
		ProteinClass.reset_counters(self.id, :protein_superfamilies)
		self.update_columns(:complex_structures_count => ComplexClassification.where(:protein_class_id => self.id).count)
	end

	def self.renumber
		puts "Renumbering ProteinClass"
		new_num = 0.0
		objects = ProteinClass.joins(:protein_type).order("protein_types.ordering_cache ASC, ordering")
		object_count = ProteinType.order("protein_classes_count DESC").first.protein_classes_count
		parent = objects.first.protein_type
		global_order = 1
		for object in objects do
			global_order += 1
			if object.protein_type.ordering_cache == parent.ordering_cache
				new_num += 1
			else
				parent = object.protein_type
				new_num = 1
			end
			object.update_column("ordering", global_order)
			new_order_cache = parent.ordering_cache + "." + self.size_order_cache(new_num, object_count)
			object.update_column("ordering_cache", new_order_cache)	
		end
	end

	include PgSearch
	pg_search_scope :search_full_text,
	against: [:name, :description],
	using: {
	  :trigram => {:threshold => 0.30},
	  :tsearch => {:prefix => true}
	},
	:ranked_by => ":trigram"
	
end

class Protein < ApplicationRecord
	has_many :protein_membranes
	has_many :protein_interactions
	has_many :complex_proteins

	belongs_to :protein_family, dependent: :destroy, counter_cache: true
	belongs_to :membrane, dependent: :destroy
	belongs_to :species, dependent: :destroy, counter_cache: true
	belongs_to :protein_superfamily
  	belongs_to :protein_class
  	belongs_to :protein_type

	has_many :domains
	has_many :pdb_links
	has_many :protein_pathways
	has_many :opm_links

	has_many :mutations


	def update_hierarchy
		#update pointers to upper level objects
		self.protein_superfamily = self.protein_family.protein_superfamily
		self.protein_class = self.protein_superfamily.protein_class
		self.protein_type = self.protein_class.protein_type

		# update caches
		self.species_name_cache = self.species.name
		self.membrane_name_cache = self.membrane.abbreviation
		self.protein_family_name_cache = self.protein_family.name
	end

	def update_count
      Protein.reset_counters(self.id, :domains, :pdb_links, :protein_pathways, :opm_links)
	end

	def self.renumber
		puts "Renumbering Protein"
		objects = Protein.joins(:protein_family).order("protein_families.ordering_cache, name")
		global_order = 1
		for object in objects do
			object.update_columns(
				:ordering => global_order,
				:ordering_cache => object.protein_family.ordering_cache
			)
			global_order += 1
		end
	end

	def self.search_limited(query)
		# search by pdbid if it was entered, otherwise search limited
		pdbid_result = self.search_pdbid(query)
		return pdbid_result.empty? ? search_limited_impl(query) : pdbid_result
	end

	def self.search_full_text(query)
		# search by pdbid if it was entered, otherwise search full text
		pdbid_result = self.search_pdbid(query)
		return pdbid_result.empty? ? search_full_text_impl(query) : pdbid_result
	end

	def self.search_pdbid(query)
		# for quick lookup when an exact pdbid is entered
		protein_pdbid_format = /^[\d\w]+\_[\d\w]+$/
		if protein_pdbid_format.match(query)
			return Protein.where("LOWER(pdbid) = ?", query.downcase)
		end

		# quickly determine no results if it's not a pdbid format
		return []
	end

	include PgSearch

	# single term search by prefix
	pg_search_scope :search_limited_impl,
	:against => [:name, :pdbid, :verification],
	:using => {
    	:tsearch => {:prefix => true}
	}
	
	# array search by any word
	pg_search_scope :search_limited_array,
	:against => [:name, :pdbid, :verification],
	:using => {
    	:tsearch => {:any_word => true}
    }
	
	# single term search by prefix
	pg_search_scope :search_full_text_impl, 
	:against => [:name, :pdbid, :uniprotcode, :genename, :comments, :verification, :altname],
	:associated_against => {
	    :domains => :protein_pdbid,
	    :pdb_links => :pdb,
	    :opm_links => :opm_link,
	    :protein_pathways => :pathway_name_cache
	},
	:using => {
    	:tsearch => {:prefix => true}
	}
	
	# array search by any word
	pg_search_scope :search_full_text_array, 
	:against => [:name, :pdbid, :uniprotcode, :genename, :comments, :verification, :altname],
	:associated_against => {
	    :domains => :protein_pdbid,
	    :pdb_links => :pdb,
	    :opm_links => :opm_link,
	    :protein_pathways => :pathway_name_cache
	},
	:using => {
    	:tsearch => {:any_word => true}
    }

end

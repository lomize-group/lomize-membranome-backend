class ComplexProtein < ApplicationRecord
	belongs_to :protein
	belongs_to :complex_structure

	validates :protein, :complex_structure, presence: true

	def update_hierarchy
		# update name cache
		self.protein_name_cache = self.protein.name
		self.complex_structure_name_cache = self.complex_structure.name
	end

end

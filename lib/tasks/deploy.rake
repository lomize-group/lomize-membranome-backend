
namespace :deploy do
  
  #invoke: rake deploy:from_scratch
  desc "This task deploys the website to heroku from from_scratch run [$ rake deploy:from_scratch] to deploy this brand new!"
  task :code do
    app = "lomize-group-membranome"
    heroku = "/usr/local/bin/heroku"
    cmd = "heroku git:remote -a " + app
    system cmd
    cmd = "git push heroku master"
    system cmd
  end

  desc "Runs your migrations on Heroku. You should run migrations locally first. Also [$rake deploy:code] first because migrations are included in code."
  task :migrations do
    app = 'lomize-group-membranome'
    heroku = '/usr/local/bin/heroku'

    cmd='rake data:backup_production'
    puts cmd
    system cmd

    cmd = heroku+' run rake db:migrate --app '+app
    puts cmd
    system cmd
  end

  desc "Backs up & deploys the app including your local database with its data. Usually run [$ rake data:pull] at some point before this."
  task :from_local do
    puts "======================================="
    puts "======================================="
    puts "WARNING: COPYING LOCAL DB TO PRODUCTION"
    puts "======================================="
    puts "======================================="

    app = 'lomize-group-membranome'
    heroku = '/usr/local/bin/heroku'
    local_db = 'lomize-membranome_development'
    remote_db = 'HEROKU_POSTGRESQL_BLUE'

    cmd="rake data:backup_production"
    puts cmd
    system cmd

    reset_db = heroku+' pg:reset --app '+app+' --confirm '+app
    push_db = heroku+' pg:push '+local_db+' '+remote_db+' --app '+app
    set_env = heroku+' run rake db:environment:set'

    cmd = 'git push heroku master && '+reset_db+' && '+push_db+' && '+set_env
    puts cmd
    system cmd
  end

  #DON'T WANT TO USE THIS RIGHT NOW BECAUSE SEED IS OUT OF DATE.
  #RUNNING THIS WILL OVERWRITE NEW IN MEMBRANOME PRODUCTION.
  # desc "This task deploys the website to heroku from from_scratch run [$ rake deploy:from_scratch] to deploy this brand new!"
  # task :from_seed do
  #   puts "=================================="
  #   puts "=================================="
  #   puts "WARNING: RESETTING FROM SEED FILE"
  #   puts "=================================="
  #   puts "=================================="
  #   app = "lomize-group-membranome"
  #   heroku = "/usr/local/bin/heroku"

  #   reset_db = heroku+' pg:reset --app '+app+' --confirm '+app+'';
  #   migrate_db = heroku+' run rake db:migrate --app '+app
  #   seed_db = heroku+' run rake db:seed --app '+app
  #   redo_cache = heroku+' run rake data:recache'
    
  #   #set figaro variables
  #   # set_config_vars = 'figaro heroku:set -e production --app ' + app
  #   set_config_vars = " echo done"
  #   cmd = "heroku git:remote -a " + app
  #   system cmd
  #   cmd = "git push heroku master && " + reset_db+' && '+migrate_db+' && '+seed_db +' && '+redo_cache 
  #   puts cmd
  #   system cmd
  # end

  desc "This task tests the website in prod mode on local [$ rake deploy:test_prod]"
  task :test_prod do

    cmd = 'bin/rails server -p 13314 -e production'
    puts cmd
    system cmd

  end

end
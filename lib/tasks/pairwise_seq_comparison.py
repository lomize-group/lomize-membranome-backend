import psycopg2
import requests
import os
import shutil
import subprocess
from pathlib import Path

import pandas as pd

# You may need to set the dbname accordignly and specify the user if needed
def get_all_pdbid(dbname="lomize-membranome_development", output_file="membranome_uniprotcodes.txt"):
    proteins_dict = {}
    proteins_list = []
    # Connect to the database
    conn = psycopg2.connect(
        dbname=dbname,
    )
    # Create a cursor
    cursor = conn.cursor()
    query = "SELECT uniprotcode, id, protein_superfamily_id, protein_class_id FROM proteins"
    cursor.execute(query)
    results = cursor.fetchall()
    # Close the cursor and connection
    cursor.close()
    conn.close()
    for row in results:
        proteins_list.append(row[0])
        proteins_dict[row[0]] = {
            "class_id": row[3],
            "superfamily_id": row[2],
        }
    return proteins_list, proteins_dict
    
    # Write the results to a file
    # with open(output_file, 'a') as f:
    #     for row in results:
    #         f.write(str(row[0]) + '\n')

def batch_list(list, batch_size):
    for i in range(0, len(list), batch_size):
        yield list[i:i + batch_size]

def get_all_fasta(proteins_list, fasta_file):
    """Requests all Fasta sequences for the proteins in batches of 25"""
    i = 0
    batch_size = 100
    for batch in batch_list(proteins_list, batch_size):
        query = " OR ".join([f"accession:{acc}" for acc in batch])
        
        url=f"http://rest.uniprot.org/uniprotkb/stream?format=fasta&query=({query})"
        response = requests.get(url)
        
        if response.status_code == 200:
            res = response.content.decode("utf-8")
            num_proteins = res.count('>')
            print(f"Batch {i + 1}: {num_proteins} proteins returned.")
            with open(fasta_file, "a") as f:
                f.write(response.text)
            i += 1
            print(f"Batch {i} of {len(proteins_list)/batch_size} downloaded successfully")
        else:
            print(f"Error: {response.status_code}")

def create_blast_db(fasta_file, blast_db_name):
    make_blast_cmd = [
        "makeblastdb",
        "-in", str(fasta_file),
        "-dbtype", "prot",
        "-out", str(blast_db_name),
    ]
    print(f"Running command: {' '.join(make_blast_cmd)}")
    subprocess.run(make_blast_cmd, check=True)
    print(f"BLAST database {blast_db_name} created successfully")

def run_blastp(fasta_file, blast_db_name, all_vs_all):
    blast_cmd = [
        "blastp",
        "-query", str(fasta_file),
        "-db", str(blast_db_name),
        "-out", str(all_vs_all),
        "-outfmt", "6",
        "-evalue", "1e-5",
        "-num_threads", "10",
        "-max_hsps", "1",
    ]
    print(f"Running command: {' '.join(blast_cmd)}")
    subprocess.run(blast_cmd, check=True)
    print(f"BLAST search completed successfully")

def filter_blast_results(all_vs_all, proteins_dict, filtered_results, class_mistmatches, superfamily_mismatches):
    """Filter the BLAST results based on the identity threshold.
    The identity threshold is set to 35% Then use the filtered results for further processing."""
    identity_threshold = 35
    columns = ['qseqid', 'sseqid', 'pident', 'length', 'mismatch', 'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore']
    df = pd.read_csv(all_vs_all, sep='\t', names=columns)
    filtered_df = df[df['pident'] >= identity_threshold]
    
    # this is for debugging purposes
    filtered_df.to_csv(filtered_results, sep='\t', index=False, header=True)
    
    # open the files for writing (don't forget to close)
    class_header = "qseqid\tq_class_id\tsseqid\ts_class_id\tpident\tlength\tqstart\tqend\tsstart\tsend\n"
    superfamily_header = "qseqid\tq_superfamily_id\tsseqid\ts_superfamily_id\tpident\tlength\tqstart\tqend\tsstart\tsend\n"
    class_f = open(class_mistmatches, "w")
    superfamily = open(superfamily_mismatches, "w")
    class_f.write(class_header)
    superfamily.write(superfamily_header)
    
    for index, row in filtered_df.iterrows():
        # maybe defer some of these until they are needed
        qseqid = row['qseqid'] # query sequence id 
        sseqid = row['sseqid'] # subject sequence id
        pident = row['pident'] # percentage identity
        qstart = row['qstart'] # query start
        qend = row['qend'] # query end
        sstart = row['sstart'] # subject start
        send = row['send'] # subject end
        length = row['length'] # alignment length
        
        # get the Uniprot codes for protein_dict lookup
        q_code = qseqid.split("|")[1]
        s_code = sseqid.split("|")[1]
        # get the class and superfamily ids from the database
        q_prot = proteins_dict[q_code]
        s_prot = proteins_dict[s_code]
        if q_prot["class_id"] != s_prot["class_id"]:
            class_f.write(f"{qseqid}\t{q_prot["class_id"]}\t{sseqid}\t{s_prot["class_id"]}\t{pident}\t{length}\t{qstart}\t{qend}\t{sstart}\t{send}\n")
        if q_prot["superfamily_id"] != s_prot["superfamily_id"]:
            superfamily.write(f"{qseqid}\t{q_prot["superfamily_id"]}\t{sseqid}\t{s_prot["superfamily_id"]}\t{pident}\t{length}\t{qstart}\t{qend}\t{sstart}\t{send}\n")
    
    class_f.close()
    superfamily.close()
        
    

def main():
    root_dir = Path(__file__).resolve().parent.parent.parent
    download_dir = root_dir / "public/pairwise_seq_comparison"
    fasta_fname = "proteins.fasta"
    fasta_file = os.path.join(download_dir, fasta_fname)
    blast_db_name = download_dir / "membranome_proteins"
    all_vs_all = download_dir / "all_vs_all.csv"
    filtered_results = download_dir / "filtered_results.csv"
    class_mistmatches = download_dir / "class_mistmatches.csv"
    superfamily_mismatches = download_dir / "superfamily_mismatches.csv"
    if os.path.exists(download_dir):
        shutil.rmtree(download_dir)
    os.makedirs(download_dir)
    proteins_list, proteins_dict = get_all_pdbid()
    get_all_fasta(proteins_list, fasta_file)
    create_blast_db(fasta_file, blast_db_name)
    run_blastp(fasta_file, blast_db_name, all_vs_all)
    filter_blast_results(all_vs_all, proteins_dict, filtered_results, class_mistmatches, superfamily_mismatches)
    
    
if __name__ == "__main__":
    main()


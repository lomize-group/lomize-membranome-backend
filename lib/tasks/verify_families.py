import requests
import os
import psycopg2
import shutil

def download_entry_list(download_dir):
    # Download the list of entries
    # from the database
    # and save it to a file
    url = "https://ftp.ebi.ac.uk/pub/databases/interpro/current_release/entry.list"
    file_name = "entry_list"
    file_path = os.path.join(download_dir, file_name)
    print("Downloading the list of entries")
    
    response = requests.get(url)
    response.raise_for_status() # Raise an exception if the request is unsuccessful
    
    family_interpro_ids = set()
    
    for line in response.text.splitlines():
        parts = line.split("\t")
        if len(parts) >= 3: # check if 
            entry_ac = parts[0]
            entry_type = parts[1]
            if entry_type == "Family":
                family_interpro_ids.add(entry_ac)
                
    # Maybe put this in a logger
    print(f"Found {len(family_interpro_ids)} families")
                
    # We don't need this file but it can be useful for debugging
    with open(file_path, 'w') as file:
        for family_id in family_interpro_ids:
            file.write(family_id + "\n")
            
    
    # we don't need to save the file but we can for debugging purposes
    with open(file_path, 'w') as file:
        file.write(response.text)
        
    print("Download complete")
    
    
    return family_interpro_ids

def connect_to_db():
    try:
        hostname = 'localhost'
        database = "lomize-membranome_development"
        username = "alexeykovalenko"
        password = ""
        port = 5432
        
        connection = psycopg2.connect(
            host=hostname,
            database=database,
            user=username,
            password=password,
            port=port
        )
    except Exception as e:
        print("Error while connecting to the database: ", e)
    return connection

def get_proteins_from_db():
    """Gets UniprotCode UniprodID 
        from table proteins:
            UniprodID = pdbid in the database
            UniprotCode = uniprotcode in the database
            Protein_family_id - protein family number in the database
        from table protein_families:
            Protein_family_id = id in the database
            interpro_id = interpro in the database
    """
    connection = connect_to_db()
    
    cursor = connection.cursor()
    
    cursor.execute("SELECT uniprotcode, pdbid, protein_family_id FROM proteins")
    proteins = cursor.fetchall()
    cursor.execute("SELECT id, interpro FROM protein_families")
    interpros = cursor.fetchall()
    
    interpro_dict = {}
    for interpro in interpros:
        interpro_dict[interpro[0]] = interpro[1]
    
    proteins_dict = {}
    
    for protein in proteins:
        proteins_dict[protein[0]] = {
            "uniprotid": protein[1],
            "family_id": protein[2],
            "interpro": interpro_dict[protein[2]]
        }

    # Clean up close connections
    cursor.close()
    connection.close()
    print("PostgreSQL connection is closed")
    
    return proteins_dict


# Function to fetch accession numbers, entry names, and InterPro codes for specific UniProt accessions
def fetch_accession_entry_interpro_in_batches(uniprot_ids, batch_size=25):
    base_url = "https://rest.uniprot.org/uniprotkb/search"
    uniprot_dict = {}
    # Split uniprot_ids into smaller batches
    for i in range(0, len(uniprot_ids), batch_size):
        batch = uniprot_ids[i:i + batch_size]
        query = " OR ".join([f"accession:{uid}" for uid in batch])
        
        # API URL with query for accession numbers, entry names, and InterPro codes in TSV format
        url = f"{base_url}?query={query}&fields=accession,xref_interpro,id&format=tsv"
        
        print(f"Results for batch {i // batch_size + 1}:\n")
        # Send the request to the UniProt API
        response = requests.get(url)
        
        if response.status_code == 200:
            # Print the results for the current batch
            # debudding purposes
            # print(batch)
            # print(response.text)
            # j = 0
            for line in response.text.splitlines():
                # j += 1
                parts = line.split("\t")
                uniprot_dict[parts[0]] = {
                    "entry_name": parts[2],
                    "interpro": parts[1]
                }
            # if j != batch_size+1:
            #     print(f"Found {j} entries\n")
        else:
            print(f"Failed to fetch data for batch {i // batch_size + 1}. Status code: {response.status_code}")
        
    return uniprot_dict

def filter_interpro_ids(uniprot_dict, family_interpro_ids, download_dir):
    filtered_proteins = {}
    orphans_file = os.path.join(download_dir, "orphans")
    with open(orphans_file, 'w') as file:
        for key, value in uniprot_dict.items():
            interpros = value["interpro"].split(";")
            for interpro in interpros:
                if interpro in family_interpro_ids:
                    filtered_proteins[key] = {
                        "entry_name": value["entry_name"],
                        "interpro": interpro
                    }
                    break
            else:
                # Write the orphan proteins to a file
                file.write(f"{key}\t{value}\n")
                filtered_proteins[key] = {
                    "entry_name": value["entry_name"],
                    "interpro": ""
                }
    return filtered_proteins 
    
def verify_families(proteins, filtered_proteins, download_dir):
    errors = []
    memb_missing_family = []
    family_missing_both = []
    only_memb_has_family = []
    family_mismatch = []

    # Loop through the proteins and check for matches and mismatches
    for key, protein in proteins.items():
        uniprot_match = filtered_proteins.get(key)
        
        if uniprot_match is None:
            errors.append(f"Uniprot ID {key} not found in the UniprotKB")
            continue

        if uniprot_match["entry_name"] != protein["uniprotid"]:
            errors.append(f"Uniprot ID {key} {protein['uniprotid']} has a different Uniprot ID in the UniprotKB {uniprot_match['entry_name']}  Interpro for sorting  {protein['interpro']}")
        
        # Compare InterPro IDs
        if protein["interpro"] == "":
            if uniprot_match["interpro"] != "":
                memb_missing_family.append(f"Uniprot ID {key} {protein['uniprotid']} missing family {protein['interpro']}. Uniprot classified {uniprot_match['interpro']}")
            else:
                family_missing_both.append(f"Uniprot ID {key} {protein['uniprotid']} missing both family in both {protein['interpro']} {uniprot_match['interpro']}")
        elif uniprot_match["interpro"] == "":
            only_memb_has_family.append(f"Uniprot ID {key} {protein['uniprotid']} only has family {protein['interpro']} in the database but not in Uniprot {uniprot_match['interpro']}.  Interpro for sorting  {protein['interpro']}")
        elif protein["interpro"] != uniprot_match["interpro"]:
            family_mismatch.append(f"Uniprot ID {key} {protein['uniprotid']} has family {protein['interpro']} in the database and {uniprot_match['interpro']} in UniprotKB. Interpro for sorting {protein['interpro']}")

    # Sort by InterPro code (assuming InterPro codes are embedded in the output text)
    memb_missing_family.sort(key=lambda x: x.split()[-1])  # Assuming InterPro is at the end of the string
    family_missing_both.sort(key=lambda x: x.split()[-1])
    only_memb_has_family.sort(key=lambda x: x.split()[-1])
    family_mismatch.sort(key=lambda x: x.split()[-1])

    # Write sorted outputs to files
    errors_file = os.path.join(download_dir, "errors")
    memb_missing_family_file = os.path.join(download_dir, "memb_missing_family")
    family_missing_both_file = os.path.join(download_dir, "family_missing_both")
    only_memb_has_family_file = os.path.join(download_dir, "only_memb_has_family")
    family_mismatch_file = os.path.join(download_dir, "family_mismatch")

    # Write errors to file
    with open(errors_file, 'w') as file:
        for line in errors:
            file.write(line + "\n")
    
    # Write sorted results to the respective files
    with open(memb_missing_family_file, 'w') as file:
        for line in memb_missing_family:
            file.write(line + "\n")

    with open(family_missing_both_file, 'w') as file:
        for line in family_missing_both:
            file.write(line + "\n")

    with open(only_memb_has_family_file, 'w') as file:
        for line in only_memb_has_family:
            file.write(line + "\n")

    with open(family_mismatch_file, 'w') as file:
        for line in family_mismatch:
            file.write(line + "\n")

        


def main():
    '''This program will output the following files:
        orphans - proteins that don't have a family in the UniprotKB
        errors - proteins that don't have a matching Uniprot ID
        memb_missing_family - proteins that have a family in the database but not in the UniprotKB
        family_missing_both - proteins that don't have a family in the database or UniprotKB
        only_memb_has_family - proteins that only have a family in the database but not in the UniprotKB
        family_mismatch - proteins that have a different family in the database and UniprotKB
    '''
    download_dir = "../../public/verify_families"
    if os.path.exists(download_dir):
        shutil.rmtree(download_dir)    
    os.makedirs(download_dir)
    family_interpro_ids = download_entry_list(download_dir)
    proteins = get_proteins_from_db()
    
    # for key, protein in proteins.items():
    #     print(f"Key: {key} protein: {protein}")
    
    # uniprot_ids = ["P12345", "Q67890", "O98765"]  # Add your list of UniProt IDs here
    uniprot_ids = list(proteins.keys())
    # Fetch and print the accession numbers, entry names (ID), and InterPro codes for the provided entries in batches
    uniprot_dict = fetch_accession_entry_interpro_in_batches(uniprot_ids, batch_size=25)
    
    filtered_proteins = filter_interpro_ids(uniprot_dict, family_interpro_ids, download_dir)
    
    print(f"Found {len(filtered_proteins)} proteins in the families")
    
    # filtered_proteins with Uniprot classifications are compared with the database
    verify_families(proteins, filtered_proteins, download_dir)
    
    # Verify the families
    # in the list
    pass

if __name__ == '__main__':
    main()
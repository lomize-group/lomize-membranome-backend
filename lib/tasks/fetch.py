from concurrent.futures import ThreadPoolExecutor, as_completed
from urllib.request import urlopen
import sys
import json

NUM_WORKERS = 20

def make_request(url, key=''):
	try: return [key, urlopen(url).read().decode('utf-8')]
	except: return [key, "error"]

def main():
	# in ruby script use find_in_batche
	batch_size = 0
	try: batch_size = int(sys.argv[1])
	except: sys.exit("specify an integer batch size:\n\tpython3 fetch.py <batch_size>")

	json_separator = '\n'
	if len(sys.argv) > 2: json_separator = sys.argv[2]
	
	executor = ThreadPoolExecutor(max_workers=NUM_WORKERS)
	waiting = set()
	for _ in range(batch_size):
		line = input().split()
		waiting.add(executor.submit(make_request, line[1], line[0]))

	for w in as_completed(waiting):
		result = [x.strip() for x in w.result()]
		print(' '.join(result), end=json_separator)


if __name__ == '__main__':
	main()

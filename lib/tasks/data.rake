namespace :data do
	desc "pulls the production data from heroku"
	task :pull do
	  app = 'lomize-group-membranome'
	  remote_db = 'DATABASE_URL'
	  local_dev_db = 'lomize-membranome_development'

	  drop_db = 'rake db:drop'
	  set_env = 'rake db:environment:set'
	  pull_db = 'heroku pg:pull '+remote_db+' '+local_dev_db+' --app '+app
	  clone_test_db = 'rake db:test:prepare'
	  
	  cmd = drop_db+' && '+pull_db+' && '+set_env+' && '+clone_test_db
	  puts cmd
	  system cmd
	end

	task :backup_production do
		puts "============================================"
		puts "============================================"
		puts "BACKING UP & DOWNLOADING PRODUCTION DATABASE"
		puts "(saving as lastest.dump)"
		puts "============================================"
		puts "============================================"

		app = 'lomize-group-membranome'
		heroku = '/usr/local/bin/heroku'

		# save backup postgres dump locally just in case
		# this is also saved for a limited time in heroku
		cmd = 'rm latest.dump'
		system cmd
		cmd = heroku+' pg:backups:capture --app '+app
		system cmd
		cmd = heroku+' pg:backups:download'
		system cmd
	end

	# desc "rebuilds the data from migrations"
	# task :rebuild do
		
	# 	drop_db = "rake db:drop"
	# 	create_db = "rake db:create"
	# 	migrate_db = "rake db:migrate"
	# 	seed_db = "rake db:seed"
	# 	hierarchy_cache = "rake data:update_caches"
	# 	standardize_numbers = "rake data:renumber"

	# 	cmd = drop_db+' && '+create_db+' && '+migrate_db +' && '+seed_db +' && '+ hierarchy_cache +' && '+ standardize_numbers 
	# 	puts cmd
	# 	system cmd
	# end

	desc "Renumbers data per new ordering"
	task renumber: :environment do
		# membrane
		Membrane.renumber

		# species
		Species.renumber

		# pathway
		PathwayClass.renumber
		PathwaySubclass.renumber
		Pathway.renumber

		# proteins
		ProteinType.renumber
		ProteinClass.renumber
		ProteinSuperfamily.renumber
		ProteinFamily.renumber
		Protein.renumber
		ProteinMembrane.renumber
		PdbLink.renumber

		# complexes
		ComplexClassification.renumber
		ComplexMembrane.renumber
		ComplexStructure.renumber
		ComplexProtein.renumber
		Subcomplex.renumber
		ComplexPdbLink.renumber
		ComplexPdbUniprot.renumber
		ComplexUniprotLink.renumber

		# mutation
		MutationType.renumber
		Disease.renumber
		Mutation.renumber		

		# protein pathway
		ProteinPathway.renumber

		# others
		Domain.renumber
		OpmLink.renumber
	end


	desc "Remakes all hierarchy and cached data"
	task update_caches: :environment do
		# membrane
		Membrane.update_cache

		# species
		Species.update_cache

		# pathways
		PathwayClass.update_cache
		PathwaySubclass.update_cache
		Pathway.update_cache

		# proteins
		ProteinType.update_cache
		ProteinClass.update_cache
		ProteinSuperfamily.update_cache
		ProteinFamily.update_cache
		Protein.update_cache
		ProteinMembrane.update_cache
		PdbLink.update_cache

		# complex
		ComplexClassification.update_cache
		ComplexMembrane.update_cache
		ComplexStructure.update_cache
		ComplexProtein.update_cache
		Subcomplex.update_cache
		ComplexPdbLink.update_cache
		ComplexPdbUniprot.update_cache
		ComplexUniprotLink.update_cache
		
		# protein interaction
		ProteinInteraction.update_cache

		# mutation
		MutationType.update_cache
		Disease.update_cache
		Mutation.update_cache

		# protein pathway
		ProteinPathway.update_cache

		# others
		Domain.update_cache
		OpmLink.update_cache
	end

	desc "Remakes all hierarchy-based cached data"
	task update_counts: :environment do
		# membrane
		Membrane.update_count

		# species
		Species.update_count

		# pathway
		PathwayClass.update_count
		PathwaySubclass.update_count
		Pathway.update_count

		# protein
		ProteinType.update_count
		ProteinClass.update_count
		ProteinSuperfamily.update_count
		ProteinFamily.update_count
		Protein.update_count
		ProteinMembrane.update_count
		PdbLink.update_count

		# complex
		ComplexClassification.update_count
		ComplexMembrane.update_count
		ComplexStructure.update_count
		ComplexProtein.update_count
		Subcomplex.update_count
		ComplexPdbLink.update_count
		ComplexPdbUniprot.update_count
		ComplexUniprotLink.update_count

		# mutation
		MutationType.update_count
		Disease.update_count
		Mutation.update_count

		# others
		Domain.update_count
		OpmLink.update_count
	end


	desc "aggregator for update_hierarchy and renumber"
	task recache: :environment do
		Rake::Task["data:update_caches"].invoke
		Rake::Task["data:renumber"].invoke
		Rake::Task["data:update_counts"].invoke
	end


	desc "remove duplicate entries from tables"
	task remove_duplicates: :environment do
		puts "Removing duplicates for ComplexPdbLink"
		ComplexPdbLink.find_each(:batch_size => 1000) do |object|
		  object.remove_duplicates
		end

		puts "Removing duplicates for ProteinInteraction"
		ProteinInteraction.find_each(:batch_size => 1000) do |object|
		  object.remove_duplicates
		end
	end

end

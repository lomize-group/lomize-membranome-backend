namespace :data do
	EBI_BASE_URL = "https://www.ebi.ac.uk/proteins/api/proteins/"
	OPM_BASE_URL = "https://lomize-group-opm.herokuapp.com/primary_structures/pdbid/"
	module DataHelper
		class Counter
			def initialize(count_start = 0)
				@current_count = count_start
			end

			def print_increment(incr_amount=1, every_n=1, suffix="records done")
				@current_count += incr_amount
				if @current_count % every_n == 0
					puts "@@@ #{@current_count.to_s} #{suffix}"
				end
			end

			def count
				@current_count
			end
		end

		class FetchScriptExecutor
			def initialize(_json_sep = "\n")
				@input_batch = []
				@json_sep = _json_sep
			end

			def add_input(line)
				@input_batch << line
			end

			def run()
				self.run_script do |io|
					@input_batch.each do |line|
						io.puts line.is_a?(Array) ? line.join(" ") : line
					end
					return io.read.split(@json_sep)
				end
			end

			def run_script()
				python = "python3"
				fetch_script = File.join Rails.root, "lib", "tasks", "fetch.py"
				yield IO.popen("#{python} #{fetch_script} #{@input_batch.count} #{@json_sep}", "r+")
			end
		end

		def self.parse_json(data)
			require 'json'
			begin
				return JSON.parse(data)
			rescue JSON::ParserError
				return nil
			end
		end

		def self.get_data(url, params = nil)
			require 'net/http'
			require 'json'

			uri = URI(url)
			if !params.nil?
				uri.query = URI.encode_www_form(params)
			end
			res = Net::HTTP.get_response(uri)
			data = nil
			begin
				data = JSON.parse(res.body)
			rescue
				data = nil
			end
			return data
		end
	end
end

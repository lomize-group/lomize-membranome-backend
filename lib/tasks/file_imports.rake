namespace :import do
	namespace :import_mutations do
		task from_file: :environment do
			require 'csv'
			puts "Resetting Mutations table"
			cmd = "rake import:mutations:reset"
			system cmd

			puts "Reading pipe separated Mutations file"
			input_filename = 'mutations.txt'
			input_path = File.join Rails.root, "db", input_filename
			rows = []
			id = 0
			File.open(input_path, "r") do |f|
				CSV.foreach(f, col_sep:"|", :headers => :first_row) do |row|
					mutation = Hash.new
					mutation["pdbid"] = row[0]&.strip
					mutation["ordering"] = 0
					mutation["ordering_cache"] = row[1]&.strip.chomp(".")
					mutation["resolution"] = row[2]&.strip
					mutation["mutation"] = row[3]&.strip
					mutation_type_name = row[4]&.strip
					mutation["mutation_type"] = MutationType.find_by(name: mutation_type_name)
					mutation["comments"] = row[5]&.strip
					disease_arr = row[6]&.split(",")
					disease_arr&.map! {|item| item.strip.downcase}
					diseases = nil
					if !disease_arr.nil? && disease_arr.size > 0
						diseases = Disease.where("lower(name) in (?)", disease_arr)
					end

					mutation["protein_family"] = ProteinFamily.find_by(ordering_cache: mutation["ordering_cache"])
					if !mutation["protein_family"].nil?
						mutation["protein_superfamily"] = mutation["protein_family"].protein_superfamily
						mutation["protein_class"] = mutation["protein_superfamily"].protein_class
						mutation["protein_type"] = mutation["protein_class"].protein_type
					end
	      	mutation["source"] = row[7]&.strip
	      	mutation["protein"] = Protein.find_by(pdbid: mutation["pdbid"])

	      	mutation_record = Mutation.create!(mutation)
	      	diseases.each do |d|
	      		mutation_record.diseases << d
	      	end

	      	mutation_record.save
				end
			end
			puts "Done adding mutations to database"
		end

		task reset: :environment do
			puts "Removing all Mutation records"
			Mutation.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:mutations)
		end

		task find_bad_families: :environment do
			require 'csv'

			input_filename = 'mutations.txt'
			input_path = File.join Rails.root, "db", input_filename

			rows = []
			count = 0
			puts "Finding Protein Families from mutations.txt not listed in Membranome ..."
			File.open(input_path, "r") do |f|
				CSV.foreach(f, col_sep:"|", :headers => :first_row) do |row|
					family_number = row[1].strip.chomp(".")
			      	family = ProteinFamily.find_by(ordering_cache: family_number)
			      	if family.nil?
			      		puts family_number
			      		count += 1
			      	end
				end
				puts "Families not found: "+count.to_s
			end
		end
	end

	namespace :import_pathways do
		task from_file: :environment do
			require 'csv'

			puts "Removing all pathway, pathway subclass, and pathway class records"
			Pathway.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:pathways)
			PathwaySubclass.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:pathway_subclasses)
			PathwayClass.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:pathway_classes)
			
			classes_filepath = File.join Rails.root, "db", "pathway_classes.txt"
			classes_file = File.open classes_filepath, "r"

			subclasses_filepath = File.join Rails.root, "db", "pathway_subclasses.txt"
			subclasses_file = File.open subclasses_filepath, "r"

			pathways_filepath = File.join Rails.root, "db", "pathways.txt"
			pathways_file = File.open pathways_filepath, "r"

			puts "Importing pathway classes from #{classes_filepath}"
			ordering = 1
			CSV.foreach(classes_filepath, :col_sep => "|", :headers => false) do |row|
				ordering = row[0].to_f
				class_name = row[1].strip

				PathwayClass.create!(
					:ordering => ordering,
					:name => class_name
				)

				ordering += 1
			end

			puts "Importing pathway subclasses from #{subclasses_filepath}"
			ordering = 1
			CSV.foreach(subclasses_file, :col_sep => "|", :headers => false) do |row|
				subclass_name = row[1].strip
				class_name = row[2].strip.downcase
				class_id = PathwayClass.where("LOWER(name) = '#{class_name}'").first.id

				PathwaySubclass.create!(
					:ordering => ordering,
					:name => subclass_name,
					:pathway_class_id => class_id
				)

				ordering += 1
			end

			puts "Importing pathways from #{pathways_filepath}"
			ordering = 1
			CSV.foreach(pathways_file, :col_sep => "|", :headers => false) do |row|
				link = row[1].strip
				pathway_name = row[2].strip
				dbname = row[3].strip
				subclass_name = row[4].strip.downcase
				subclass = PathwaySubclass.where("LOWER(name) = '#{subclass_name}'").first

				# replace symbols such as "&alpha;", "&gamma;" in pathway name
				parts = pathway_name.match(/(&)(\w+)(;)/)
				if !parts.nil?
					full_match = parts[0]
					text = parts[2]
					text[0] = text[0].upcase
					pathway_name = pathway_name.gsub(parts[0], text)
				end
				# replace html tags such as "<sub>", "</sub>" in pathway name
				pathway_name = pathway_name.gsub(/<\/?\w+>/, '').strip

				pathway = Pathway.create!(
					:ordering => ordering,
					:link => link,
					:name => pathway_name,
					:dbname => dbname,
					:pathway_class_id => subclass.pathway_class_id,
					:pathway_subclass_id => subclass.id
				)

				# match protein pathway by link to create association
				protein_pathways = ProteinPathway.where("LOWER(pathway_link_cache) = '#{link.downcase}'")
				protein_pathways.each do |pp|
					pp.pathway_id = pathway.id
					pp.save!
				end
				
				ordering += 1
			end

			# fix duplicate names by adding "-n" where n is the number of time it appeared
			# (keep the first one without a "-n")
			dup_name_pws = Pathway.select(:name).group(:name).having("COUNT(*) > 1")
			dup_name_pws.each do |d|
				count = 1
				Pathway.where(:name => d.name).order(:id).each do |pw|
					if count > 1
						pw.name = pw.name + "-#{count}"
					end
					pw.save!
					count += 1
				end
			end
		end
		task remove_unmatched_pathways: :environment do
			puts "Finding and removing unmatched ProteinPathway"
			unmatched_pp = ProteinPathway.where(:pathway_id => nil)
			unmatched_pp.each do |pp|
				puts "#{pp.id} #{pp.pathway_name_cache} (#{pp.pathway_dbname_cache})"
			end
			unmatched_pp.destroy_all
		end
	end

	namespace :import_interactions do
		task from_file: :environment do
			require "csv"

			puts "Removing all #{ProteinInteraction.all.count} ProteinInteraction records"
			ProteinInteraction.delete_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:protein_interactions)

			interactions_filepath = File.join Rails.root, "db", "protein_interactions.txt"
			interactions_file = File.open interactions_filepath, "r"

			puts "Importing ProteinInteraction from #{interactions_filepath}"
			CSV.foreach(interactions_file, :col_sep => "|", :headers => false) do |row|
				protein1_uniprotcode = row[1]
				protein2_uniprotcode = row[2]
				complex = row[3]
				pubmed = row[4]
				experiment_type = row[5]
				interaction_type = row[6]
				dbname = row[7]

				protein1_id = Protein.find_by(:uniprotcode => protein1_uniprotcode).id
				protein2_id = Protein.find_by(:uniprotcode => protein2_uniprotcode).id


				num_proteins = protein1_id == protein2_id ? 1 : 2

				params = {
					:complex => complex,
					:pubmed => pubmed,
					:primary_protein_id => protein1_id,
					:related_protein_id => protein2_id,
					:dbname => dbname,
					:experiment_type => experiment_type,
					:interaction_type => interaction_type,
					:bitopic_protein_count => num_proteins
				}
				ProteinInteraction.create!(params)

				if protein1_id != protein2_id
					params[:primary_protein_id] = protein2_id
					params[:related_protein_id] = protein1_id
					ProteinInteraction.create!(params)
				end
			end
			puts "Done (#{ProteinInteraction.all.count} records imported)"
		end
	end

	namespace :complexes do
		task from_file: :environment do
			require "csv"
			data_filepath = File.join Rails.root, "db", "complex_import"

			# remove old records and reset id counter to 1
			puts "Removing all ComplexProtein records"
			ComplexProtein.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_proteins)
			puts "Removing all ComplexMembrane records"
			ComplexMembrane.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_membranes)
			puts "Removing all ComplexClassification records"
			ComplexClassification.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_classifications)
			puts "Removing all Subcomplex records"
			Subcomplex.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:subcomplexes)
			puts "Removing all ComplexPdbUniprot records"
			ComplexPdbUniprot.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_pdb_uniprots)
			puts "Removing all ComplexPdbLink records"
			ComplexPdbLink.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_pdb_links)
			puts "Removing all ComplexUniprotLink records"
			ComplexUniprotLink.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_uniprot_links)
			puts "Removing all ComplexStructure records"
			ComplexStructure.destroy_all
			ActiveRecord::Base.connection.reset_pk_sequence!(:complex_structures)

			# add new complexes
			complex_filepath = File.join data_filepath, "complexes.txt"
			complex_file = File.open complex_filepath, "r"
			puts "Importing ComplexStructure from #{complex_filepath}"
			CSV.foreach(complex_file, :col_sep => "|", :headers => false, :quote_char => "\x00") do |row|
				complex_name = row[0].to_s.strip
				numsubunits = complex_name.count(":") + 1
				comments = row[1].to_s.strip
				source_db = row[2].to_s.strip
				source_db_link = row[3].to_s.strip
				pubmed = row[4].to_s.strip
				refpdbcode = row[5].to_s.strip.downcase

				next if ComplexStructure.exists?(:name => complex_name)
				
				ComplexStructure.create!(
					:name => complex_name,
					:numsubunits => numsubunits,
					:comments => comments,
					:source_db => source_db,
					:source_db_link => source_db_link,
					:pubmed => pubmed,
					:refpdbcode => refpdbcode,

					# get from separate file
					:thickness => 0.0,
					:thicknesserror => 0.0,
					:tilt => 0,
					:tilterror => 0,
					:gibbs => 0.0,

					# need to import ComplexProtein before setting this
					:species_id => nil
				)
			end
			complex_file.close

			# import thickness, tilt, gibbs for complexes that were previously in the database
			old_complex_measurement_filepath = File.join data_filepath, "old_complex_measurements.csv"
			old_complex_measurement_file = File.open old_complex_measurement_filepath, "r"
			puts "Importing thickness, tilt, and gibbs data for ComplexStructure"
			CSV.foreach(old_complex_measurement_file, :headers => true, :header_converters => :symbol) do |row|
				# see if complex in new db
				complex = ComplexStructure.find_by(:refpdbcode => row[:refpdbcode].to_s.strip.downcase)
				if complex.nil?
					complex = ComplexStructure.find_by(:name => row[:name].to_s.strip)
				end
				next if complex.nil?
				# add measurements to complex if in new db
				complex.thickness = row[:thickness].to_f
				complex.thicknesserror = row[:thicknesserror].to_f
				complex.tilt = row[:tilt].to_i
				complex.tilterror = row[:tilterror].to_i
				complex.gibbs = row[:gibbs].to_f
				complex.save!
			end
			old_complex_measurement_file.close

			# import ComplexProtein to link Complexes to their component proteins
			complex_proteins_filepath = File.join data_filepath, "complex_proteins.txt"
			complex_proteins_file = File.open complex_proteins_filepath, "r"
			puts "Importing ComplexProtein from #{complex_proteins_filepath}"
			CSV.foreach(complex_proteins_file, :col_sep => "|", :headers => false) do |row|
				complex_name = row[1].to_s.strip
				protein_uniprotcode = row[2].to_s.strip.upcase

				complex = ComplexStructure.find_by(:name => complex_name)
				protein = Protein.find_by(:uniprotcode => protein_uniprotcode)

				if !ComplexProtein.exists?(:protein_id => protein.id, :complex_structure_id => complex.id)
					ComplexProtein.create!(
						:protein_id => protein.id,
						:complex_structure_id => complex.id
					)
				end
			end
			complex_proteins_file.close

			# define the classification for all ComplexStructure
			puts "Generating ComplexClassification and ComplexMembrane"
			ComplexStructure.find_each(:batch_size => 2000) do |c|
				c.species_id = c.complex_proteins.first.protein.species.id
				c.save!

				c.complex_proteins.each do |cp|
					# add ComplexMembrane record if it's not already there (don't want duplicates)
					protein = cp.protein
					membrane_id = protein.membrane_id
					if not ComplexMembrane.exists?(:complex_structure_id => c.id, :membrane_id => membrane_id)
						ComplexMembrane.create!(
							:complex_structure_id => c.id,
							:membrane_id => membrane_id
						)
					end

					family_id = protein.protein_family_id
					# add ComplexClassification record if it's not already there (don't want duplicates)
					if not ComplexClassification.exists?(:complex_structure_id => c.id, :protein_family_id => family_id)
						ComplexClassification.create!(
							:complex_structure_id => c.id,
							:protein_family_id => family_id,
							:protein_superfamily_id => protein.protein_superfamily_id,
							:protein_class_id => protein.protein_class_id,
							:protein_type_id => protein.protein_type_id
						)
					end
				end
			end

			# import Subcomplex
			subcomplex_filepath = File.join data_filepath, "subcomplexes_with_uniprotid.txt"
			subcomplex_file = File.open subcomplex_filepath, "r"
			puts "Importing Subcomplex from #{subcomplex_filepath}"
			CSV.foreach(subcomplex_file, :col_sep => "|", :headers => false) do |row|
				complex_name = row[0].to_s.strip
				subcomplex_name = row[1].to_s.strip
				numsubunits = row[2].to_i
				pdb_files_count = row[4].to_i
				pdbcodes = row[5].to_s.strip.downcase
				comments = row[6].to_s.strip
				source_db = row[7].to_s.strip
				source_db_link = row[8].to_s.strip
				pubmed = row[9].to_s.strip
				uniprotids = row[-1].to_s.strip.upcase
				complex = ComplexStructure.find_by(:name => complex_name)
				if not complex.nil?
					Subcomplex.create!(
						:name => subcomplex_name,
						:numsubunits => numsubunits,
						:uniprotids => uniprotids,
						:pdbcodes => pdbcodes,
						:pdb_files_count => pdb_files_count,
						:comments => comments,
						:source_db => source_db,
						:source_db_link => source_db_link,
						:pubmed => pubmed,
						:complex_structure_id => complex.id
					)
				end
			end
			subcomplex_file.close

			# import ComplexPdbLink
			complex_pdb_link_filepath = File.join data_filepath, "complex_pdb_links.txt"
			complex_pdb_link_file = File.open complex_pdb_link_filepath, "r"
			puts "Importing ComplexPdbLink from #{complex_pdb_link_filepath}"
			CSV.foreach(complex_pdb_link_file, :col_sep => "|", :headers => false) do |row|
				complex_name = row[1].to_s.strip
				pdbcode = row[2].to_s.strip.downcase
				complex = ComplexStructure.find_by(:name => complex_name)
				ComplexPdbLink.create!(
					:complex_structure_id => complex.id,
					:pdb => pdbcode,

					# leave these as default values, found in another file
					:resolution => nil,
					:species_name => nil
				)

				# add refpdbcode to complex if it does not have one
				if complex.refpdbcode.to_s == ""
					complex.refpdbcode = pdbcode
					complex.save!
				end
			end
			complex_pdb_link_file.close

			# get resolution values for ComplexPdbLink
			resolution_filepath = File.join data_filepath, "cmpd_res.idx"
			resolution_file = File.open resolution_filepath, "r"
			puts "Importing resolution values for ComplexPdbLink (may take a while) from #{resolution_filepath}"
			CSV.foreach(resolution_file, :col_sep => ";", :headers => false, :quote_char => "\x00") do |row|
				pdbid = row[0].to_s.strip.downcase
				ComplexPdbLink.where(:pdb => pdbid).each do |cpl|
					res = row[1].to_f
					cpl.resolution = res == 0 ? nil : res
					cpl.save!
				end
			end

			# import ComplexPdbUniprot
			uniprot_pbd_filepath = File.join data_filepath, "uniprot_pdb.tsv2"
			uniprot_pbd_file = File.open uniprot_pbd_filepath, "r"
			puts "Importing ComplexPdbUniprot from #{uniprot_pbd_filepath}"
			CSV.foreach(uniprot_pbd_file, :col_sep => "|", :headers => false) do |row|
				uniprotcode = row[1].strip.upcase
				pdbids = row[2].downcase.split(";").map{|pdbid| pdbid.strip}
				cpl_matches = ComplexPdbLink.where(:pdb => pdbids)
				cpl_matches.each do |cpl|
					ComplexPdbUniprot.create!(
						:complex_pdb_link_id => cpl.id,
						:uniprotcode => uniprotcode
					)
				end
			end
			uniprot_pbd_file.close

			# import ComplexUniprotLink
			complex_uniprot_link_filepath = File.join data_filepath, "complex_uniprot_links.txt"
			complex_uniprot_link_file = File.open complex_uniprot_link_filepath, "r"
			puts "Importing ComplexUniprotLink from #{complex_uniprot_link_filepath}"
			CSV.foreach(complex_uniprot_link_file, :col_sep => "|", :headers => false) do |row|
				complex_name = row[1].strip
				uniprotcode = row[2].strip.upcase
				complex = ComplexStructure.find_by(:name => complex_name)
				ComplexUniprotLink.create!(
					:uniprotcode => uniprotcode,
					:complex_structure_id => complex.id,

					# leave null, will be found in another file
					:uniprotid => nil,
					:uniprot_name => nil
				)
			end
			complex_uniprot_link_file.close

			def import_uniprot_ids_and_names filepath, col_sep, type_of_protein
				# get additional uniprot info for each uniprot code
				uniprot_list_file = File.open filepath, "r"
				puts "Importing Uniprot IDs and Names from #{filepath}"
				i = 0
				CSV.foreach(uniprot_list_file, :col_sep => col_sep, :headers => true, :header_converters => :symbol) do |row|
					uniprotid = row[:entry_name].to_s.strip.upcase
					uniprotcode = row[:entry].to_s.strip.upcase

					# someone decided to name it "pdbid" in Protein even though
					# it's a uniprot id...
					if Protein.exists?(:pdbid => uniprotid)
						# remove bitopic complex uniprot links
						ComplexUniprotLink.where(:uniprotcode => uniprotcode).destroy_all
					else
						uniprot_name = row[:protein_names].split(" (")[0].to_s.strip
						ComplexUniprotLink.where(:uniprotcode => uniprotcode, :uniprotid => nil).each do |cul|
							cul.uniprotid = uniprotid
							cul.uniprot_name = uniprot_name
							cul.type_of_protein = type_of_protein
							cul.save!
						end
					end

					# suffix of the uniprot id indicates species name
					# e.g, uniprot id "EGFR_HUMAN" means species name is "HUMAN"
					species_name = uniprotid.split("_")[-1]
					ComplexPdbUniprot.where(:uniprotcode => uniprotcode, :uniprotid => nil).each do |cpu|
						cpu.uniprotid = uniprotid
						cpu.save!
						
						cpl = cpu.complex_pdb_link
						cpl.species_name = species_name
						cpl.save!
					end
				end
				uniprot_list_file.close
			end
			import_uniprot_ids_and_names File.join(data_filepath, "TM.txt"), "|", "polytopic"
			import_uniprot_ids_and_names File.join(data_filepath, "TM2.txt"), "|", "polytopic"
			import_uniprot_ids_and_names File.join(data_filepath, "uniprot_list2.txt"), "\t", "other"
			import_uniprot_ids_and_names File.join(data_filepath, "uniprot_list3.txt"), "\t", "other"
		end
	end
end






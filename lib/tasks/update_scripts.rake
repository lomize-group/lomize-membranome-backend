require 'csv.rb'

namespace :data do
	namespace :update do
		task update: :environment do
			# back up
			app = 'lomize-group-membranome'
			heroku = '/usr/local/bin/heroku'
			cmd = heroku+' pg:backups:capture --app '+app
			system cmd

			# run each task
			Rake::Task["data:update:update_proteins"].invoke
			Rake::Task["data:update:update_features"].invoke
		end

		task update_proteins: :environment do
			require "typhoeus"

			# function copied from opm backend update_scripts.rake
			def create_request url
				request = Typhoeus::Request.new(url)
				request.on_complete do |response|
					if response.success?
						yield response
					else
						# if we do happen to still be requesting too fast, wait more
						# and re-send the request synchronously.
						# Response code of 0 might sometimes happen if connection fluctuates.
						# Response code 429 indicates that we have sent requests too fast.
						# The RCSB API sometimes responds with 503 if it has a lot of traffic.
						if response.code == 0 || response.code == 429 || response.code == 503
							sleep(3)
							puts "Retrying #{url}"
							response = Typhoeus.get(url)
							if response.code == 200
								yield response
							else
								# Nothing you can really do about it if it fails again
								puts "ERROR SENDING REQUEST TO URL #{url}"
								puts "Failed with status code #{response.code}"
							end
						else
							# Nothing you can really do about failures
							puts "ERROR SENDING REQUEST TO URL #{url}"
							puts "Failed with status code #{response.code}"
						end
					end
				end

				return request
			end

			def delete_records
				## pdb_links
				# delete all pdb link entries since we should remove records
				# that are no longer in uniprot
				delete_count = PdbLink.count
				puts "Removing all #{delete_count} pdb_links records"
				PdbLink.delete_all

				# restart primary key (id) at 1
				ActiveRecord::Base.connection.reset_pk_sequence!(:pdb_links)

				## opm_links
				# delete all opm link entries since we should remove records
				# that are no longer in opm
				delete_count = OpmLink.count
				puts "Removing all #{delete_count} opm_links records"
				OpmLink.delete_all

				# restart primary key (id) at 1
				ActiveRecord::Base.connection.reset_pk_sequence!(:opm_links)
			end

			def update_annotations protein, data
				# get new annotations
				annotations = data["comments"]
				new_annotation = ""

				# update uniprot annotations
				if !annotations.nil?
					wanted_annotations = [
						'FUNCTION', 'CATALYTIC_ACTIVITY', 'SUBUNIT',
						'SUBCELLULAR_LOCATION', 'TISSUE_SPECIFICITY',
						'DOMAIN', 'PTM', 'DISEASE', 'SIMILARITY'
					]

					annotations = annotations.select{|item| wanted_annotations.include? item["type"]}
					annotations.each do |a|
						next if a["text"].nil? || a["text"][0]["value"].nil?

						text = a["text"][0]["value"]
						if a["type"] == "DISEASE"
							text = ""
							if !a["diseaseId"].nil?
								text += a["diseaseId"] + " "
							end
							if !a["acronym"].nil?
								text += "("+a["acronym"]+") "
							end
							if !a["dbReference"].nil? && a["dbReference"]["type"] == "MIM"
								text += "<a href=\"https://www.omim.org/entry/"
								text += a["dbReference"]["id"] + "\""
								text += ">OMIM</a>"
							end
							if text != ""
								text += ": "
							end
							if !a['description'].nil?
								text += a['description']['value'] || ""
							end
						end

						# only want to add this to annotations if there's stuff to add
						if text != ""
							if text[-1] != "."
								text += "."
							end

							text.scan(/PubMed:\d+/).each do |pm|
								pm_split = pm.split(":")
								num = pm_split[-1]
								anchor_tag = "<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/?term=#{num}\">#{num}</a>"
								new_pubmed = "#{pm_split[0]}:#{anchor_tag}"
								text = text.gsub(pm, new_pubmed)
							end

							annotation_type = a['type'].gsub("_", " ")
							new_annotation += "<p>#{annotation_type}: #{text}</p>"
						end
					end
				end

				# log only unique annotation changes for output
				annotation_output = "-"
				if protein.verification != new_annotation
					annotation_output = new_annotation
				end

				# finalize new annotation
				protein.verification = new_annotation
				protein.save!

				return annotation_output

			end

			def update_pdb_opm_links pool, protein, data
				# get new pdb links and to pdb_links table
				link_data = data['dbReferences'].select{ |item|
					item["type"] == "PDB" &&
					(item["properties"].nil? || item["properties"]["method"].upcase != "MODEL")
				}
				link_data.each do |item|
					chains = item["properties"].nil? ? nil : item["properties"]["chains"]
					pdb_code = item["id"]

					# create/save new pdb_link row from data
					PdbLink.create(
						:protein_id => protein.id,
						:pdb => pdb_code,
						:subunit_name => chains
					)

					# fixme: make opm accept non-lowercase pdb id
					request = create_request(OPM_BASE_URL + pdb_code.downcase) do |response|
						OpmLink.create(
							:protein_id => protein.id,
							:opm_link => pdb_code,
							:subunit_name => chains
						)
					end
					pool.queue(request)
				end
			end

			def update_intact_biogrid protein, data
				return if data["dbReferences"].nil?

				# filter down to intact and biogrid data in one pass
				intAct = "IntAct"
				bioGrid = "BioGrid"
				data = data["dbReferences"].select{|obj| obj["type"] == intAct || obj["type"] == bioGrid }

				# separate intact and biogrid
				intAct_data = data.find{|obj| obj["type"] == intAct}
				bioGrid_data = data.find{|obj| obj["type"] == bioGrid}

				# these will be stored in protein row
				use_intact = !intAct_data.nil? && !intAct_data["id"].nil?
				biogrid_id = bioGrid_data.nil? ? nil : bioGrid_data["id"]

				protein.intact = use_intact
				protein.biogrid = biogrid_id
				protein.save!

				# intact, then biogrid
				return [intAct != "IntAct" ? intAct : "-", bioGrid != "BioGrid" ? bioGrid : "-"]
			end

			def update_names protein, data

				#keep track of which portions were updated and which ones weren't
				#order: new_name, new_alt_name, new_gene_name
				output_record = ["-","-","-"]

				# parse for updated protein name and alternative name
				protein_data = data['protein']
				new_protein_name = ""
				if !protein_data.nil?
					name_data = protein_data['recommendedName']
					if !name_data.nil? && !name_data['fullName'].nil?
						new_protein_name = name_data['fullName']['value']
					end

					if new_protein_name == ""
						name_data = protein_data["submittedName"][0]
						if !name_data.nil? && !name_data['fullName'].nil?
							new_protein_name = name_data['fullName']['value']
						end
					end

					alt_name_data = protein_data['alternativeName']
					new_alt_name = []
					if !alt_name_data.nil?
						alt_name_data.each do |a|
							new_alt_name.push(a['fullName']['value'])
						end
						alt_name_data.each do |a|
							if !a['shortName'].nil?
								a['shortName'].each do |b|
									new_alt_name.push(b['value'])
								end
							end
						end
					end

					if !name_data.nil?
						alt_name_data = name_data['shortName']
						if !alt_name_data.nil?
							alt_name_data.each do |a|
								new_alt_name.push(a['value'])
							end
						end
					end
				end

				# parse for updated gene name
				gene_data = data['gene']
				if !gene_data.nil?
					gene_data = gene_data[0]
				end

				new_gene_name = []
				if !gene_data.nil?
					# we should take the first that appears on uniprot web page
					# the order they are shown is Name, olnNames, orfNames
					if !gene_data['name'].nil?
						new_gene_name.push(gene_data['name']['value'])
					elsif !gene_data['olnNames'].nil?
						gene_data['olnNames'].each do |gn|
							new_gene_name.push(gn['value'])
						end
					elsif !gene_data['orfNames'].nil?
						gene_data['orfNames'].each do |gn|
							new_gene_name.push(gn['value'])
						end
					end
				end


				updated = false
				# update changed protein info
				if protein.name != new_protein_name
					puts "protein id=" + protein.id.to_s + ": name changed"
					puts  "\told: " + protein.name
					puts "\tnew: " + new_protein_name
					protein.name = new_protein_name
					updated = true

					output_record[0] = new_protein_name
				end

				new_alt_name.sort!
				new_alt_str = new_alt_name.join("; ")
				if protein.altname != new_alt_name
					# only want to report differences that aren't simply
					# differences in semicolons/whitespace/order of names
					if protein.altname?
						altname = protein.altname
					else
						altname = ""
					end

					old_alt_name = altname.gsub("; ", ";") .split(";")

					if old_alt_name.sort != new_alt_name
						puts "protein id=" + protein.id.to_s + ": altname changed"
						puts "\told: " + altname
						puts "\tnew: " + new_alt_str

						output_record[1] = new_alt_str #TODO: might be new_alt_name 
					end
					protein.altname = new_alt_str
					updated = true

				end

				new_gene_str = ""
				if (new_gene_name.size > 0)
					new_gene_str = new_gene_name[0]
				end
				if protein.genename != new_gene_name

					# only want to report differences that aren't simply
					# differences in semicolons/whitespace/order of names
					old_gene_arr = protein.genename.gsub("; ", ";").split(";")
					old_gene_str = ""
					if (old_gene_arr.size > 0)
						old_gene_str = old_gene_arr[0]
					end
					if old_gene_str != new_gene_str
						puts "protein id=" + protein.id.to_s + ": genename changed"
						puts  "\told: " + protein.genename
						puts "\tnew: " + new_gene_str

						output_record[2] = new_gene_str #TODO: might be new_gene_name
					end
					protein.genename = new_gene_str
					updated = true

				end

				# save protein if anything changed
				#also returns values as array for processing in CSV
				if updated
					protein.save!
				end
				return output_record
			end

			def update_pbdid protein, data
				if data["id"] != protein.pdbid
					puts "protein id=" + protein.id.to_s + ": PBDID changed"
					puts  "\told: " + protein.pdbid
					puts "\tnew: " + data["id"]
					protein.pdbid = data["id"]
					protein.save!
					return data["id"]
				else
					return "-"
				end
			end
			puts "Updating protein data from EBI"
			

			delete_records()

			# fetch data and update
			ctr = DataHelper::Counter.new
			Protein.find_in_batches(:batch_size => 200) do |protein_batch|
				# generate input batch for multithreaded python script
				executor = DataHelper::FetchScriptExecutor.new
				protein_batch.each do |protein|
					executor.add_input([protein.id, EBI_BASE_URL+protein.uniprotcode])
				end

				# run script for the batch
				results = executor.run

				CSV.open("protein_test.csv","ab") do |csv|
					csv << ["uniprotcode","PBDID","protein_name","alt_name", "new_gene_name", "verification", "intact", "biogrid"]
				end

				# header for uniprot id diff	
				#CSV.open("uniprot_id_diff.csv","ab") do |csv|
				#	csv << ["old membranome code", "uniprot code"]
				#end

				# request pool for opm_links
				pool = Typhoeus::Hydra.new(max_concurrency: 15)

				# process
				results.each do |i|
					line = i.split(" ", 2)
					data = DataHelper::parse_json(line[1])

					next if data.nil?

					protein = Protein.find(line[0].to_i)

					verification_output = update_annotations(protein, data)
					intact_biogrid_output = update_intact_biogrid(protein, data)
					update_pdb_opm_links(pool, protein, data)
					# update_reactome(protein, data)
					name_output = update_names(protein, data)
					pbd_output = update_pbdid(protein,data)
					
					# check if diff for uniprot id exists and write it 
					#if data["id"] != protein.pdbid
					#	CSV.open("uniprot_id_diff.csv","ab") do |csv|
					#		csv << [protein.pdbid , data["id"]]
					#	end
					#end


					CSV.open("protein_test.csv","ab") do |csv|
						if pbd_output != "-" || name_output != ["-","-","-"] ||verification_output != "-" || intact_biogrid_output != ["-","-"]
							csv << [protein.uniprotcode, pbd_output, name_output[0], name_output[1],name_output[2], verification_output,intact_biogrid_output[0],intact_biogrid_output[1]]
						end

					end
				end

				pool.run

				ctr.print_increment(protein_batch.count)
			end
		end


		task update_features: :environment do
			puts "Updating protein features from EBI"

			base_url = "https://www.ebi.ac.uk/proteins/api/features"
			need_features = ["SIGNAL", "CHAIN", "TOPO_DOM", "TRANSMEM", "DOMAIN", "REGION"]

			total_counter = 0
			Protein.find_each(:batch_size => 1000) do |protein|
				params = {
					:offset => "0",
					:size => "-1",
					:accession => protein.uniprotcode,
					:types => "signal,chain,topo_dom,transmem,domain,region"
				}
				data = DataHelper::get_data(base_url, params)

				next if data.nil? || data[0].nil?

				features = data[0]['features']
				new_features = ""

				if !features.nil?
					# filter out unneeeded features
					features = features.select{ |item| need_features.include? item['type'] }
					features.each do |feature|
						new_features += feature['type'] + " "
						new_features += feature['begin'] + " "
						new_features += feature['end'] + " "
						new_features += feature["description"] + "<br/>"
					end
				end
				protein.comments = new_features
				protein.save!
				if (total_counter % 100 == 0)
					puts total_counter.to_s + " proteins updated..."
				end
				total_counter += 1
			end
		end

		task update_protein_membranes: :environment do
			require 'csv'

			# construct update record
			CSV.open("protein_membrane_test.csv","ab") do |csv|
				csv << ["membrane_name_cache","protein_pbdid_cache","pubmed","main", "membrane_id", "protein_id", "created_at", "updated_at"]
			end			

			# construct the uniprot-to-membranome map of membrane names
			membrane_map_filename = "uniprot_membrane_map.csv"
			membrane_map_path = File.join Rails.root, "db", membrane_map_filename
			membrane_map_csv = File.open membrane_map_path, "r"
			membrane_map = {}
			proteins_with_bad_membranes = []
			CSV.foreach(membrane_map_csv, {:headers => true, :header_converters => :symbol}) do |row|
				membrane_map[row[:uniprot_name]] = row[:correct_name]
			end

			puts "Updating information in protein_membranes table"
			ctr = DataHelper::Counter.new

			# fetch data and update
			Protein.find_in_batches(:batch_size => 200) do |protein_batch|
				# generate input batch for multithreaded python script
				executor = DataHelper::FetchScriptExecutor.new
				protein_batch.each do |protein|
					executor.add_input([protein.id, EBI_BASE_URL+protein.uniprotcode])
				end

				# run script for the batch
				results = executor.run
				results.each do |i|
					line = i.split(" ", 2)
					curr_protein_id = line[0].to_i
					data = DataHelper::parse_json(line[1])
					next if data.nil? || data["comments"].nil?

					# remove all data without pubmed source
					data = data["comments"].select{ |c|
						c["type"] == "SUBCELLULAR_LOCATION" &&
						!c["locations"].nil? && !c["locations"].empty?
					}

					# store data
					data.each do |d|
						d["locations"].each do |loc|

							# filter out unusable data
							next if loc["location"].nil? || loc["location"]["evidences"].nil?
							evidence = loc["location"]["evidences"].find{ |x|
								!x["source"].nil? &&
								x["source"]["name"] == "PubMed" &&
								!x["source"]["id"].nil?
							}
							next if evidence.nil?

							membrane_name_raw = loc["location"]["value"]
							membrane_name = membrane_map[membrane_name_raw]

							# one of the localizations we do not want to include
							if membrane_name.nil?
								proteins_with_bad_membranes << [curr_protein_id, membrane_name_raw]
								next
							end

							pubmed_id = evidence["source"]["id"]

							# this should always exist based on the constructed map
							membrane = Membrane.find_by(:name => membrane_name)

							# change from generic "Membrane" if a specific one is found
							protein = Protein.find(curr_protein_id)
							if protein.membrane_name_cache == "Membrane"
								protein.membrane_name_cache = membrane_name
								protein.membrane = membrane
								protein.save!
								next
								# ProteinMembrane record will be created with Main record creation (below)
							end

							prot_membrane = ProteinMembrane.find_by(
								:membrane_id => membrane.id,
								:protein_id => curr_protein_id
							)

							# create new ProteinMembrane record if it doesn't exist
							if prot_membrane.nil?
								prot_membrane = ProteinMembrane.create(
									:pubmed => pubmed_id,
									:protein_id => curr_protein_id,
									:membrane_id => membrane.id,
									:main => false
								)
								# another place where new membrane records created
								CSV.open("protein_membrane_test.csv","ab") do |csv|
									csv << [prot_membrane.membrane_name_cache, prot_membrane.protein_pdbid_cache, prot_membrane.pubmed, prot_membrane.main, prot_membrane.membrane_id, prot_membrane.protein_id, prot_membrane.created_at, prot_membrane.updated_at]
								end		
								
							# if it does exist just update the pubmed and membrane
							else
								if prot_membrane.pubmed != pubmed_id || prot_membrane.membrane_id != membrane.id
									# existing records updated
									CSV.open("protein_membrane_test.csv","ab") do |csv|
										csv << [prot_membrane.membrane_name_cache, prot_membrane.protein_pdbid_cache, prot_membrane.pubmed, prot_membrane.main, prot_membrane.membrane_id, prot_membrane.protein_id, prot_membrane.created_at, prot_membrane.updated_at]
									end		
								end
								prot_membrane.pubmed = pubmed_id
								prot_membrane.membrane_id = membrane.id
							end

							# update caches
							prot_membrane.update_hierarchy
							prot_membrane.save!
						end
					end
				end
				ctr.print_increment(protein_batch.count)
			end

			# Main record creation
			# each protein gets a record for its main membrane
			Protein.find_each(:batch_size => 1000) do |protein|
				pm = ProteinMembrane.find_by(:membrane_id => protein.membrane_id, :protein_id => protein.id)
				if pm.nil?
					pm = ProteinMembrane.create(
						:membrane_name_cache => protein.membrane.name,
						:protein_id => protein.id,
						:membrane_id => protein.membrane.id,
						:main => true
					)
					pm.update_hierarchy
					# add freshly created protein membranes
					CSV.open("protein_membrane_test.csv","ab") do |csv|
						csv << [pm.membrane_name_cache, pm.protein_pdbid_cache, pm.pubmed, pm.main, pm.membrane_id, pm.protein_id, pm.created_at, pm.updated_at]
					end		
				else
					pm.main = true;
				end
				pm.save!
			end

			# output bad localizations
			proteins_with_bad_membranes = proteins_with_bad_membranes.sort do |a, b|
				case
				when a[0] < b[0]
					-1
				when a[0] > b[0]
					1
				else
					a[1] <=> b[1]
				end
			end

			puts "# membranome protein id and corresponding bad localizations"
			puts "protein_id,localization"
			proteins_with_bad_membranes.each do |pair|
				puts "#{pair[0]},\"#{pair[1]}\""
			end
		end

		task update_complex_pdb_links: :environment do
			CSV.open("complex_pdb_links_test.csv","ab") do |csv|
				csv << ["complex_structure_id","ordering","pdb","species_name","created_at","updated_at","resolution"]
			end
			puts "Updating ComplexPdbLink based on ComplexProtein and PdbLink"
			ComplexStructure.find_each(:batch_size => 1000) do |complex|
				protein_ids = ComplexProtein.where(:complex_structure_id => complex.id).pluck(:protein_id)
				#protein_ids = ComplexProtein.where(:complex_id => complex.id).pluck(:protein_id)
				protein_ids.combination(2).each do |pair|
					pdb_links = PdbLink.select(:pdb).where(:protein_id => pair).group(:pdb).having("count(*) > 1")
					pdb_links.each do |l|
						if !ComplexPdbLink.exists?(:complex_structure_id => complex.id,:pdb => l.pdb)
						#if !ComplexPdbLink.exists?(:complex_id => complex.id,:pdb => l.pdb)
							species_name = Protein.find(pair[0]).pdbid.split("_")[-1]
							complex_pdb_obj = ComplexPdbLink.create(
								:complex_structure_id => complex.id,
								:pdb => l.pdb,
								:species_name => species_name
							)

							#ComplexPdbLink.create(
							#	:complex_id => complex.id,
							#	:pdb => l.pdb,
							#	:species_name => species_name
							#)

							CSV.open("complex_pdb_links_test.csv","ab") do |csv|
								csv << [complex_pdb_obj.complex_structure_id, complex_pdb_obj.ordering, complex_pdb_obj.pdb, complex_pdb_obj.species_name, complex_pdb_obj.created_at, complex_pdb_obj.updated_at, complex_pdb_obj.resolution]
							end
						end
					end
				end
			end
		end
	end
end



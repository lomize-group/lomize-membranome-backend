1.1.|Global and overview maps|Global and overview maps 
2.1.|Energy metabolism|Energy metabolism and biological oxidations 
2.2.|Biological oxidations|Energy metabolism and biological oxidations 
3.1.|General carbohydrate and glycan metabolism maps|Carbohydrate and glycan metabolism 
3.2.|TCA cycle, pyruvate|Carbohydrate and glycan metabolism 
3.3.|Carbohydrate biosynthesis|Carbohydrate and glycan metabolism 
3.4.|Carbohydrate degradation|Carbohydrate and glycan metabolism 
3.5.|Glycan biosynthesis and metabolism|Carbohydrate and glycan metabolism 
3.6.|Blood group biosynthesis|Carbohydrate and glycan metabolism 
4.1.|Wax, cell wall and plasmalogen biosynthesis|Lipid, fatty acid and terpenoid metabolism 
4.2.|Biosynthesis of specialized proresolving mediators|Lipid, fatty acid and terpenoid metabolism 
4.3.|Phospholipid metabolism (general)|Lipid, fatty acid and terpenoid metabolism 
4.4.|Phospholipid biosynthesis|Lipid, fatty acid and terpenoid metabolism 
4.5.|Regulation of lipid metabolism|Lipid, fatty acid and terpenoid metabolism 
4.6.|Phospholipid degradation|Lipid, fatty acid and terpenoid metabolism 
4.7.|Sphingolipid metabolism|Lipid, fatty acid and terpenoid metabolism 
4.8.|Fatty acid metabolism (general)|Lipid, fatty acid and terpenoid metabolism 
4.9.|Fatty acid synthesis|Lipid, fatty acid and terpenoid metabolism 
4.10.|Fatty acid degradation|Lipid, fatty acid and terpenoid metabolism 
4.11.|Metabolism of steroids (general)|Lipid, fatty acid and terpenoid metabolism 
4.12.|Steroid biosynthesis|Lipid, fatty acid and terpenoid metabolism 
4.13.|Triglyceride metabolism|Lipid, fatty acid and terpenoid metabolism 
4.14.|Surfactant metabolism|Lipid, fatty acid and terpenoid metabolism 
4.15.|Metabolism of terpenoids and polyketides|Lipid, fatty acid and terpenoid metabolism 
5.1.|General pathways and degradation of nucleotides/nucleosides|Nucleotide metabolism 
5.2.|Nucleotide biosynthesis|Nucleotide metabolism 
6.1.|Synthesis and pathways of common amino acids|Metabolism of amino acids, peptides and derivatives 
6.2.|Amino acid degradation|Metabolism of amino acids, peptides and derivatives 
6.3.|Biogenic amines|Metabolism of amino acids, peptides and derivatives 
6.4.|Hormone biosynthesis|Metabolism of amino acids, peptides and derivatives 
6.5.|Hormone degradation|Metabolism of amino acids, peptides and derivatives 
6.6.|Melanogenesis|Metabolism of amino acids, peptides and derivatives 
6.7.|Peptide hormone metabolism|Metabolism of amino acids, peptides and derivatives 
7.1.|General metabolism pathways of cofactors and vitamins|Metabolism of cofactors and vitamins 
7.2.|Cofactor/vitamin biosynthesis|Metabolism of cofactors and vitamins 
7.3.|Cofactor/vitamin degradation|Metabolism of cofactors and vitamins 
8.1.|Various secondary metabolites|Biosynthesis of secondary metabolites 
8.2.|Nitrogen-containing glucoside biosynthesis|Biosynthesis of secondary metabolites 
8.3.|Phenilpropanoid/Flavonoid biosynthesis|Biosynthesis of secondary metabolites 
9.1.|Xenobiotics biodegradation|Metabolism of xenobiotics and other compounds 
9.2.|Metabolism of inorganic compounds|Metabolism of xenobiotics and other compounds 
9.3.|Degradation of various compounds|Metabolism of xenobiotics and other compounds 
10.1.|Gene expression, transcription|Genetic information processing 
10.2.|Pathways with microRNA and piRNA|Genetic information processing 
10.3.|Metabolism/assembly of RNA|Genetic information processing 
10.4.|tRNA processing|Genetic information processing 
10.5.|DNA metabolism, including damage response|Genetic information processing 
11.1.|Translation|Folding and processing of proteins 
11.2.|Protein transport and localization|Folding and processing of proteins 
11.3.|Folding and assembly examples|Folding and processing of proteins 
11.4.|Unfolded protein response|Folding and processing of proteins 
11.5.|Plasma lipoprotein assembly|Folding and processing of proteins 
11.6.|Post-translational modifications (PTM) of proteins|Folding and processing of proteins 
11.7.|PTM (SUMOylation)|Folding and processing of proteins 
11.8.|PTM (glycosylation)|Folding and processing of proteins 
11.9.|PTM (ubiquitination)|Folding and processing of proteins 
11.10.|Proteolisis|Folding and processing of proteins 
11.11.|Proteins in extracellular matrix|Folding and processing of proteins 
11.12.|Amyloid fiber formation|Folding and processing of proteins 
12.1.|Vesicle-mediated membrane trafficing|Transport 
12.2.|Membrane transport|Transport 
12.3.|Electron transfer chains|Transport 
12.4.|Iron uptake and transport|Transport 
12.5.|O2/CO2 exchange in erythocytes|Transport 
13.1.|Various signal transduction pathways|Signal transduction 
13.2.|Signaling by nuclear receptors|Signal transduction 
13.3.|Intracellular signaling by secondary messengers|Signal transduction 
13.4.|Signaling by Erythropoietin|Signal transduction 
13.5.|Signaling by NOTCH|Signal transduction 
13.6.|Signaling by WNT|Signal transduction 
13.7.|Signaling by RTK (EGFR)|Signal transduction 
13.8.|Signaling by RTK (FGFR)|Signal transduction 
13.9.|Signaling by RTK (ERBB)|Signal transduction 
13.10.|Signaling by RTK (VEGF)|Signal transduction 
13.11.|Signaling by RTK (PDGF)|Signal transduction 
13.12.|Signaling by RTK (MET)|Signal transduction 
13.13.|Signaling by RTK (insulin receptor)|Signal transduction 
13.14.|Signaling by RTK (Type-1 insulin-like receptors)|Signal transduction 
13.15.|Signaling by RTK (SCG-KIT)|Signal transduction 
13.16.|Signaling by RTK (MST1)|Signal transduction 
13.17.|Signaling by RTK (NTRKs)|Signal transduction 
13.18.|Signaling by death receptors|Signal transduction 
13.19.|TRAIL signaling|Signal transduction 
13.20.|Signaling by leptins|Signal transduction 
13.21.|MAPK signaling|Signal transduction 
13.22.|Signaling by Hedgehog|Signal transduction 
13.23.|Signaling by TGF-beta family members|Signal transduction 
13.24.|Signaling by non-receptor tyrosine kinases|Signal transduction 
13.25.|Integrin signaling|Signal transduction 
13.26.|Signaling by GPCR|Signal transduction 
13.27.|Signaling by Rho GTPases|Signal transduction 
13.28.|Insulin signaling|Signal transduction 
13.29.|Cellular responses to external stimuli|Signal transduction 
14.1.|Cell cycle|Cell growth and death 
14.2.|Organelle biogenesis|Cell growth and death 
14.3.|Cell motility|Cell growth and death 
14.4.|Eukaryotic cell-cell communication and interactions|Cell growth and death 
14.5.|Protein-protein interactions in synapse|Cell growth and death 
14.6.|Cell death|Cell growth and death 
14.7.|Autophagy|Cell growth and death 
15.1.|TCR signaling|Adaptive immune system 
15.2.|Class I MHC mediated antigen presentation|Adaptive immune system 
15.3.|MHC class II antigen presentation|Adaptive immune system 
15.4.|Signaling by B-cell receptor|Adaptive immune system 
15.5.|Butyrophilin (BTN) family interactions|Adaptive immune system 
15.6.|Costimulation by CD28|Adaptive immune system 
15.7.|Immunoregulatory interactions|Adaptive immune system 
16.1.|Toll-like receptors|Innate Immune System 
16.2.|C-type lectin receptors|Innate Immune System 
16.3.|Complement cascade|Innate Immune System 
16.4.|Cytoplasmic pattern recognition receptors|Innate Immune System 
16.5.|DDX58/IFIH1-mediated induction of interferon-alpha/beta|Innate Immune System 
16.6.|Advanced glycosylation endproduct receptor signaling|Innate Immune System 
16.7.|Neutrophil degranulation|Innate Immune System 
16.8.|ROS and RNS production in phagocytes|Innate Immune System 
16.9.|Fc epsilon (FCERI) signaling|Innate Immune System 
16.10.|Fc gamma receptot (FCGR) dependent phogocytosis|Innate Immune System 
16.11.|NBD, LLR containing receptor (NLR) signaling|Innate Immune System 
16.12.|DAP12 interactions|Innate Immune System 
16.13.|Antimicrobial peptides|Innate Immune System 
17.1.|General cytokine pathways|Cytokine signaling in Immune system 
17.2.|Interferon signaling|Cytokine signaling in Immune system 
17.3.|Interleukins signaling|Cytokine signaling in Immune system 
17.4.|Growth hormone receptor|Cytokine signaling in Immune system 
17.5.|Prolactin receptor|Cytokine signaling in Immune system 
17.6.|FLT3 signaling|Cytokine signaling in Immune system 
18.1.|Digestive system|Digestive and excretory systems 
18.2.|Excretory system|Digestive and excretory systems 
19.1.|Nervous system|Nervous and sensory systems, muscle contruction 
19.2.|Synapse transmission|Nervous and sensory systems, muscle contruction 
19.3.|Sensory system and environmental adaptation|Nervous and sensory systems, muscle contruction 
19.4.|Cardiovascular system, muscle contraction|Nervous and sensory systems, muscle contruction 
20.1.|Platellet homeostasis|Hemostasis 
20.2.|Platellet activation, signaling and aggregation|Hemostasis 
20.3.|Cell surface interactions at the vascular wall|Hemostasis 
20.4.|Clotting cascade|Hemostasis 
20.5.|Factors involved in megakaryocyte development and platelet production|Hemostasis 
21.1.|General development pathways|Organ development and cell differentiation 
21.2.|Signalig by NODAL|Organ development and cell differentiation 
21.3.|Various development pathways|Organ development and cell differentiation 
21.4.|Adipogenesis|Organ development and cell differentiation 
21.5.|Hematopoesis|Organ development and cell differentiation 
21.6.|Neurons and sensory cells|Organ development and cell differentiation 
21.7.|Axon giudance|Organ development and cell differentiation 
21.8.|Reproductive system|Organ development and cell differentiation 
21.9.|Plant development|Organ development and cell differentiation 
22.1.|Cancer: overview/general|Diseases 
22.2.|Cancer: specific types|Diseases 
22.3.|Immune disease|Diseases 
22.4.|Neurodegenerative disease|Diseases 
22.5.|Substance dependence|Diseases 
22.6.|Cardiovascular disease|Diseases 
22.7.|Endocrine and metabolic disease|Diseases 
22.8.|Metabolic diseases of biological oxidation enzymes|Diseases 
22.9.|Infectious disease: bacterial|Diseases 
22.10.|Infectious disease: viral|Diseases 
22.11.|Infectious disease: parasitic|Diseases 
22.12.|Diseases of signal transduction|Diseases 
22.13.|Diseases of glycosylation|Diseases 
22.14.|Diseases of TM transporters|Diseases 
22.15.|Other diseases|Diseases 

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Reset all Type Objects


# Type.delete_all
# Type.delete_all

require 'json'

#This assumes an import from a Json export
filename = "membranome_3-20.json"
filepath = File.join Rails.root, "db", filename

old_tables = {}
current_table = ""
File.open(filepath).each do |line|
	if line[0..1] == "//"
		current_table = line[3...-1]
	elsif line[0] == "["
		old_tables[current_table] = JSON.parse(line)
		puts "Reading old table: " + current_table
	end
end

new_tables = {}
puts "Removing old tables and re-building with seed....\n\n"

# ==================== creating new ProteinType ====================
# ==================== creating new ProteinType ====================
ProteinType.destroy_all

old_db_table = "membranome.type"
new_type = "ProteinType"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")
	new_obj = ProteinType.create(
		ordering: number[-1],
		name: object["name"].strip,
		description: object["description"],
		)
	new_tables[new_type][object["number"]] = new_obj
end
puts :json=> ProteinType.first
puts "Created " +ProteinType.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new ProteinClass ====================
## ==================== creating new ProteinClass ====================
ProteinClass.destroy_all

old_db_table = "membranome.class"
new_type = "ProteinClass"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")	
	old_id = number[-1]
	table_id = number[0..-2].join(".") + "."
	reftype = new_tables["ProteinType"][table_id]

	new_obj = ProteinClass.create(
		ordering: old_id,
		name: object["name"],
		description: object["description"],
		protein_type: reftype
		)
	new_tables[new_type][object["number"]] = new_obj
end

puts :json=> ProteinClass.first
puts "Created " +ProteinClass.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new ProteinSuperfamily ====================
## ==================== creating new ProteinSuperfamily ====================
ProteinSuperfamily.destroy_all

old_db_table = "membranome.superfamily"
new_type = "ProteinSuperfamily"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")	
	old_id = number[-1]
	classtype_id = number[0..-2].join(".") + "."
	class_ref = new_tables["ProteinClass"][classtype_id]
	
	new_obj = ProteinSuperfamily.create(
		ordering: old_id,
		name: object["name"],
		comment: object["comment"],
		pfam: object["tcdb"],
		protein_class: class_ref,
		)
	new_tables[new_type][object["number"]] = new_obj

end
puts :json=> ProteinSuperfamily.first
puts "Created " +ProteinSuperfamily.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new ProteinFamily ====================
## ==================== creating new ProteinFamily ====================\
ProteinFamily.destroy_all

old_db_table = "membranome.family"
new_type = "ProteinFamily"
puts "Migrating " + old_db_table
new_tables[new_type] = {}
family_id_map = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")	
	old_id = number[-1]
	parent_id = number[0..-2].join(".") + "."
	super_ref = new_tables["ProteinSuperfamily"][parent_id]

	new_obj = ProteinFamily.create(
		ordering: old_id,
		name: object["name"],
		interpro: object["tcdb"],
		protein_superfamily: super_ref,
		)

	new_tables[new_type][object["id"]] = new_obj
	family_id_map[object["id"]] = number
end
puts :json=> ProteinFamily.first
puts "Created " +ProteinFamily.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new Species ====================
## ==================== creating new Species ====================
Species.destroy_all


old_db_table = "membranome.species"
new_type = "Species"
puts "Migrating " + old_db_table

new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	new_obj = Species.create(
		ordering: object["id"],
		name: object["name"],
		)
	new_tables[new_type][object["id"]] = new_obj
end
puts :json=> Species.first
puts "Created " +Species.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new Membrane ====================
## ==================== creating new Membrane ====================
Membrane.destroy_all

old_db_table = "membranome.membrane"
new_type = "Membrane"
puts "Migrating " + old_db_table

new_tables[new_type] = {}
print(old_tables[old_db_table][0])

for object in old_tables[old_db_table] do
	new_obj = Membrane.create(
		ordering: object["id"],
		name: object["name"],
		topology_in: object["in"],
		topology_out: object["out"],
		abbreviation: object["abbreviation"],
		)
	new_tables[new_type][object["id"]] = new_obj
end
puts :json=> Membrane.first
puts "Created " +Membrane.all.count.to_s+ " of type '"+new_type+"'\n\n"

# ### =========================================
# ### =========================================
# ### =========================================
# ### =========================================
# ### BASIC OBJECTS ARE SETUP
# ### =========================================
# ### =========================================
# ### =========================================
# ### =========================================


## ==================== creating new Proteins ====================
## ==================== creating new Proteins ====================
Protein.destroy_all

old_db_table = "membranome.protein"
new_type = "Protein"
puts "Migrating " + old_db_table + ".... this may take a few minutes"
new_tables[new_type] = {}
proteins = {}

for object in old_tables[old_db_table] do
	fam_ref = new_tables["ProteinFamily"][object["family_id"]]
	number = family_id_map[object["family_id"]]

	super_id = number[0..-2].join(".") + "."
	super_ref = new_tables["ProteinSuperfamily"][super_id]

	class_id = number[0..-3].join(".") + "."
	class_ref = new_tables["ProteinClass"][class_id]

	type_id = number[0..-4].join(".") + "."
	type_ref = new_tables["ProteinType"][type_id]

	topology_subunit = object["topology"].strip
	topology_show_in = nil
	if topology_subunit.downcase == "in"
		topology_show_in = true
	elsif topology_subunit.downcase == "out"
		topology_show_in = false
	end

	breactome = object["reactome"].to_i == 1
	
	primary = Protein.create(
		ordering: new_tables[new_type].count,
		membrane: new_tables["Membrane"][object["membrane_id"]],
		species: new_tables["Species"][object["species_id"]],
		protein_family: fam_ref,
		protein_superfamily: super_ref,
		protein_class: class_ref,
		protein_type: type_ref,
		name: object["name"],
		comments: object["comments"],
		pdbid: object["pdbid"].strip,
		resolution: object["resolution"],
		topology_show_in: topology_show_in,
		thickness: object["thickness"],
		thicknesserror: object["thicknesserror"],
		tilt: object["tilt"],
		tilterror: object["tilterror"],
		gibbs: object["gibbs"],
		tau: object["tau"],
		verification: object["verification"],
		uniprotcode: object["uniprotcode"],
		genename: object["genename"],
		genenumber: object["genenumber"],
		altname: object["altname"],
		segment: object["segment"],
		aaseq: object["aaseq"],
		interpro: object["interproid"],
		pharmgkb: object["pharmgkb"],
		mimegene: object["mimegene"],
		hmp: object["hmp"],
		display_reactome: breactome,
		)
	# not sure we need "numsubunits"
	new_tables[new_type][object["id"]] = primary
	proteins[object["pdbid"].strip] = primary
end
puts :json=> Protein.first
puts "Created " +Protein.all.count.to_s+ " of type '"+new_type+"\n\n"


## ==================== creating new ComplexClasses ====================
## ==================== creating new ComplexClasses ====================
ComplexClass.destroy_all

old_db_table = "membranome.class_complex"
new_type = "ComplexClass"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")
	new_obj = ComplexClass.create(
		ordering: number[-1],
		name: object["name"].strip,
		)
	new_tables[new_type][object["number"]] = new_obj
end
puts :json=> ComplexClass.first
puts "Created " +ComplexClass.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new ComplexFamilies ====================
## ==================== creating new ComplexFamilies ====================
ComplexFamily.destroy_all

old_db_table = "membranome.family_complex"
new_type = "ComplexFamily"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

complexfam_id_map = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")
	table_id = number[0..-2].join(".") + "."
	reftype = new_tables["ComplexClass"][table_id]

	new_obj = ComplexFamily.create(
		ordering: number[-1],
		name: object["name"].strip,
		complex_class: reftype,
		)
	new_tables[new_type][object["id"]] = new_obj
	complexfam_id_map[object["id"]] = number
end
puts :json=> ComplexFamily.first
puts "Created " +ComplexFamily.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new ProteinComplexes ====================
## ==================== creating new ProteinComplexes ====================
ProteinComplex.destroy_all

old_db_table = "membranome.complex"
new_type = "ProteinComplex"
puts "Migrating " + old_db_table
new_tables[new_type] = {}
complexes = {}

for object in old_tables[old_db_table] do	
	fam_ref = new_tables["ComplexFamily"][object["family_id"]]
	number = complexfam_id_map[object["family_id"]]

	class_id = number[0..-2].join(".") + "."
	class_ref = new_tables["ComplexClass"][class_id]

	topology_subunit = object["topology"].split(" ")[0]
	topology_show_in = false
	topology_parts = object["topology"].split(" ")
	if topology_parts.length > 1 && topology_parts[1].downcase == "in"
		topology_show_in = true
	end
	if topology_parts[0].downcase == "in" or topology_parts[0].downcase == "out"
		topology_subunit = ""
		topology_show_in = false
		if object["topology"].split(" ")[0] == "in"
			topology_show_in = true
		end
	end


	new_obj = ProteinComplex.create(
		ordering: new_tables[new_type].count,
		name: object["name"].strip,
		numsubunits: object["numsubunits"],
		membrane: new_tables["Membrane"][object["membrane_id"]],
		complex_class: class_ref,
		complex_family: fam_ref,
		complexid: object["complexid"].strip,
		refpdbcode: object["refpdbcode"],
		topology_show_in: topology_show_in,
		topology_subunit: topology_subunit,
		thickness: object["thickness"],
		thicknesserror: object["thicknesserror"],
		tilt: object["tilt"],
		tilterror: object["tilterror"],
		gibbs: object["gibbs"],
		comments: object["comments"],
		)
	new_tables[new_type][object["id"]] = new_obj
	complexes[object["complexid"].strip] = new_obj
end

puts :json=> ProteinComplex.first
puts "Created " +ProteinComplex.all.count.to_s+ " of type '"+new_type+"'\n\n"
## ==================== creating new ProteinComplexInteractions ====================
## ==================== creating new ProteinComplexInteractions ====================
ProteinComplexInteraction.delete_all

old_db_table = "membranome.protein_complex"
new_type = "ProteinComplexInteraction"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	protein_ref = proteins[object["uniprotid"].strip]
	complex_ref = complexes[object["complexid"].strip]

	if protein_ref == nil || complex_ref == nil
		next
	end

	ProteinComplexInteraction.create(
		protein: protein_ref,
		protein_complex: complex_ref,
		protein_name_cache: protein_ref.name,
		protein_complex_name_cache: complex_ref.name,
	)
end
puts :json=> ProteinComplexInteraction.first
puts "Created " +ProteinComplexInteraction.all.count.to_s+ " of type '"+new_type+"'\n\n"
## ==================== creating new ProteinInteractions ====================
## ==================== creating new ProteinInteractions ====================
ProteinInteraction.delete_all

old_db_table = "membranome.relatedproteins"
new_type = "ProteinInteraction"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	protein_ref1 = proteins[object["protein_uniprotid"].strip]
	protein_ref2 = proteins[object["uniprotid"].strip]

	if protein_ref1 == nil || protein_ref2 == nil
		next
	end

	if new_tables[new_type][protein_ref1.id] && new_tables[new_type][protein_ref1.id][protein_ref2.id]
		next
	end

	ProteinInteraction.create(
		primary_protein: protein_ref1,
		related_protein: protein_ref2,
		related_protein_name_cache: protein_ref2.name,
		complex: object["complex"],
		pdbid: object["pdbid"],
		pubmed: object["pubmed"],
	)

	ProteinInteraction.create(
		primary_protein: protein_ref2,
		related_protein: protein_ref1,
		related_protein_name_cache: protein_ref1.name,
		complex: object["complex"],
		pdbid: object["pdbid"],
		pubmed: object["pubmed"],
	)

	if new_tables[new_type][protein_ref2.id] == nil
		new_tables[new_type][protein_ref2.id] = {}
	end
	
	new_tables[new_type][protein_ref2.id][protein_ref1.id] = true;

end
puts :json=> ProteinInteraction.first
puts "Created " +ProteinInteraction.all.count.to_s+ " of type '"+new_type+"'\n\n"


# ## ==================== creating new Domains ====================
# ## ==================== creating new Domains ====================
Domain.destroy_all


old_db_table = "membranome.domain"
new_type = "Domain"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	protein_ref = proteins[object["uniprotid"].strip]
	Domain.create(
		protein: protein_ref,
		firstaa: object["firstaa"],
		lastaa: object["lastaa"],
		protein_pdbid: object["protein_pdbid"],
		pdbsubunit: object["pdbsubunit"],
		seqidentity: object["seqidentity"],
		domain: object["domain"],
		pfamlink: object["pfamlink"],
	)
end

puts :json=> Domain.first
puts "Created " +Domain.all.count.to_s+ " of type '"+new_type+"'\n\n"

# ## ==================== creating new Opm Links ====================
# ## ==================== creating new Opm Links ====================
OpmLink.destroy_all

old_db_table = "membranome.opm"
new_type = "opm_links"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	protein_ref = proteins[object["uniprotid"].strip]
	OpmLink.create(
		protein: protein_ref,
		opm_link: object["opm"],
	)
end

puts :json=> OpmLink.first
puts "Created " +OpmLink.all.count.to_s+ " of type '"+new_type+"'\n\n"

# ## ==================== creating new Pathways ====================
# ## ==================== creating new Pathways ====================
ProteinPathway.destroy_all

old_db_table = "membranome.pathways"
new_type = "protein_pathways"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	protein_ref = proteins[object["uniprotid"].strip]
	ProteinPathway.create(
		protein: protein_ref,
		link: object["link"],
		pathway: object["pathway"],
		dbname: object["dbname"],
	)
end

puts :json=> ProteinPathway.first
puts "Created " +ProteinPathway.all.count.to_s+ " of type '"+new_type+"'\n\n"

# ## ==================== creating new Pdb Links ====================
# ## ==================== creating new Pdb Links ====================
PdbLink.destroy_all

old_db_table = "membranome.pdb"
new_type = "pdb_links"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	protein_ref = proteins[object["uniprotid"].strip]
	PdbLink.create(
		protein: protein_ref,
		pdb: object["pdb"],
		subunit_name: object["subunit_name"],
	)
end

puts :json=> PdbLink.first
puts "Created " +PdbLink.all.count.to_s+ " of type '"+new_type+"'\n\n"

ComplexPdbLink.destroy_all

old_db_table = "membranome.complex_pdb"
new_type = "complex_pdb_links"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	complex_ref = complexes[object["complexid"].strip]
	ComplexPdbLink.create(
		protein_complex: complex_ref,
		pdb: object["refpdbcode"],
		species_name: object["specie_name"],
	)
end

puts :json=> ComplexPdbLink.first
puts "Created "+ComplexPdbLink.all.count.to_s+ " of type '"+new_type+"'\n\n"



# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200130170129) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "fuzzystrmatch"
  enable_extension "pg_trgm"

  create_table "complex_classifications", force: :cascade do |t|
    t.string   "protein_family_name_cache", default: ""
    t.integer  "complex_structure_id"
    t.integer  "protein_family_id"
    t.integer  "protein_superfamily_id"
    t.integer  "protein_class_id"
    t.integer  "protein_type_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["complex_structure_id"], name: "index_complex_classifications_on_complex_structure_id", using: :btree
    t.index ["protein_class_id"], name: "index_complex_classifications_on_protein_class_id", using: :btree
    t.index ["protein_family_id"], name: "index_complex_classifications_on_protein_family_id", using: :btree
    t.index ["protein_superfamily_id"], name: "index_complex_classifications_on_protein_superfamily_id", using: :btree
    t.index ["protein_type_id"], name: "index_complex_classifications_on_protein_type_id", using: :btree
  end

  create_table "complex_membranes", force: :cascade do |t|
    t.string   "membrane_name_cache",            default: ""
    t.integer  "complex_structures_count_cache", default: 0
    t.integer  "membrane_id"
    t.integer  "complex_structure_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["complex_structure_id"], name: "index_complex_membranes_on_complex_structure_id", using: :btree
    t.index ["membrane_id"], name: "index_complex_membranes_on_membrane_id", using: :btree
  end

  create_table "complex_pdb_links", force: :cascade do |t|
    t.integer  "complex_structure_id"
    t.float    "ordering"
    t.string   "pdb"
    t.string   "species_name"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.float    "resolution"
    t.index ["complex_structure_id"], name: "index_complex_pdb_links_on_complex_structure_id", using: :btree
  end

  create_table "complex_pdb_uniprots", force: :cascade do |t|
    t.integer  "ordering"
    t.string   "uniprotcode"
    t.string   "uniprotid"
    t.integer  "complex_pdb_link_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["complex_pdb_link_id"], name: "index_complex_pdb_uniprots_on_complex_pdb_link_id", using: :btree
  end

  create_table "complex_proteins", force: :cascade do |t|
    t.integer  "protein_id"
    t.integer  "complex_structure_id"
    t.string   "protein_name_cache"
    t.string   "complex_structure_name_cache"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["complex_structure_id"], name: "index_complex_proteins_on_complex_structure_id", using: :btree
    t.index ["protein_id"], name: "index_complex_proteins_on_protein_id", using: :btree
  end

  create_table "complex_structures", force: :cascade do |t|
    t.float    "ordering"
    t.string   "name"
    t.float    "thickness"
    t.float    "thicknesserror"
    t.integer  "tilt"
    t.integer  "tilterror"
    t.integer  "numsubunits"
    t.float    "gibbs"
    t.text     "comments"
    t.string   "refpdbcode"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "source_db"
    t.string   "source_db_link"
    t.string   "pubmed"
    t.string   "species_name_cache",       default: ""
    t.integer  "species_id"
    t.integer  "polytopic_proteins_count", default: 0
    t.index ["species_id"], name: "index_complex_structures_on_species_id", using: :btree
  end

  create_table "complex_uniprot_links", force: :cascade do |t|
    t.integer  "ordering"
    t.string   "uniprotcode"
    t.string   "uniprotid"
    t.string   "uniprot_name"
    t.string   "type_of_protein"
    t.integer  "complex_structure_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["complex_structure_id"], name: "index_complex_uniprot_links_on_complex_structure_id", using: :btree
  end

  create_table "diseases", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "mutations_count", default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "diseases_mutations", id: false, force: :cascade do |t|
    t.integer "mutation_id", null: false
    t.integer "disease_id",  null: false
    t.index ["disease_id", "mutation_id"], name: "index_diseases_mutations_on_disease_id_and_mutation_id", using: :btree
    t.index ["mutation_id", "disease_id"], name: "index_diseases_mutations_on_mutation_id_and_disease_id", using: :btree
  end

  create_table "domains", force: :cascade do |t|
    t.integer  "protein_id"
    t.float    "ordering"
    t.string   "firstaa"
    t.string   "lastaa"
    t.string   "protein_pdbid"
    t.string   "pdbsubunit"
    t.string   "seqidentity"
    t.string   "domain"
    t.string   "pfamlink"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["protein_id"], name: "index_domains_on_protein_id", using: :btree
  end

  create_table "membranes", force: :cascade do |t|
    t.float    "ordering"
    t.string   "name"
    t.string   "topology_in"
    t.string   "topology_out"
    t.string   "abbreviation"
    t.integer  "proteins_count",           default: 0
    t.integer  "complex_structures_count", default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "mutation_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "mutations_count", default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "mutations", force: :cascade do |t|
    t.float    "ordering"
    t.string   "ordering_cache"
    t.string   "mutation"
    t.text     "comments"
    t.string   "pdbid"
    t.string   "residue"
    t.string   "source"
    t.integer  "mutation_type_id"
    t.integer  "protein_type_id"
    t.integer  "protein_class_id"
    t.integer  "protein_superfamily_id"
    t.integer  "protein_family_id"
    t.string   "protein_family_name_cache"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "protein_id"
    t.string   "mutation_type_name_cache"
    t.string   "diseases_name_cache"
    t.index ["mutation_type_id"], name: "index_mutations_on_mutation_type_id", using: :btree
    t.index ["protein_class_id"], name: "index_mutations_on_protein_class_id", using: :btree
    t.index ["protein_family_id"], name: "index_mutations_on_protein_family_id", using: :btree
    t.index ["protein_id"], name: "index_mutations_on_protein_id", using: :btree
    t.index ["protein_superfamily_id"], name: "index_mutations_on_protein_superfamily_id", using: :btree
    t.index ["protein_type_id"], name: "index_mutations_on_protein_type_id", using: :btree
  end

  create_table "opm_links", force: :cascade do |t|
    t.integer  "protein_id"
    t.float    "ordering"
    t.string   "opm_link"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "subunit_name"
    t.index ["protein_id"], name: "index_opm_links_on_protein_id", using: :btree
  end

  create_table "pathway_classes", force: :cascade do |t|
    t.integer  "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.integer  "pathway_subclasses_count", default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "pathway_subclasses", force: :cascade do |t|
    t.integer  "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.string   "pathway_class_name_cache", default: ""
    t.integer  "pathway_class_id"
    t.integer  "pathways_count",           default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["pathway_class_id"], name: "index_pathway_subclasses_on_pathway_class_id", using: :btree
  end

  create_table "pathways", force: :cascade do |t|
    t.integer  "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.string   "dbname"
    t.string   "link"
    t.string   "pathway_subclass_name_cache", default: ""
    t.integer  "pathway_class_id"
    t.integer  "pathway_subclass_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["pathway_class_id"], name: "index_pathways_on_pathway_class_id", using: :btree
    t.index ["pathway_subclass_id"], name: "index_pathways_on_pathway_subclass_id", using: :btree
  end

  create_table "pdb_links", force: :cascade do |t|
    t.integer  "protein_id"
    t.float    "ordering"
    t.string   "pdb"
    t.string   "subunit_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["protein_id"], name: "index_pdb_links_on_protein_id", using: :btree
  end

  create_table "protein_classes", force: :cascade do |t|
    t.float    "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.string   "description"
    t.integer  "protein_type_id"
    t.integer  "protein_superfamilies_count", default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "complex_structures_count",    default: 0
    t.index ["protein_type_id"], name: "index_protein_classes_on_protein_type_id", using: :btree
  end

  create_table "protein_families", force: :cascade do |t|
    t.float    "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.string   "interpro"
    t.integer  "protein_superfamily_id"
    t.integer  "proteins_count",           default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "complex_structures_count", default: 0
    t.index ["protein_superfamily_id"], name: "index_protein_families_on_protein_superfamily_id", using: :btree
  end

  create_table "protein_interactions", force: :cascade do |t|
    t.string   "related_protein_name_cache"
    t.string   "complex"
    t.string   "pubmed"
    t.integer  "primary_protein_id"
    t.integer  "related_protein_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "dbname"
    t.integer  "bitopic_protein_count"
    t.string   "experiment_type"
    t.string   "interaction_type"
    t.index ["primary_protein_id"], name: "index_protein_interactions_on_primary_protein_id", using: :btree
    t.index ["related_protein_id"], name: "index_protein_interactions_on_related_protein_id", using: :btree
  end

  create_table "protein_membranes", force: :cascade do |t|
    t.string   "membrane_name_cache"
    t.string   "protein_pdbid_cache"
    t.string   "pubmed"
    t.boolean  "main"
    t.integer  "membrane_id"
    t.integer  "protein_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["membrane_id"], name: "index_protein_membranes_on_membrane_id", using: :btree
    t.index ["protein_id"], name: "index_protein_membranes_on_protein_id", using: :btree
  end

  create_table "protein_pathways", force: :cascade do |t|
    t.integer  "protein_id"
    t.float    "ordering"
    t.string   "pathway_link_cache"
    t.string   "pathway_name_cache"
    t.string   "pathway_dbname_cache"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "pathway_id"
    t.string   "protein_name_cache"
    t.string   "protein_pdbid_cache"
    t.index ["pathway_id"], name: "index_protein_pathways_on_pathway_id", using: :btree
    t.index ["protein_id"], name: "index_protein_pathways_on_protein_id", using: :btree
  end

  create_table "protein_superfamilies", force: :cascade do |t|
    t.float    "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.string   "comment"
    t.string   "pfam"
    t.integer  "protein_class_id"
    t.integer  "protein_families_count",   default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "complex_structures_count", default: 0
    t.index ["protein_class_id"], name: "index_protein_superfamilies_on_protein_class_id", using: :btree
  end

  create_table "protein_types", force: :cascade do |t|
    t.float    "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.string   "description"
    t.integer  "protein_classes_count",    default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "complex_structures_count", default: 0
  end

  create_table "proteins", force: :cascade do |t|
    t.float    "ordering"
    t.string   "ordering_cache"
    t.string   "name"
    t.text     "comments"
    t.string   "pdbid"
    t.string   "resolution"
    t.boolean  "topology_show_in"
    t.float    "thickness"
    t.float    "thicknesserror"
    t.integer  "tilt"
    t.integer  "tilterror"
    t.float    "gibbs"
    t.string   "tau"
    t.text     "verification"
    t.string   "uniprotcode"
    t.string   "genename"
    t.integer  "genenumber"
    t.string   "altname"
    t.string   "segment"
    t.text     "aaseq"
    t.float    "interpro"
    t.float    "pharmgkb"
    t.integer  "mimegene"
    t.string   "hmp"
    t.boolean  "display_reactome"
    t.integer  "protein_type_id"
    t.integer  "protein_class_id"
    t.integer  "protein_superfamily_id"
    t.integer  "protein_family_id"
    t.string   "protein_family_name_cache"
    t.integer  "membrane_id"
    t.string   "membrane_name_cache"
    t.integer  "species_id"
    t.string   "species_name_cache"
    t.integer  "domains_count",             default: 0
    t.integer  "protein_pathways_count",    default: 0
    t.integer  "opm_links_count",           default: 0
    t.integer  "pdb_links_count",           default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "intact"
    t.string   "biogrid"
    t.index ["membrane_id"], name: "index_proteins_on_membrane_id", using: :btree
    t.index ["protein_class_id"], name: "index_proteins_on_protein_class_id", using: :btree
    t.index ["protein_family_id"], name: "index_proteins_on_protein_family_id", using: :btree
    t.index ["protein_superfamily_id"], name: "index_proteins_on_protein_superfamily_id", using: :btree
    t.index ["protein_type_id"], name: "index_proteins_on_protein_type_id", using: :btree
    t.index ["species_id"], name: "index_proteins_on_species_id", using: :btree
  end

  create_table "species", force: :cascade do |t|
    t.float    "ordering"
    t.string   "name"
    t.string   "name_common"
    t.text     "description"
    t.integer  "proteins_count",           default: 0
    t.integer  "complex_structures_count", default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "subcomplexes", force: :cascade do |t|
    t.string   "name"
    t.integer  "numsubunits"
    t.string   "uniprotids"
    t.string   "pdbcodes"
    t.integer  "pdb_files_count"
    t.text     "comments"
    t.string   "source_db"
    t.string   "source_db_link"
    t.string   "pubmed"
    t.integer  "complex_structure_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["complex_structure_id"], name: "index_subcomplexes_on_complex_structure_id", using: :btree
  end

  add_foreign_key "complex_classifications", "complex_structures"
  add_foreign_key "complex_classifications", "protein_classes"
  add_foreign_key "complex_classifications", "protein_families"
  add_foreign_key "complex_classifications", "protein_superfamilies"
  add_foreign_key "complex_classifications", "protein_types"
  add_foreign_key "complex_membranes", "complex_structures"
  add_foreign_key "complex_membranes", "membranes"
  add_foreign_key "complex_pdb_links", "complex_structures"
  add_foreign_key "complex_pdb_uniprots", "complex_pdb_links"
  add_foreign_key "complex_proteins", "complex_structures"
  add_foreign_key "complex_proteins", "proteins"
  add_foreign_key "complex_structures", "species"
  add_foreign_key "complex_uniprot_links", "complex_structures"
  add_foreign_key "domains", "proteins"
  add_foreign_key "mutations", "mutation_types"
  add_foreign_key "mutations", "protein_classes"
  add_foreign_key "mutations", "protein_families"
  add_foreign_key "mutations", "protein_superfamilies"
  add_foreign_key "mutations", "protein_types"
  add_foreign_key "opm_links", "proteins"
  add_foreign_key "pathway_subclasses", "pathway_classes"
  add_foreign_key "pathways", "pathway_classes"
  add_foreign_key "pathways", "pathway_subclasses"
  add_foreign_key "pdb_links", "proteins"
  add_foreign_key "protein_classes", "protein_types"
  add_foreign_key "protein_families", "protein_superfamilies"
  add_foreign_key "protein_interactions", "proteins", column: "primary_protein_id"
  add_foreign_key "protein_interactions", "proteins", column: "related_protein_id"
  add_foreign_key "protein_membranes", "membranes"
  add_foreign_key "protein_membranes", "proteins"
  add_foreign_key "protein_pathways", "pathways"
  add_foreign_key "protein_pathways", "proteins"
  add_foreign_key "protein_superfamilies", "protein_classes"
  add_foreign_key "proteins", "membranes"
  add_foreign_key "proteins", "protein_classes"
  add_foreign_key "proteins", "protein_families"
  add_foreign_key "proteins", "protein_superfamilies"
  add_foreign_key "proteins", "protein_types"
  add_foreign_key "proteins", "species"
  add_foreign_key "subcomplexes", "complex_structures"
end

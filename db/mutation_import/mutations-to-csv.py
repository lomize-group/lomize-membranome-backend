import sys

in_idxs = {
	'pdbid': 0,
	'ordering_cache': 1,
	'resolution': 2,
	'mutation': 3,
	'mutation_type': 4,
	'comments': 5,
	'disease': 6,
	'source': 7
}

out_idxs = {	
	'ordering': 0,
	'ordering_cache': 1,
	'mutation': 2,
	'comments': 3,
	'pdbid': 4,
	'resolution': 5,
	'source': 6,
	'mutation_type': 7,
	'disease': 8,
	'protein_type': 9,
	'protein_class': 10,
	'protein_superfamily': 11,
	'protein_family': 12,
	'protein_family_name_cache': 13,
	'protein': 14
}

mutation_types = {
	'germline': 1,
	'somatic': 2,
	'germline and somatic': 3,
	'unknown': 4
}

diseases = {
	'cancers': 1,
	'immune system diseases': 2,
	'nervous system diseases': 3,
	'cardiovascular diseases': 4,
	'congenital disorders of metabolism': 5,
	'other congenital disorders': 6,
	'digestive system diseases': 7,
	'endocrine and metabolic diseases': 8,
	'musculoskeletal diseases': 9,
	'skin diseases': 10,
	'urinary system diseases': 11,
	'Polymorphism': 12,
	'Not provided': 13
}

import_path = ""
if len(sys.argv) < 2:
	import_path = '../../db/mutations.txt'
else:
	import_path = sys.argv[1]

row_strs = []
with open(import_path) as f:
	# read in all rows
	read_data = f.read()
	row_strs = read_data.split('\n')

# parse rows
rows = []
seen = set() # avoid duplicates because they exist in the input
for i in range(len(row_strs)):
	row = row_strs[i].split('|')
	for i in range(len(row)): row[i] = row[i].strip()
	set_key = tuple(row)
	if set_key not in seen and len(row) == 8:
		seen.add(set_key)
		rows.append(row)
rows.pop(0)


# out_rows = [[''] * len(out_idxs)] * len(rows)
out_rows = []
for i in range(len(rows)):
	out_rows.append(['']*len(out_idxs))
	field = 'pdbid'
	out_rows[i][out_idxs[field]] = rows[i][in_idxs[field]]
	field = 'ordering_cache'
	out_rows[i][out_idxs[field]] = rows[i][in_idxs[field]]
	field = 'resolution'
	out_rows[i][out_idxs[field]] = rows[i][in_idxs[field]]
	field = 'mutation'
	out_rows[i][out_idxs[field]] = rows[i][in_idxs[field]]
	field = 'comments'
	out_rows[i][out_idxs[field]] = rows[i][in_idxs[field]]
	field = 'source'
	out_rows[i][out_idxs[field]] = rows[i][in_idxs[field]]

	field = 'ordering'
	out_rows[i][out_idxs[field]] = '0'
	field = 'protein_family_name_cache'
	out_rows[i][out_idxs[field]] = 'none'

	field = 'mutation_type'
	out_rows[i][out_idxs[field]] = str(mutation_types[rows[i][in_idxs[field]]])

	field = 'protein_type'
	out_rows[i][out_idxs[field]] = '0'
	field = 'protein_class'
	out_rows[i][out_idxs[field]] = '0'
	field = 'protein_superfamily'
	out_rows[i][out_idxs[field]] = '0'
	field = 'protein_family'
	out_rows[i][out_idxs[field]] = '0'
	field = 'protein'
	out_rows[i][out_idxs[field]] = '0'

	field = 'protein_family_name_cache'
	out_rows[i][out_idxs[field]] = 'none'

	field = 'disease'
	disease_type = rows[i][in_idxs[field]]
	out_rows[i][out_idxs[field]] = str(diseases[disease_type])

headers = sorted(out_idxs, key=out_idxs.get)
for i in range(len(headers)):
	output = headers[i]
	if (i < len(headers) - 1): output += ','
	print(output, end='')
print()

# to csv
for i in range(len(out_rows)):
	for j in range(len(out_rows[i])):
		output = out_rows[i][j].replace(',', ';')
		if (j < len(out_rows[i]) - 1): output += ','
		print(output, end='')
	print()


class AddIntactBiogridToProteins < ActiveRecord::Migration[5.0]
  def change
  	add_column :proteins, :intact, :boolean
  	add_column :proteins, :biogrid, :string
  end
end

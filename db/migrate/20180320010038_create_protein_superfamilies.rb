class CreateProteinSuperfamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :protein_superfamilies do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      t.string :comment
      t.string :pfam
      
      t.references :protein_class, foreign_key: true, index: true

      t.integer  :protein_families_count, default: 0

      t.timestamps
    end
  end
end

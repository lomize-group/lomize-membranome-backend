class UpateComplexStructureColumns < ActiveRecord::Migration[5.0]
	def change
		remove_column :complex_structures, :complexid
		remove_column :complex_structures, :complex_family_name_cache
		remove_column :complex_structures, :membrane_name_cache
		remove_column :complex_structures, :complex_pdb_links_count
		remove_column :complex_structures, :ordering_cache
		remove_column :complex_structures, :topology_show_in
		remove_column :complex_structures, :topology_subunit

		remove_reference :complex_structures, :membrane, index: true, foreign_key: true
		remove_reference :complex_structures, :complex_class, index: true, foreign_key: true
		remove_reference :complex_structures, :complex_family, index: true, foreign_key: true

		add_column :complex_structures, :source_db, :string
		add_column :complex_structures, :source_db_link, :string
		add_column :complex_structures, :pubmed, :string

		add_column :complex_structures, :species_name_cache, :string, default: ""

		add_reference :complex_structures, :species, foreign_key: true, index: true, null: true
	end
end

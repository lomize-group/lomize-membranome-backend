class RenameProteinComplexToComplex < ActiveRecord::Migration[5.0]
	def change
		rename_table :protein_complexes, :complex_structures
		rename_table :protein_complex_interactions, :complex_proteins

		rename_column :complex_pdb_links, :protein_complex_id, :complex_structure_id
		rename_column :complex_proteins, :protein_complex_id, :complex_structure_id
		rename_column :complex_proteins, :protein_complex_name_cache, :complex_structure_name_cache

		rename_column :complex_families, :protein_complexes_count, :complex_structures_count
		rename_column :species, :protein_complexes_count, :complex_structures_count
		rename_column :membranes, :protein_complexes_count, :complex_structures_count
	end
end

class CreateDomains < ActiveRecord::Migration[5.0]
  def change
    create_table :domains do |t|
		t.references :protein, foreign_key: true, index: true
		t.float :ordering
		t.string :firstaa
		t.string :lastaa
		t.string :protein_pdbid
		t.string :pdbsubunit
		t.string :seqidentity
		t.string :domain
		t.string :pfamlink
		t.timestamps
    end
  end
end

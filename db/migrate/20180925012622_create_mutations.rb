class CreateMutations < ActiveRecord::Migration[5.0]
  def change
    create_table :mutations do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :mutation
      t.text :comments
      t.string :pdbid
      t.string :resolution
      t.string :source
      
      t.references :mutation_type, foreign_key: true, index: true
      t.references :disease, foreign_key: true, index: true
      t.references :protein_type, foreign_key: true, index: true
      t.references :protein_class, foreign_key: true, index: true
      t.references :protein_superfamily, foreign_key: true, index: true

      t.references :protein_family, foreign_key: true, index: true
      t.string :protein_family_name_cache

      t.timestamps
    end
  end
end

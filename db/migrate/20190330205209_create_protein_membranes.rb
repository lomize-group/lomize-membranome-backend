class CreateProteinMembranes < ActiveRecord::Migration[5.0]
    def change
      create_table :protein_membranes do |t|
      	t.string :membrane_name_cache
      	t.string :protein_pdbid_cache
      	t.string :pubmed
        t.boolean :main
      	t.references :membrane, foreign_key: true, index: true
      	t.references :protein, foreign_key: true, index: true
  		  t.timestamps
      end
    end
end

class CreateComplexClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :complex_classes do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name

      t.integer  :complex_families_count, default: 0
      
      t.timestamps
    end
  end
end

class CreateProteinComplexes < ActiveRecord::Migration[5.0]
  def change
    create_table :protein_complexes do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      t.float :thickness
      t.float :thicknesserror
      t.integer :tilt
      t.integer :tilterror
      t.integer :numsubunits
      t.float :gibbs
      t.boolean :topology_show_in
      t.string :topology_subunit
      t.text :comments
      t.string :complexid
      t.string :refpdbcode

      t.references :complex_class, foreign_key: true, index: true
      t.references :complex_family, foreign_key: true, index: true
      t.string :complex_family_name_cache
      t.references :membrane, foreign_key: true, index: true
      t.string :membrane_name_cache

      t.integer  :complex_pdb_links_count, default: 0


      t.timestamps
    end
  end
end

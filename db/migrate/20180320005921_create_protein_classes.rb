class CreateProteinClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :protein_classes do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      t.string :description
      
      t.references :protein_type, foreign_key: true, index: true

      t.integer  :protein_superfamilies_count, default: 0

      t.timestamps
    end
  end
end

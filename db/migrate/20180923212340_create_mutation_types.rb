class CreateMutationTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :mutation_types do |t|
      t.string :name
      t.string :description
      
      t.integer :mutations_count, default: 0

      t.timestamps
    end
  end
end

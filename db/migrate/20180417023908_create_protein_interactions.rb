class CreateProteinInteractions < ActiveRecord::Migration[5.0]
  def change
    create_table :protein_interactions do |t|
      
      t.string :related_protein_name_cache
      t.string :complex
      t.string :pdbid
      t.string :pubmed

      t.references :primary_protein
      t.references :related_protein

      t.timestamps
    end

    add_foreign_key :protein_interactions, :proteins, column: :primary_protein_id, primary_key: :id, index: true
    add_foreign_key :protein_interactions, :proteins, column: :related_protein_id, primary_key: :id, index: true
  end
end

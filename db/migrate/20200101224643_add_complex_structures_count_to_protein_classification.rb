class AddComplexStructuresCountToProteinClassification < ActiveRecord::Migration[5.0]
  def change
    add_column :protein_families, :complex_structures_count, :integer, default: 0
    add_column :protein_superfamilies, :complex_structures_count, :integer, default: 0
    add_column :protein_classes, :complex_structures_count, :integer, default: 0
    add_column :protein_types, :complex_structures_count, :integer, default: 0
  end
end

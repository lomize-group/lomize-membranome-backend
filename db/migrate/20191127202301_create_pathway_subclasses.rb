class CreatePathwaySubclasses < ActiveRecord::Migration[5.0]
	def change
		create_table :pathway_subclasses do |t|
			t.integer :ordering
			t.string :ordering_cache
			t.string :name

			t.string :pathway_class_name_cache, default: ""

			t.references :pathway_class, foreign_key: true, index: true

			t.integer :pathways_count, default: 0

			t.timestamps
		end
	end
end

class AddColumnsToProteinInteractions < ActiveRecord::Migration[5.0]
	def change
		add_column :protein_interactions, :dbname, :string
		add_column :protein_interactions, :bitopic_protein_count, :integer
	end
end

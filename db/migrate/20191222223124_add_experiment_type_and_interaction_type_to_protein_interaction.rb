class AddExperimentTypeAndInteractionTypeToProteinInteraction < ActiveRecord::Migration[5.0]
	def change
		remove_column :protein_interactions, :pdbid, :string

		add_column :protein_interactions, :experiment_type, :string
		add_column :protein_interactions, :interaction_type, :string
	end
end

class CreateProteinTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :protein_types do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      t.string :description

      t.integer  :protein_classes_count, default: 0
      
      t.timestamps
    end
  end
end

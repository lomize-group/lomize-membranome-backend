class CreateProteins < ActiveRecord::Migration[5.0]
  def change
    create_table :proteins do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      t.text :comments
      t.string :pdbid
      t.string :resolution
      t.boolean :topology_show_in
      t.float :thickness
      t.float :thicknesserror
      t.integer :tilt
      t.integer :tilterror
      t.float :gibbs
      t.string :tau
      t.text :verification
      t.string :uniprotcode
      t.string :genename
      t.integer :genenumber
      t.string :altname
      t.string :segment
      t.text :aaseq
      t.float :interpro
      t.float :pharmgkb
      t.integer :mimegene
      t.string :hmp
      t.boolean :display_reactome
      
      t.references :protein_type, foreign_key: true, index: true
      t.references :protein_class, foreign_key: true, index: true
      t.references :protein_superfamily, foreign_key: true, index: true
      t.references :protein_family, foreign_key: true, index: true
      t.string :protein_family_name_cache
      t.references :membrane, foreign_key: true, index: true
      t.string :membrane_name_cache
      t.references :species, foreign_key: true, index: true
      t.string :species_name_cache

      t.integer  :domains_count, default: 0
      t.integer  :pathways_count, default: 0
      t.integer  :opm_links_count, default: 0
      t.integer  :pdb_links_count, default: 0

      t.timestamps
    end
  end
end

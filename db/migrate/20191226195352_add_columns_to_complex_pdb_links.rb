class AddColumnsToComplexPdbLinks < ActiveRecord::Migration[5.0]
    def change
        add_column :complex_pdb_links, :resolution, :float
    end
end

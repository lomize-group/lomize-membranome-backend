class CreateProteinComplexInteractions < ActiveRecord::Migration[5.0]
  def change
    #models the many-to-many relationship between proteins and protein complexes
    create_table :protein_complex_interactions do |t|
      t.references :protein, foreign_key: true, index: true
      t.references :protein_complex, foreign_key: true, index: true
      
      t.string :protein_name_cache
      t.string :protein_complex_name_cache
      
      t.timestamps
    end
  end
end


class AddCachesToMutations < ActiveRecord::Migration[5.0]
  def change
  	add_column :mutations, :mutation_type_name_cache, :string
  	add_column :mutations, :diseases_name_cache, :string
  end
end

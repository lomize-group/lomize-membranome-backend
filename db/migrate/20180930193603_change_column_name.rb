class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
  	rename_column :diseases, :diseases_count, :mutations_count
  end
end

class CreatePdbLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :pdb_links do |t|
      t.references :protein, foreign_key: true, index: true
      t.float :ordering
      t.string :pdb
      t.string :subunit_name
      t.timestamps
    end
  end
end

class UpdateProteinPathwaysColumns < ActiveRecord::Migration[5.0]
	def change
		rename_column :protein_pathways, :link, :pathway_link_cache
		rename_column :protein_pathways, :pathway, :pathway_name_cache
		rename_column :protein_pathways, :dbname, :pathway_dbname_cache
		add_reference :protein_pathways, :pathway, foreign_key: true, index: true

		add_column :protein_pathways, :protein_name_cache, :string
		add_column :protein_pathways, :protein_pdbid_cache, :string
	end
end

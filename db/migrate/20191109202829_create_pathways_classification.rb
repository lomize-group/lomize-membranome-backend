class CreatePathwaysClassification < ActiveRecord::Migration[5.0]
  def change
    create_table :pathways do |t|
    	t.string :pathway
    	t.string :dbname
    	t.string :link

    	# cache
    	t.string :pathway_family_name_cache

    	# references
    	t.references :pathway_family, foreign_key: true, index: true

    	t.timestamps
    end
  end
end

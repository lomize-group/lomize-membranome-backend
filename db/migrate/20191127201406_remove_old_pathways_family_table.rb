class RemoveOldPathwaysFamilyTable < ActiveRecord::Migration[5.0]
	def change
		drop_table :pathway_families
	end
end

class CreateComplexFamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :complex_families do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      
      t.references :complex_class, foreign_key: true, index: true
      t.integer  :protein_complexes_count, default: 0

      t.timestamps
    end
  end
end

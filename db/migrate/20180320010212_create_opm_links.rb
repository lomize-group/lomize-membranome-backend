class CreateOpmLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :opm_links do |t|
      t.references :protein, foreign_key: true, index: true
      t.float :ordering
      t.string :opm_link
      t.timestamps
    end
  end
end

class CreateComplexMembranes < ActiveRecord::Migration[5.0]
	def change
		create_table :complex_membranes do |t|
			t.string :membrane_name_cache, default: ""
			t.integer :complex_structures_count_cache, default: 0

			t.references :membrane, foreign_key: true, index: true
			t.references :complex_structure, foreign_key: true, index: true

			t.timestamps
		end
	end
end

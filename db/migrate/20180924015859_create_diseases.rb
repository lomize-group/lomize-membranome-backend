class CreateDiseases < ActiveRecord::Migration[5.0]
  def change
    create_table :diseases do |t|
      t.string :name
      t.string :description

      t.integer :diseases_count, default: 0

      t.timestamps
    end
  end
end

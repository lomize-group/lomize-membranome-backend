class CreateComplexUniprotLinks < ActiveRecord::Migration[5.0]
	def change
		create_table :complex_uniprot_links do |t|
			t.integer :ordering
			t.string :uniprotcode
			t.string :uniprotid
			t.string :uniprot_name
			t.string :type_of_protein # polytopic, water_soluble, etc.

			t.references :complex_structure, :foreign_key => true, :index => true

			t.timestamps
		end
	end
end

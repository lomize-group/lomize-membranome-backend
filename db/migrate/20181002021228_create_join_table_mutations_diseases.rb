class CreateJoinTableMutationsDiseases < ActiveRecord::Migration[5.0]
  def change
    create_join_table :mutations, :diseases do |t|
      t.index [:mutation_id, :disease_id]
      t.index [:disease_id, :mutation_id]
    end
  end
end

class AddSubunitNameToOpmLinks < ActiveRecord::Migration[5.0]
	def change
		change_table :opm_links do |t|
			t.string :subunit_name
		end
	end
end

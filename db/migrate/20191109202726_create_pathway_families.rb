class CreatePathwayFamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :pathway_families do |t|
    	t.string :name
    	t.integer  :pathways_count, default: 0

    	t.timestamps
    end
  end
end

class RemoveDiseaseFromMutation < ActiveRecord::Migration[5.0]
  def change
    remove_reference :mutations, :disease, foreign_key: true
  end
end

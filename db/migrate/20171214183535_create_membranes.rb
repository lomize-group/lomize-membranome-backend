class CreateMembranes < ActiveRecord::Migration[5.0]
  def change
    create_table :membranes do |t|
      t.float :ordering
      t.string :name
      t.string :topology_in
      t.string :topology_out
      t.string :abbreviation

      t.integer  :proteins_count, default: 0
      t.integer  :protein_complexes_count, default: 0
      
      t.timestamps
    end
  end
end

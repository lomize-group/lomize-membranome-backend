class ChangeColumnNameInMutations < ActiveRecord::Migration[5.0]
  def change
  	rename_column :mutations, :resolution, :residue
  end
end

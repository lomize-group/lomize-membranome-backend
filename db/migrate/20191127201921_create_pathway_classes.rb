class CreatePathwayClasses < ActiveRecord::Migration[5.0]
	def change
		create_table :pathway_classes do |t|
			t.integer :ordering
			t.string :ordering_cache
			t.string :name

			t.integer :pathway_subclasses_count, default: 0

			t.timestamps
		end
	end
end

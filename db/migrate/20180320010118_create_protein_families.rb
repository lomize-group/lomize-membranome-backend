class CreateProteinFamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :protein_families do |t|
      t.float :ordering
      t.string :ordering_cache
      t.string :name
      t.string :interpro
      
      t.references :protein_superfamily, foreign_key: true, index: true

      t.integer  :proteins_count, default: 0

      t.timestamps
    end
  end
end

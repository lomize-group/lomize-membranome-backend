class CreateComplexClassifications < ActiveRecord::Migration[5.0]
	def change
		# remove old classification tables
		drop_table :complex_families
		drop_table :complex_classes

		# create new unified classification table
		# relates to protein tables for classification
		create_table :complex_classifications do |t|
			t.string :protein_family_name_cache, default: ""

			t.references :complex_structure, foreign_key: true, index: true
			t.references :protein_family, foreign_key: true, index: true
			t.references :protein_superfamily, foreign_key: true, index: true
			t.references :protein_class, foreign_key: true, index: true
			t.references :protein_type, foreign_key: true, index: true

			t.timestamps
		end
	end
end

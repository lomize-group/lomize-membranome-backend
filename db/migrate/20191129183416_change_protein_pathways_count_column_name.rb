class ChangeProteinPathwaysCountColumnName < ActiveRecord::Migration[5.0]
	def change
		rename_column :proteins, :pathways_count, :protein_pathways_count
	end
end

class CreateComplexPdbLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :complex_pdb_links do |t|
    	t.references :protein_complex, foreign_key: true, index: true
		t.float :ordering
		t.string :pdb
		t.string :species_name
		t.timestamps
    end
  end
end

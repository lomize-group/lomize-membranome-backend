class RenamePathwaysToProteinPathways < ActiveRecord::Migration[5.0]
	def change
		rename_table :pathways, :protein_pathways
	end
end

class AddProteinToMutations < ActiveRecord::Migration[5.0]
  def change
  	    add_reference :mutations, :protein, index: true
  end
end

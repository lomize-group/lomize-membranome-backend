class CreatePathways < ActiveRecord::Migration[5.0]
	def change
		create_table :pathways do |t|
			t.integer :ordering
			t.string :ordering_cache
			t.string :name
			t.string :dbname
			t.string :link

			t.string :pathway_subclass_name_cache, default: ""

			t.references :pathway_class, foreign_key: true, index: true
			t.references :pathway_subclass, foreign_key: true, index: true

			t.timestamps
		end
	end
end

class AddPolytopicProteinsCountToComplexStructure < ActiveRecord::Migration[5.0]
  def change
    add_column :complex_structures, :polytopic_proteins_count, :integer, default: 0
  end
end

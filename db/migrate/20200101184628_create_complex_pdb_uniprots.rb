class CreateComplexPdbUniprots < ActiveRecord::Migration[5.0]
    def change
        create_table :complex_pdb_uniprots do |t|
            t.integer :ordering
            t.string :uniprotcode
            t.string :uniprotid

            t.references :complex_pdb_link, :foreign_key => true, :index => true

            t.timestamps
        end
    end
end

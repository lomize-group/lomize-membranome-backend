class CreateSubcomplexes < ActiveRecord::Migration[5.0]
	def change
		create_table :subcomplexes do |t|
			t.string :name
			t.integer :numsubunits
			t.string :uniprotids
			t.string :pdbcodes
			t.integer :pdb_files_count
			t.text :comments
			t.string :source_db
			t.string :source_db_link
			t.string :pubmed

			t.references :complex_structure, foreign_key: true, index: true

			t.timestamps
		end
	end
end
